﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using Tests;
using Utils.Components.Screens.Efui;
using Utils.Components.Screens.Emis;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Utils;
using Utils.Utils.Drivers;
using Utils.Utils.VboxUtils.Enums;

namespace RemoteEmis
{
    [TestFixture]
    public abstract class RemoteEmisBaseTest : BaseTest
    {
        /// <summary>
        /// Nunit requires at least one constructor without parameters
        /// </summary>
        protected RemoteEmisBaseTest() : this(null) { }

        protected RemoteEmisBaseTest(DriverOptions driverOptions = null) : base(driverOptions ?? RemoteEmisDefaultDriverOptions)
        {
        }

        public static DriverOptions RemoteEmisDefaultDriverOptions => new DriverOptions(
            useLocalIp: true,
            mainWindows: new Window[] { },
            vboxOptions: null // if you pass null here, the defaults are used
        );


    }

}
