﻿using NUnit.Framework;
using RemoteEmis;
using Utils;
using Utils.Components.Screens.RemoteEmis;
using Utils.Components.Windows.Temporary;
using Utils.Utils;

namespace Tests.Emis
{
    [TestFixture]
    internal class RemoteEmisTest : RemoteEmisBaseTest
    {
        [Test]
        [Order(2)]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [TestCase(Constants.s_versionToUpdateToFullBuildNumber, Constants.releaseBuild)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void TestRemoteEmisCleanInstall(string s_versionToUpdateToFullBuildNumber, bool releaseBuild)
        {
            JFrogHelper.DownloadeRemoteEmisPackages(s_versionToUpdateToFullBuildNumber, releaseBuild);
            //RemoteEmisWindow.InstallRemoteEmis(Constants.RemoteEmisDefault);
            RemoteEmisScreen remoteEmisScreen = new RemoteEmisScreen();

            remoteEmisScreen.Login();

            remoteEmisScreen.Dispose();
            //RemoteEmisWindow.LaunchRemoteEmis();

            //Utils.Components.Screens.Emis.RemoteEmisMainScreen.InstallRemoteEmis(Constants.RemoteEmisDefault);
            //Utils.Components.Screens.Emis.RemoteEmisMainScreen.LaunchRemoteEmis(Constants.RemoteEmisversion);
            //s_emisMainScreen.Login();
        }
    }
}
