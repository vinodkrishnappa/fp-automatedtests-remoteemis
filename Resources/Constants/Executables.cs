﻿namespace Resources.Constants
{
    public static class Executables
    {
        public const string CreateInstallablePackagesExe = ExecutablesPath + @"\fizx.exe";
        public const string JFrogExe = ExecutablesPath + @"\jfrog.exe";
        private const string ExecutablesPath = @"Resources\Executables";
    }
}
