﻿namespace Resources.Constants.Images
{
    public static class CommonImages
    {
        public const string CloseX = CommonImagesPath + @"\close-x-icon.png";
        private const string CommonImagesPath = @"Resources\Images\CommonImages";
    }
}
