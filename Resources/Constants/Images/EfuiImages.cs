﻿namespace Resources.Constants.Images
{
    public static class EfuiImages
    {
        public const string BlueCog = EfuiImagesPath + @"\blueCog.png";
        public const string CancelButton = EfuiImagesPath + @"\cancelButtonIcon.png";
        public const string CloseShift = EfuiImagesPath + @"\closeShift.png";
        public const string InstallationInProgress = EfuiImagesPath + @"\installationInProgressIcon.png";
        public const string Ok = EfuiImagesPath + @"\OK.png";
        public const string OpenShift = EfuiImagesPath + @"\openShift.png";
        public const string Operator = EfuiImagesPath + @"\operator.png";
        public const string StartSelfCheckout = EfuiImagesPath + @"\start-sco.png";
        private const string EfuiImagesPath = @"Resources\Images\EfuiImages";
    }
}
