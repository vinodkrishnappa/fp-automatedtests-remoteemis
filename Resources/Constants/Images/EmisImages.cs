﻿namespace Resources.Constants.Images

{
    public static class EmisImages
    {
        public const string InstallationInProgress = EmisImagesPath + @"\installationInProgressIcon.png";
        private const string EmisImagesPath = @"Resources\Images\EmisImages";
    }
}
