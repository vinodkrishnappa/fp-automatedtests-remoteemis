﻿namespace Resources.Constants.Images
{
    public static class SelfCheckoutImages
    {
        public const string GreenVerify = SelfCheckoutImagesPath + @"\greenVerifyIcon.png";
        public const string GreyWarning = SelfCheckoutImagesPath + @"\greyWarningTriangle.png";
        public const string Minus = SelfCheckoutImagesPath + @"\min.png";
        public const string Plus = SelfCheckoutImagesPath + @"\plus.png";
        public const string RedCone = SelfCheckoutImagesPath + @"\red-cone-icon.png";
        private const string SelfCheckoutImagesPath = @"Resources\Images\SelfCheckoutImages";
    }
}