﻿namespace Resources.Constants.Images
{
    public static class WindowsIcons
    {
        public const string Computer = WindowsIconsPath + @"\computerIcon.png";
        public const string ComputerWithArrows = WindowsIconsPath + @"\computerWithArrowsIcon.png";
        public const string Executable = WindowsIconsPath + @"\executableIcon.png";
        public const string Info = WindowsIconsPath + @"\infoIcon.png";
        public const string PowerOff = WindowsIconsPath + @"\powerOffIcon.png";
        private const string WindowsIconsPath = @"Resources\Images\WindowsIcons";
    }
}
