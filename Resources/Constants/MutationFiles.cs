﻿namespace Resources.Constants
{
    public static class MutationFiles
    {
        public const string FTPDownloadsDirectory = MutationSourceFileDirectory + @"FTPDownloads\";
        public const string FTPUploadsDirectory = MutationSourceFileDirectory + @"FTPUploads\";
        public const string MutationCleanupFileDirectory = MutationSourceFileDirectory + @"Mutation Files\CleanupMutationFiles\";
        public const string MutationResultDownloadDirectory = MutationSourceFileDirectory + @"Mutation Files\ResultFileDownloads\";
        public const string MutationSourceFileDirectory = @"Resources\";
    }
}
