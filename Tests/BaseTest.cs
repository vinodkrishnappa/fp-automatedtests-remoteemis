﻿using NUnit.Framework;
using Utils.Utils;
using Utils.Utils.Drivers;
using Utils.Utils.VboxUtils;

namespace Tests
{
    [TestFixture]
    public abstract class BaseTest
    {
        protected static VBoxManager s_vbox;
        protected ExtentReportsHelper _extentReportsHelper;
        protected DriverOptions driverOptions;
        private DriverConnection _driverConnection;

        protected BaseTest(DriverOptions driverOptions) => this.driverOptions = driverOptions;

        #region ExtentReports functions

        [SetUp]
        public void ExtentReportsSetUp() => _extentReportsHelper.AddReportForCurrentTest();

        [TearDown]
        public void ExtentReportsTearDown() => _extentReportsHelper.LogTestResult();

        #endregion ExtentReports functions

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            s_vbox.ToggleInUse(false);
            DriverConnection.CloseConnection();
            _extentReportsHelper.CloseReport();
        }

        [OneTimeSetUp]
        public void Setup()
        {
            // Create driverConnection
            _driverConnection = new DriverConnection(driverOptions);
            s_vbox = DriverConnection.GetVmInUse();

            // Reports (automatically starts stopwatch to report how long the run lasted)
            _extentReportsHelper = new ExtentReportsHelper();
        }
    }
}
