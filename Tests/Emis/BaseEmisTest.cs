﻿using Utils.Components.Screens.Efui;
using Utils.Components.Screens.Emis;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Utils.Drivers;

namespace Tests.Emis
{
    public abstract class BaseEmisTest : BaseTest
    {
        #region Screens

        protected static EfuiMainScreen EfuiScreen => DriverConnection.GetScreen<EfuiMainScreen>();
        protected static EmisMainScreen EmisScreen => DriverConnection.GetScreen<EmisMainScreen>();

        #endregion Screens

        /// <summary>
        /// Nunit requires at least one constructor without parameters
        /// </summary>
        protected BaseEmisTest() : this(null) { }

        protected BaseEmisTest(DriverOptions driverOptions = null) : base(driverOptions ?? EmisDefaultDriverOptions)
        {
        }

        public static DriverOptions EmisDefaultDriverOptions => new DriverOptions(
            mainWindows: new Window[] { MainWindows.Efui, MainWindows.Emis },
            vboxOptions: null // if you pass null here, the defaults are used
        );
    }
}
