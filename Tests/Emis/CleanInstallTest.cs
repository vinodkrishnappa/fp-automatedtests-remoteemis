﻿using System;
using NUnit.Framework;
using Utils;
using Utils.Utils;
using Utils.Utils.ConfigurationManager;
using Utils.Utils.Drivers;
using Utils.Utils.Enums;
using Utils.Utils.FtpHelper;
using Utils.Utils.SettingsInterfaces;

namespace Tests.Emis
{
    [TestFixture]
    internal class CleanInstallTest : BaseEmisTest
    {
        private static IRecoveryRebootSettings RecoveryRebootSettings => ConfigurationManager.Config.RecoveryRebootSettings;

        [Test]
        [Order(1)]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public static void CleanInstallWithSetup()
        {
            //-------------------DEVICE SETTINGS----------------------
            //Set the CIS peripherals, enable OPTs, and set the carwash.
            EmisScreen.Configuration.SetupCISPeripheralsConfiguration()
                .SetIQCryptoVGA(true)
                .SetCarwash(CarWashTypes.s_extended_generic_version);

            //Set Pinpad & Customer Display
            EmisScreen.Configuration.SetupPOSPeripheralsConfiguration()
                .SetPinPad("Verifone P400")
                .SetCustomerDisplay("VGA USB");

            //Make sure to add 5 port pairs in Com0Com, checking the 'use Ports Class' checkbox
            //Assign the pairs as follows: COM1 & COM6 - COM2 & COM7 - COM3 & COM8 - COM4 & COM9 - COM5 & COM10
            //First autodetect hardware and software
            //Then set serial Ports for dispensing

            EmisScreen.Configuration.SetupHardwareAndSoftware()
                .SetTouchScreen("PCAP USB")
                .AutoDetectSerialPorts();

            EmisScreen.Configuration.SetupSerialPortAssignment()
                .SetDeviceForComport("1", Devices.s_dispensing_port_without_number + "1");

            //-------------------GENERAL SETTINGS----------------------
            //Set the station name, address and logo
            EmisScreen.Configuration
                .SetupStationInformation();

            //Set the four language buttons
            EmisScreen.Configuration
                .SetupLanguageSelection();

            //The required settings to succesfully handle the ART_MUT & KEY_MUTfiles
            EmisScreen.Configuration.SetupFuelAndArticleProgramming();

            //Check the boxes for extra payment modes, litre coupons and payment vouchers,
            //Also required for some of the MUT tests to work.
            EmisScreen.Configuration.SetupPaymentModeConfiguration()
                .SetExtraPaymentModes(true)
                .SetLitreCoupons(true)
                .SetPaymentVouchers(true);

            //Adds and configures 4 fuels
            EmisScreen.Configuration.FuelConfiguration
                .AddFuel(FillingTypes.s_super_with_index)
                .AddFuel(FillingTypes.s_super_plus_with_index)
                .AddFuel(FillingTypes.s_diesel_with_index)
                .AddFuel(FillingTypes.s_lpg_with_index)
                .AddFuel(FillingTypes.s_eurosuper_with_index);
            //30 seconds after saving a fuel, a pop up shows in the bottom right corner for 5 seconds
            //This can block clicks in further steps so we save after adding all fuels and
            //wait for the popup to pass.
            //TODO Make dynamic
            EmisScreen.Configuration.FuelConfiguration.SaveFuelsWithWaitForPopUp();

            EmisScreen.Configuration.SetupDispensingConfiguration()
                .ConfigurePriceSign(1);

            EmisScreen.Configuration.SetupDispensingConfiguration()
                .ConfigureTankLevelGauge();

            //TODO Add 4 more serial ports and add 8 pumps instead of 4
            EmisScreen.Configuration.SetupDispensingConfiguration()
                .ConfigurePump(1)
                .ConfigurePump(2)
                .ConfigurePump(3)
                .ConfigurePump(4);

            //Adds the 4 EV Charge Points with default configuration
            EmisScreen.Configuration.SetupDispensingConfiguration()
                .ConfigureChargePoint(1)
                .ConfigureChargePoint(2)
                .ConfigureChargePoint(3)
                .ConfigureChargePoint(4);

            EmisScreen.Configuration.SaveConfigurationWithoutActivating();

            EmisScreen.Configuration
                .ConfigureTankGroup(1, FillingTypes.s_super)
                .ConfigureTankGroup(2, FillingTypes.s_super_plus)
                .ConfigureTankGroup(3, FillingTypes.s_diesel)
                .ConfigureTankGroup(4, FillingTypes.s_eurosuper);

            EmisScreen.Configuration
                .ConfigureNozzle(1, 1, FillingTypes.s_super)
                .ConfigureNozzle(2, 1, FillingTypes.s_super_plus)
                .ConfigureNozzle(3, 1, FillingTypes.s_diesel)
                .ConfigureNozzle(4, 1, FillingTypes.s_eurosuper);

            EmisScreen.Configuration.SaveConfigurationWithoutActivating();

            //Sets all pumps to automatic relase for both day and night mode
            EmisScreen.Configuration.SetupPumpsConfiguration()
               .SetAutomaticReleaseForAllPumps();

            //Set the online host settings, evoucher type, loyalty scheme and select OASE_PAY
            EmisScreen.Configuration.SetupOnlineHostConfiguration()
                .SetEvoucherType("On-line E-Voucher")
                .SetLoyaltyScheme("On-line OASE")
                .NavigateToOnlineServicesTab()
                .SetOasePay(true);

            EmisScreen.Configuration.SetupSalesOptionsConfiguration()
               .SetSelfCheckoutAllowed(true);

            EmisScreen.Configuration.SetupLoyaltyPromotionDiscountConfiguration()
                .SetPromotions("All")
                .SetLoyaltyDiscountOnIndividualItems("All")
                .SetDiscountVouchers(true);

            EmisScreen.Configuration.SetupOPTConfiguration()
                .ConfigureOutdoorPaymentTeminal(1, OPTTypes.s_crypto_vga_opt)
                .LinkPumpToOPT(1, 1, OPTTypes.s_crypto_vga_opt)
                .LinkPumpToOPT(2, 1, OPTTypes.s_crypto_vga_opt)
                .LinkPumpToOPT(3, 1, OPTTypes.s_crypto_vga_opt)
                .LinkChargePointToOPT(1, 1, OPTTypes.s_crypto_vga_opt)
                .LinkChargePointToOPT(2, 1, OPTTypes.s_crypto_vga_opt)
                .LinkChargePointToOPT(3, 1, OPTTypes.s_crypto_vga_opt)
                .LinkChargePointToOPT(4, 1, OPTTypes.s_crypto_vga_opt)
                .ConfigureOutdoorPaymentTeminal(2, OPTTypes.s_crypto_vga_dit)
                .LinkPumpToOPT(4, 2, OPTTypes.s_crypto_vga_dit);

            EmisScreen.Configuration.SaveConfiguration();

            //There is an open issue with the tariff names: FP-7494,
            //This could cause errors if you run the test multiple times,
            //Fixed in 59.06
            EmisScreen.Configuration.SetupTariffConfiguration()
               .AddTariff(1)
               .AddTariff(2)
               .AddTariff(3)
               .AddTariff(4);

            EmisScreen.Configuration.SaveConfiguration();

            //There is an open issue with the ChargePointConnectors
            //Causing a recovery reboot: FP-7493
            EmisScreen.Configuration.SetupChargePointConnectors()
                .AddConnector(1)
                .AddConnector(2)
                .AddConnector(3)
                .AddConnector(4);

            EmisScreen.Configuration.SaveConfigurationAndActivateForecourt();

            //-------------------------MUT FILES---------------------
            /*
           //Send in the 3 default MUT files found in P:/Testing/Mutation files
           //Send the MUT file to the remote EBOC directory via FTP
           FtpHelper.UploadFileToEBOC(Constants.MutationSourceFileDirectory + Constants.DefaultArtMutFile2Name, FtpHelper.s_primaryFtpUser);
           FtpHelper.UploadFileToEBOC(Constants.MutationSourceFileDirectory + Constants.DefaultArtMutFile3Name, FtpHelper.s_primaryFtpUser);
           //Upload the article Images found in P:\Testing\Mutation files\images\ART_KEYS
           FtpHelper.UploadAllFilesInDirectory(Constants.ImagesSourceDirectoryOnPDrive, Constants.FtpDirectoryImages, FtpHelper.s_primaryFtpUser, TimeSpan.FromSeconds(60));
           FtpHelper.UploadFileToEBOC(Constants.MutationSourceFileDirectory + Constants.DefaultKeyMutFile2Name, FtpHelper.s_primaryFtpUser);

           //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
           FtpHelper.DownloadFile(Constants.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, Constants.DefaultArtResFile2Name);
           FtpHelper.DownloadFile(Constants.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, Constants.DefaultArtResFile3Name);
           FtpHelper.DownloadFile(Constants.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, Constants.DefaultKeyResFile2Name);

            //Check the result files for errors
            //ART_MUT.002 Is currently throwing an unexpected error, still under investigation
            //Commenting it out to keep the tests running.
            //ResultFileParser.ParseResultFileForErrors(Constants.MutationResultDownloadDirectory + Constants.DefaultArtResFile2Name);
            //ResultFileParser.ParseResultFileForErrors(Constants.MutationResultDownloadDirectory + Constants.DefaultArtResFile3Name);
            //ResultFileParser.ParseResultFileForErrors(Constants.MutationResultDownloadDirectory + Constants.DefaultKeyResFile2Name);
            */

            //Close the configuration screen
            EmisScreen.Configuration.CloseConfigurationScreen();

            //The system reboots twice after this setup.
            DriverConnection.WaitUntilFuelPosIsRebooted(TimeSpan.FromMinutes(5));
            DriverConnection.WaitUntilFuelPosIsRebooted(TimeSpan.FromMinutes(5));

            DriverConnection.Reconnect(true);
        }

        [SetUp]
        public void CleanInstallTestSetup()
        {
            EmisScreen.Login();
            const string fuelPosBuildPath = @"C:\FuelPosAutomation\RELEASE\GroupFuelPos\FuelPos\60.01.0000000.0000334153";
            EmisScreen
                .GoToFuelPosSetup()
                .Install(fuelPosBuildPath);

            //STARTING FROM A CLEAN INSTALL

            EmisScreen.Login();
            EmisScreen.GoToFuelPosConfiguration();

            //There is an open issue with the ChargePointConnectors
            //Causing a recovery reboot: FP-7493
            //To check C:/FPD/RESTART.ASC via FTP we need to give our FTP user access to the FPD directory
            //To do this, we add the line 'C:\FPD\*.* /s' to C:/Pro_bo/text/SYSTEM.DLC & SYSTEM.DEL & SYSTEM.ULC
            Logger.LogInfo("Granting ourselves FTP access to C:/FPD/ to be able to check for a Recovery Reboot.");
            FtpHelper.GrantFTPAccessToFPDDirectory();
            //Cleanup the RESTART.ASC & FINISHED.ASC files before starting the test.
            FtpHelper.CleanupFPDRestartAndFinishedFiles();
        }

        [TearDown]
        public void TearDown()
        {
            //Checks the C:/FPD/RESTART.ASC for process failures and reboots
            if (DriverUtils.HasTheSystemEnteredARecoveryReboot(RecoveryRebootSettings.FrequencyToCheckForARecoveryRebootInSeconds, RecoveryRebootSettings.MaxMinutesToWaitForARecoveryReboot))
            {
                //Wait untill the recovery reboot has finished
                DriverConnection.WaitUntilRecoveryRebootIsFinished();

                //Checks if the RESTART.ASC file contains the succesmessage 'DONE'.
                DriverUtils.CheckForASuccesfullRecovery();
            }
            else
            {
                TestContext.Progress.WriteLine("The system did not enter a recovery reboot after the clean install and setup.");
            }
        }
    }
}
