﻿using NUnit.Framework;
using Utils;
using Utils.Utils.Drivers;
using Utils.Utils.FtpHelper;
using static Utils.Utils.Utils;

namespace Tests.Emis
{
    [TestFixture]
    internal class FailedProcessRecoveryTest : BaseEmisTest
    {
        private static int s_frequencyToCheckForARecoveryRebootInSeconds;
        private static int s_maxMinutesToWaitForARecoveryReboot;
        private static int s_maxMinutesToWaitUntilFuelPosIsUpdated;

        //Kills the process given as testdata and validates if FuelPos recovers correctly.
        //Succesful Recoveries and logging: cisbroker - RDM____ - LOGS___ - LOGM___
        //Succesful Recovery but empty RESTART.ASC: RDMUDP_ - DevMngr
        //!!!NO RECOVERY FROM: PWR____
        [Test]
        [TestCase("cisbroker")]
        [Order(1)]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Robin)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void FuelPosShouldRecoverAfterProcessFailure(string processNameToKill)
        {
            //To check C:/FPD/RESTART.ASC via FTP we need to give our FTP user access to the FPD directory
            //To do this, we add the line 'C:\FPD\*.* /s' to C:/Pro_bo/text/SYSTEM.DLC & SYSTEM.DEL & SYSTEM.ULC
            FtpHelper.GrantFTPAccessToFPDDirectory();

            //Cleanup the RESTART.ASC & FINISHED.ASC files before starting the test.
            FtpHelper.CleanupFPDRestartAndFinishedFiles();

            //Kills the specified process
            KillProcess(processNameToKill);

            //C:/FPD/RESTART.ASC Should log the process failure and the system should go into a recovery reboot, this method returns true if it does.
            Assert.That(
                DriverUtils.HasTheSystemEnteredARecoveryReboot(s_frequencyToCheckForARecoveryRebootInSeconds, s_maxMinutesToWaitForARecoveryReboot),
                "The system HAS NOT ENTERED a Recovery Reboot after a process was killed."
            );

            //Wait untill the recovery reboot has finished
            DriverConnection.WaitUntilRecoveryRebootIsFinished();

            //Checks if the RESTART.ASC file contains the succesmessage 'DONE'.
            DriverUtils.CheckForASuccesfullRecovery();
        }

        [SetUp]
        public void SetUp()
        {
            // The benefit of specifying the time per update is the time that is spared if it fails. It stops waiting if it succeeds.
            // This variable is the default timeout
            s_maxMinutesToWaitUntilFuelPosIsUpdated = 20;
            if (TestContext.Parameters["maxMinutesToWaitUntilFuelPosIsUpdated"] != null)
                s_maxMinutesToWaitUntilFuelPosIsUpdated = int.Parse(TestContext.Parameters["maxMinutesToWaitUntilFuelPosIsUpdated"]);

            // This variable is the default timeout to wait on a process failure
            s_maxMinutesToWaitForARecoveryReboot = 10;
            if (TestContext.Parameters["maxMinutesToWaitForProcessFailure"] != null)
                s_maxMinutesToWaitForARecoveryReboot = int.Parse(TestContext.Parameters["maxMinutesToWaitForProcessFailure"]);

            // This variable is the default timeout to wait on a process failure
            s_frequencyToCheckForARecoveryRebootInSeconds = 5;
            if (TestContext.Parameters["frequencyToCheckForARecoveryRebootInSeconds"] != null)
                s_frequencyToCheckForARecoveryRebootInSeconds = int.Parse(TestContext.Parameters["frequencyToCheckForARecoveryRebootInSeconds"]);
        }
    }
}
