﻿using System;
using NUnit.Framework;
using Resources.Constants;
using Utils;
using Utils.Utils.FtpHelper;

namespace Tests.Emis
{
    [TestFixture]
    internal class MutationFileTest : BaseEmisTest
    {
        private string _currentMutationFileName;
        private string _currentMutationFileLocalPath;
        private string _currentMutationResultFileName;

        /// <summary>
        /// Sets the local variable using the testcase testdata, for Teardown access.
        /// </summary>
        /// <param name="mutFile"></param>
        private void SetCurrentMutationVariables(string mutFile)
        {
            _currentMutationFileName = mutFile;
            _currentMutationResultFileName = _currentMutationFileName.Replace("MUT", "RES");
            _currentMutationFileLocalPath = MutationFiles.MutationSourceFileDirectory + _currentMutationFileName;
        }

        [SetUp]
        public void SetUp() => EmisScreen.Login();

        //Added articles can be found in eMis under 4-1
        //If you get errors related to carwash, you have to select 'Generic Carwash' in 9-a-1 -> Cis Peripherals
        //If you get errors relating to Unknown Card code:
        //Then for the fields CRDx in the ART_MUT make sure to give a CRD code defined in the CRDC_CardCode_MUT file.
        [Test]
        [TestCase("ART_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void ART_Articles_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files with the same name from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //It is no mistake that this MUT file has no underscore before the MUT, some MUT files are formatted this way.
        //Throws an internal communication error in the result file, this is reported but not yet closed in: FP5662-----------------
        [Test]
        [TestCase("BARDEFMUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void BARDEF_BarcodeDefinition_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files with the same name from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //TODO--------------------Find Added BNA in eMis, changes not appearing in 3-3-3-----------------------
        [Test]
        [TestCase("BNA_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void BNA_BankNote_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files with the same name from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //CardCodes can be found in eMis under 3-1-9 and in the Fuel Configuration in 9-a-1
        //Both places are checked after the MUT file is sent in.
        //Pre-condition: this test requires a configured fuel
        [Test]
        [TestCase("CRDC_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void CRDC_CardCode_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files with the same name from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);

            //Validate in eMis 3-1-9 if all Card Codes are added
            if (!EmisScreen.CardCodesScreen.ValidateNumberOfCardCodes(5))
                throw new Exception("Number of Card Codes is not as expected in the Card Code settings, see test logs.");

            EmisScreen.AttemptToReturnToEMisBaseScreen();
            EmisScreen.GoToFuelPosConfiguration();

            //Validate in eMis 9-A-1 if all Card Codes are visible in the Fuel settings
            if (!EmisScreen.Configuration.FuelConfiguration.ValidateNumberOfCardCodesInFuelSettings(5))
                throw new Exception("Number of Card Codes is not as expected in the Fuel settings, see test logs.");
        }

        //Copy of the previous testcase but covering the defective flow described in FP-7328
        //Will fail untill the defect is resolved
        //Created a seperate MUT file for this with suffix .002
        //CardCodes can be found in eMis under 3-1-9 and in the Fuel Configuration in 9-a-1
        //Both places are checked after the MUT file is sent in.
        //Pre-condition: this test requires a configured fuel
        [Test]
        [TestCase("CRDC_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void CRDC_CardCode_NEGATIVE_FLOW_FP7328_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files with the same name from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);

            //Validate in eMis 3-1-9 if all Card Codes are added
            if (!EmisScreen.CardCodesScreen.ValidateNumberOfCardCodes(2))
                throw new Exception("Number of Card Codes is not as expected in the Card Code settings, see test logs.");

            EmisScreen.AttemptToReturnToEMisBaseScreen();
            EmisScreen.GoToFuelPosConfiguration();

            //Validate in eMis 9-A-1 if all Card Codes are visible in the Fuel settings
            if (!EmisScreen.Configuration.FuelConfiguration.ValidateNumberOfCardCodesInFuelSettings(2))
                throw new Exception("Number of Card Codes is not as expected in the Fuel settings, see test logs.");
        }

        //It is no mistake that this MUT file has no underscore before the MUT, some MUT files are formatted this way.
        //TODO------------------------------Find CardIdentification settings in eMis------------------------------------
        [Test]
        [TestCase("CRDIDMUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void CRDID_CardIdentification_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files with the same name from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Resources/Mutation Files/ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added currency information can be found in eMis under 5-1
        //Pre-condition: the checkbox 'Discount Vouchers' has to be checked under 7-A-Discount Vouchers Tab
        [Test]
        [TestCase("CUR_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void CUR_Currencies_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added local customers can be found in eMis under 5-3-1
        [Test]
        [TestCase("CUST_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void CUST_Customers_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added discount vouchers can be found in eMis under 7-7
        [Test]
        [TestCase("DV_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void DV_DiscountVouchers_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added Extra Payment Modes can be found in eMis under 5-4
        //but to enable this check the 'Extra Payment Modes' Checkbox in 5-A
        [Test]
        [TestCase("EPM_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void EPM_ExtraPaymentModes_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

         //TODO------------------------------------Find added Groups in eMis settings------------------------
        [Test]
        [TestCase("GRP_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void GRP_GroupCode_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added Receipt and Expense Types can be found in eMis under 7-2 & 7-3 respectively
        [Test]
        [TestCase("IO_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void IO_CashInOut_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //The Key Mut file links articles to buttons in eFui & SCO
        //the button links can be found in eMis under 4-2-1 & 4-2-2 respectively
        [Test]
        [TestCase("KEY_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void KEY_ArticleButtons_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Litre Coupons are found in eMis under 5-5
        //Pre-condition check the 'Litre Coupons' checkbox under 5-A
        //Found Defect FP-7212 - Defect is resolved, test should pass for the Fixed version.
        [Test]
        [TestCase("LC_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void LC_LitreCoupon_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);

            //Validate in eMis 5-5 if all 10 Litre Coupons are added
            if (!EmisScreen.LitreCouponScreen.ValidateNumberOfLitreCoupons(10))
                throw new Exception("Number of Litre Coupons is not as expected, see test logs.");
        }

        //TODO -------------------FIND MERCHANT SETTINGS IN eMiS-------------------------------------------
        [Test]
        [TestCase("MER_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void MER_Merchants_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added OPT settings can be found in eMIs under 3-3-1 when on the 'OPT CONFIGURATION' list item, in the receipt tab.
        //TODO--------------------------INVESTIGATE ERRORS OCCURING DUE TO '[LOYALTY_TEXT_OPT]' BLOCKS in the CLEANUP FILE------------------
        [Test]
        [TestCase("OPT_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void OPT_OutdoorPaymentTerminals_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //TODO--------------FIND OUT HOW TO ENABLE 3-1-2 in eMis to view and confirm the Pump Discount Criteria------------------
        [Test]
        [TestCase("PMPD_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void PMPD_PumpDiscountCriteria_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Editted Pos information can be found in eMis in 9-a-1 under Station information (possibly in other places too)
        [Test]
        [TestCase("POS_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void POS_PosSettings_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Station settings can be found in various menus in eMis.
        //The website information can be found under 1-5 (Only visible after executing the test
        [Test]
        [TestCase("STAT_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void STAT_Station_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //Added users can be found in eMis under 1-3-1
        //Pre-condition: give an 'External Reference' of 1 to the 'STATION  MANAGER'(or any other) profile in eMis 1-3-2
        [Test]
        [TestCase("USR_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void USR_Users_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //The updated VAT information can be found in eMis under 7-1
        [Test]
        [TestCase("VAT_MUT.001")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void VAT_TaxSettings_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);
            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        //The payment vouchers can be found in eMis under 5-7
        //Pre-condition: check 'Off-line payment vouchers' in 5-A
        [Test]
        [TestCase("VCH_MUT.002")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void VCH_PaymentVouchers_MUT(string mutFile)
        {
            //Uses the testcase testdata to set the variables: currentMutationFileName,
            //currentMutationResultFileName, currentMutationFileLocalPath, for Teardown access.
            SetCurrentMutationVariables(mutFile);

            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            //Send the MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(_currentMutationFileLocalPath, FtpHelper.s_primaryFtpUser);

            //Download the remote Mutation Result file into the local Resources/Mutation Files/ ResultFileDownloads folder.
            FtpHelper.DownloadFile(MutationFiles.MutationResultDownloadDirectory, Constants.FtpDirectoryEBOC, _currentMutationResultFileName);

            ResultFileParser.ParseResultFileForErrors(MutationFiles.MutationResultDownloadDirectory + _currentMutationResultFileName);
        }

        [TearDown]
        public void Cleanup()
        {
            //Clear any existing MUT and RES files from the remote EBOC directory before sending the new file.
            FtpHelper.DeleteFile(_currentMutationFileName, Constants.FtpDirectoryEBOC);
            FtpHelper.DeleteFile(_currentMutationResultFileName, Constants.FtpDirectoryEBOC);

            TestContext.Progress.Write("Transferring Cleanup file to remove all the added articles.");
            //Send the Cleanup MUT file to the remote EBOC directory via FTP
            FtpHelper.UploadFileToEBOC(MutationFiles.MutationCleanupFileDirectory + _currentMutationFileName, FtpHelper.s_primaryFtpUser);

            //Wait for the Cleanup result file to exist before ending the test
            FtpHelper.WaitForFileToExistInRemoteDirectory(Constants.FtpDirectoryEBOC, _currentMutationResultFileName, 60);

            //Some MUT files require specific Teardown steps or additional validations after the cleanup.
            EmisScreen.SpecificCleanupStepsPerMUTFile(_currentMutationFileName);

            //If a previous testcase has ended inside one of the eMis menus, by design or because of an error
            //Then this method can be used to ATTEMPT to resset the system back to the eMis BaseScreen
            EmisScreen.AttemptToReturnToEMisBaseScreen();
        }
    }
}
