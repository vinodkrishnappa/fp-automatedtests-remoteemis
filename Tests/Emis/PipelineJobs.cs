﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Utils;
using Utils.Utils;
using Utils.Utils.Drivers;
using Utils.Utils.VboxUtils;

namespace Tests.Emis
{
    [TestFixture]
    public class PipelineJobs : BaseEmisTest
    {
        private static List<string> s_fuelPosVersionsToUpdateFrom;
        private static string s_versionToUpdateToFullBuildNumber;
        private static List<string> s_vmNamesToUpdateFrom;

        /// <summary>
        /// Nunit requires at least one constructor without parameters
        /// </summary>
        public PipelineJobs() : this(null) { }

        public PipelineJobs(DriverOptions driverOptions = null) : base(driverOptions ?? PipelineJobsDefaultDriverOptions)
        {
        }

        public static DriverOptions PipelineJobsDefaultDriverOptions
        {
            get
            {
                DriverOptions options = EmisDefaultDriverOptions;
                options.ConnectAfterFirstInitialisation = false;
                options.VboxOptions.RestoreDefaultSnapshotBeforeConnect = false;
                return options;
            }
        }

        /// <summary>
        /// Will start an UpdateTest for the given version
        /// Meaning updates will be performed from a specified amount of versions back from the version given
        /// Based on default snapshots, to the version given
        /// Some basic validations will be done to confirm the update has performed as expected.
        /// </summary>
        [Test]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Jim)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void UpdateTest()
        {
            // TODO: create a test config.json for test parameters, for easy use in teamcity
            int majorVersionToUpdateTo = 60;
            int minorVersionToUpdateTo = 01;
            bool releaseBuild = false;

            if (TestContext.Parameters.Exists(nameof(majorVersionToUpdateTo)))
                majorVersionToUpdateTo = int.Parse(TestContext.Parameters[nameof(majorVersionToUpdateTo)]);
            if (TestContext.Parameters.Exists(nameof(minorVersionToUpdateTo)))
                minorVersionToUpdateTo = int.Parse(TestContext.Parameters[nameof(minorVersionToUpdateTo)]);
            if (TestContext.Parameters.Exists(nameof(releaseBuild)))
                releaseBuild = bool.Parse(TestContext.Parameters[nameof(releaseBuild)]);

            //Gets the latest build number of the specified major and minor version, if the bool is true it will search in RELEASE instead of ReadyToTest
            s_versionToUpdateToFullBuildNumber = JFrogHelper.GetLatestBuildNumberFromJFrog(majorVersionToUpdateTo, minorVersionToUpdateTo, releaseBuild);

            // Update
            // TODO: in the future when jfrog supports FTP, packages won't have to be downloaded locally anymore
            JFrogHelper.DownloadFuelPosPackages(s_versionToUpdateToFullBuildNumber, releaseBuild);

            //Lists of the versions to update from, counting back the set amount of versions from the version we are testing
            s_fuelPosVersionsToUpdateFrom = GetMajorVersionsToUpdateFrom(majorVersionToUpdateTo);
            //Lists a corresponding VM name for each version in 's_fuelPosVersionsToUpdateFrom'
            s_vmNamesToUpdateFrom = VBoxManager.GetListOfSnapshotsToUpdateFrom(s_fuelPosVersionsToUpdateFrom);

            s_vmNamesToUpdateFrom.ForEach(
                currentVmToUpdateFrom =>
                {
                    //The VM name should be the same as the default snapshot name
                    //Restoring it before each test to start from a clean snapshot
                    s_vbox.RestoreSnapshot(currentVmToUpdateFrom);
                    s_vbox.Start();
                    DriverConnection.Reconnect(true);

                    Emis.UpdateTest.FuelPosVersion_ShouldBeCorrectAfterUpdating(
                        majorVersionToUpdateTo,
                        minorVersionToUpdateTo,
                        s_versionToUpdateToFullBuildNumber,
                        currentVmToUpdateFrom,
                        false
                    );
                }
            );

            TeamCityArrayBuilder.BuildArray("vbox-snapshots");
        }

        private static List<string> GetMajorVersionsToUpdateFrom(int majorVersionToUpdateTo)
        {
            int excludedVersionCounter = 0;
            string[] toSkip = Constants.FuelPosVersionsToSkip;
            string[] toAlwaysInclude = Constants.FuelPosVersionsToAlwaysInclude;
            const int versionsToGoBack = Constants.FuelPosVersionsToGoBack;

            //List that holds all versions we should update from
            List<string> versionsToUpdateFrom = new List<string>();

            //Add all the mandatory versions to include
            toAlwaysInclude.ToList()
                           .ForEach(version => versionsToUpdateFrom.Add(version));

            //Add the number of versions that we have to go back, counting back from the version to update to,
            //excluding the versions that are excluded (58)
            for (int numberOfVersionsToGoBack = versionsToGoBack; numberOfVersionsToGoBack > 0; numberOfVersionsToGoBack--)
            {
                int currentVersion = majorVersionToUpdateTo - numberOfVersionsToGoBack;

                if (toSkip.Contains(currentVersion.ToString()))
                    excludedVersionCounter++;
                else
                    versionsToUpdateFrom.Add(currentVersion.ToString());
            }

            //For each version that is skipped in the initial 7 that we count back, we add another lower version to the list
            for (int numberOfVersionsToGoLower = 1; numberOfVersionsToGoLower < excludedVersionCounter + 1; numberOfVersionsToGoLower++)
            {
                int currentVersion = majorVersionToUpdateTo - versionsToGoBack - numberOfVersionsToGoLower;

                if (toSkip.Contains(currentVersion.ToString()))
                    excludedVersionCounter++;
                else
                    versionsToUpdateFrom.Add(currentVersion.ToString());
            }

            versionsToUpdateFrom.Sort();
            Logger.LogInfo($"Major versions to update from: [{string.Join(", ", versionsToUpdateFrom.ToArray())}]");
            return versionsToUpdateFrom;
        }
    }
}
