﻿using NUnit.Framework;
using Utils.Utils.Enums;

namespace Tests.Emis
{
    [TestFixture]
    internal class ReleaseTest : BaseEmisTest
    {
        [Test]
        public void ConfigureFuelPOS()
        {
            EmisScreen.Login();
            EmisScreen.GoToFuelPosConfiguration();
            EmisScreen.Configuration.SetupTankGroupsAndNozzlesConfiguration(FillingTypes.s_super);
            //emisScreen.SetupPumpsConfiguration();
        }
    }
}
