﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Resources.Constants.Images;
using Utils;
using Utils.Utils;
using Utils.Utils.ConfigurationManager;
using Utils.Utils.Drivers;
using Utils.Utils.FtpHelper;
using Utils.Utils.SettingsInterfaces;
using static Utils.Utils.ImageHandler;
using static Utils.Utils.Utils;

namespace Tests.Emis
{
    [TestFixture]
    internal class UpdateTest : BaseEmisTest
    {
        private static bool s_driverProblemsArePresent;
        private static IRecoveryRebootSettings RecoveryRebootSettings => ConfigurationManager.Config.RecoveryRebootSettings;

        [Test]
        [Order(2)]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Robin)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public static void FuelPosVersion_ShouldBeCorrectAfterUpdating(
            int majorVersionToUpdateTo,
            int minorVersionToUpdateTo,
            string versionToUpdateToFullBuildNumber,
            string vmNameToUpdateFrom,
            bool runAsSingleTest = true
        )
        {
            string minorVersionToUpdateToString = minorVersionToUpdateTo.ToString();
            if (minorVersionToUpdateTo.ToString().Length == 1)
                minorVersionToUpdateToString = $"0{minorVersionToUpdateTo}";

            // Every test in this class should only run if rebooting works correctly
            Assert.False(
                s_driverProblemsArePresent,
                "Not running this test, reason: 'RebootingFuelPos_ShouldNotCauseAnyIssuesWithTheDrivers' test failed"
            );
            Logger.LogInfoHeaderSection($"Starting Update Test - Updating snapshot: {vmNameToUpdateFrom} - To version: {versionToUpdateToFullBuildNumber}");
            // Login to emis
            EmisScreen.Login();

            Dictionary<string, string> fuelPosDetailsFromEmis = EmisScreen.GetFuelPosDetails();
            string fuelPosVersionFullBeforeUpdate = fuelPosDetailsFromEmis["FuelPosFullVersion"];
            string fuelPosVersionShortBeforeUpdate = fuelPosDetailsFromEmis["FuelPosVersionShort"];

            Assert.Less(
                fuelPosVersionFullBeforeUpdate,
                versionToUpdateToFullBuildNumber,
                "The fuelPosVersion to update to can't be the same or greater than the currently installed fuelPos version"
            );

            // Close shift if open (update won't activate immediatly with open shift)
            EmisScreen.OpenEfui();
            EfuiScreen.CloseShift();

            FtpHelper.UploadPackagesToAcivate(versionToUpdateToFullBuildNumber);

            // FuelPos sometimes breaks the connection during an upgrade, verify that we still have a connection
            DriverConnection.WaitOnFuelPosPing(TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(5));

            // Verify that installation is in progress
            WaitTillImageIsFoundRetryWhenConnectionCloses(EfuiImages.InstallationInProgress, 3, TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(3), EfuiScreen);

            // Wait on update
            DriverConnection.WaitOnFuelPosUpdateReboot(TimeSpan.FromMinutes(Constants.MaxMinutesToWaitUntilFuelPosIsUpdated));

            //To check C:/FPD/RESTART.ASC via FTP we need to give our FTP user access to the FPD directory
            //To do this, we add the line 'C:\FPD\*.* /s' to C:/Pro_bo/text/SYSTEM.DLC & SYSTEM.DEL & SYSTEM.ULC
            Logger.LogInfo("Granting ourselves FTP access to C:/FPD/ to be able to check for a Recovery Reboot.");
            FtpHelper.GrantFTPAccessToFPDDirectory();
            //Cleanup the RESTART.ASC & FINISHED.ASC files before starting the test.
            FtpHelper.CleanupFPDRestartAndFinishedFiles();

            //Checks the C:/FPD/RESTART.ASC for process failures and reboots
            if (DriverUtils.HasTheSystemEnteredARecoveryReboot(RecoveryRebootSettings.FrequencyToCheckForARecoveryRebootInSeconds, RecoveryRebootSettings.MaxMinutesToWaitForARecoveryReboot))
            {
                //Wait untill the recovery reboot has finished
                DriverConnection.WaitUntilRecoveryRebootIsFinished();

                //Checks if the RESTART.ASC file contains the succesmessage 'DONE'.
                DriverUtils.CheckForASuccesfullRecovery();
            }
            else
            {
                Logger.LogInfo("The system did not enter a recovery reboot after the update.");
            }

            // Login so that we can verify that the installed fuelPos version is correct
            EfuiScreen.OpenEmis();
            EmisScreen.Login();

            // Verify FuelPos version
            Dictionary<string, string> updatedEmisDetails = EmisScreen.GetFuelPosDetails();
            string updatedFuelPosFullVersion = EmisScreen.GetFuelPosDetails()["FuelPosFullVersion"];
            Assert.That(
               versionToUpdateToFullBuildNumber.Equals(updatedFuelPosFullVersion),
               "The fuelPos does not have the expected version after updating. " +
               $"\n Expected: {versionToUpdateToFullBuildNumber} \n Received: {updatedFuelPosFullVersion}"
            );

            string snapshotName = $"{Constants.SnapshotUpdatedPrefix}{majorVersionToUpdateTo}.{minorVersionToUpdateToString}{Constants.SnapshotSuffix}_{GetDateLocalTime("HH:mm-dd/MM/yyyy")}";

            s_vbox.MakeSnapshot(
                snapshotName,
                $"Update test snapshot after update: {fuelPosVersionFullBeforeUpdate} to version {updatedFuelPosFullVersion}"
            );

            TeamCityArrayBuilder.AddValueToArray("vbox-snapshots", snapshotName);
            if (runAsSingleTest)
                TeamCityArrayBuilder.BuildArray("vbox-snapshots");
            Logger.LogInfoHeaderSection($"Finished Update Test - Succesfully updated from version: {fuelPosVersionFullBeforeUpdate} - to version: {updatedFuelPosFullVersion} (version verified)!");
        }

        // This might look more than a method than a test, but it verifies of these functions work correctly
        // It is important functionality for the update tests. Tests shouldn't take resources/time if it isn't needed.
        [Test]
        [Order(1)]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Robin)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void RebootingFuelPos_ShouldNotCauseAnyIssuesWithTheDrivers()
        {
            //Reset bool
            s_driverProblemsArePresent = false;
            try
            {
                // Check if rebooting and reconnecting to the driver works
                EmisScreen.Reboot();
                EmisScreen.Login();
            }
            catch (Exception e)
            {
                // Make sure that the update tests don't run
                s_driverProblemsArePresent = true;

                // Fail the test
                Assert.Fail($"Rebooting and reconnecting to FuelPos failed \n {Logger.GetExceptionInfo(e)}");
            }
        }
    }
}
