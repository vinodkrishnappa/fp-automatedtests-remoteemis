﻿using NUnit.Framework;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Components.Windows.Simulator;
using Utils.Utils.Drivers;

namespace Tests.Helpers
{
    [TestFixture]
    public class SimulatorHelpers
    {
        private DriverConnection _driverConnection;

        [Test]
        public void LaunchSimulators()
        {
            // Create driverConnection
            _driverConnection = new DriverConnection(
                new DriverOptions(
                    mainWindows: new Window[] { MainWindows.Efui, MainWindows.Emis },
                    simulatorWindows: new Window[] { SimulatorWindows.Mux, SimulatorWindows.Oase, SimulatorWindows.PinPad },
                    vboxOptions: null // if you pass null here, the defaults are used
                )
            );

            // Make connection
            _driverConnection.Connect();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => DriverConnection.CloseConnection();
    }
}
