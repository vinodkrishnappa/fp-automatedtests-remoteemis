﻿using NUnit.Framework;
using Utils.Components.Screens.SelfCheckout;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Utils.Drivers;

namespace Tests.Helpers
{
    [TestFixture]
    public class SelfCheckoutHelpers
    {
        private DriverConnection _driverConnection;

        [OneTimeTearDown]
        public void OneTimeTearDown() => DriverConnection.CloseConnection();

        [Test]
        public void SelfCheckOut_OpenEfui()
        {
            // Create driverConnection
            _driverConnection = new DriverConnection(
                new DriverOptions(
                    mainWindows: new Window[] { MainWindows.Efui, MainWindows.Emis, MainWindows.SelfCheckout },
                    vboxOptions: null // if you pass null here, the defaults are used
                )
            );

            // Make connection
            _driverConnection.Connect();

            // Create screen
            DriverConnection.GetScreen<SelfCheckoutMainScreen>().OpenEfui();
        }
    }
}
