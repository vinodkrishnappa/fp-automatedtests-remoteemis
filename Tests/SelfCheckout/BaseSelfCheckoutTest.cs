﻿using NUnit.Framework;
using Utils.Components.Screens.Efui;
using Utils.Components.Screens.Emis;
using Utils.Components.Screens.SelfCheckout;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Screens;
using Utils.Utils.Drivers;

namespace Tests.SelfCheckout
{
    [TestFixture]
    public abstract class BaseSelfCheckoutTest : BaseTest
    {
        #region Screens

        protected static EfuiMainScreen EfuiScreen => DriverConnection.GetScreen<EfuiMainScreen>();
        protected static EmisMainScreen EmisScreen => DriverConnection.GetScreen<EmisMainScreen>();
        protected static MuxScreen MuxScreen => DriverConnection.GetScreen<MuxScreen>();
        protected static SelfCheckoutMainScreen SelfCheckoutScreen => DriverConnection.GetScreen<SelfCheckoutMainScreen>();

        #endregion Screens

        /// <summary>
        /// Nunit requires at least one constructor without parameters
        /// </summary>
        protected BaseSelfCheckoutTest() : this(null) { }

        protected BaseSelfCheckoutTest(DriverOptions driverOptions = null) : base(driverOptions ?? SelfCheckoutDefaultDriverOptions)
        {
        }

        public static DriverOptions SelfCheckoutDefaultDriverOptions => new DriverOptions(
            mainWindows: new Window[] { MainWindows.Efui, MainWindows.Emis, MainWindows.SelfCheckout },
            vboxOptions: null // if you pass null here, the defaults are used
        );
    }
}
