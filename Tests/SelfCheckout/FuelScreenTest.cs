﻿using NUnit.Framework;
using Utils;
using Utils.Utils.Enums;

namespace Tests.SelfCheckout
{
    [TestFixture]
    public class FuelScreenTest : BaseSelfCheckoutTest
    {
        // The pay button will only throw a popup when the pinPadSimulator is running.
        [Test]
        [Property("Priority", 0)]
        [TestCase("Cola", Constants.FuelNameSuper)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void GetFuelAndColaTest(string itemToAdd, FillingTypes fuelType)
        {
            // Get fuel
            MuxScreen.GetFuel(1);

            // Do action (tests are verified in the methods itself)
            // Open fuel screen
            SelfCheckoutScreen.FuelScreen
                // Select pump 1, click on button with fuelType
                .FuelSelector.SelectPump(1).ClickButtonByFillingType(fuelType)
                // Select item
                .SelectArticlesWithoutBarCode().ArticleBasket.AddItemToBasket(itemToAdd)
                // Pay
                .PayWithCreditCardNoReceipt();
        }

        // The pay button will only throw a popup when the pinPadSimulator is running.
        [Test]
        [Property("Priority", 0)]
        [TestCase(Constants.FuelNameSuper)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-")]
        public void GetFuelTest(FillingTypes fuelType)
        {
            // Setup
            // Get fuel
            MuxScreen.GetFuel(1);

            // Do action (tests are verified in the methods itself)
            // Open fuel screen
            SelfCheckoutScreen.FuelScreen
                // Select pump 1, click on button with fuelType
                .FuelSelector.SelectPump(1).ClickButtonByFillingType(fuelType)
                // Pay
                .ArticleBasket.PayWithCreditCardNoReceipt();
        }

        /// <summary>
        /// Verify that we are on the start screen
        /// </summary>
        [TearDown]
        public void Teardown() => SelfCheckoutScreen.VerifyIfElementIsVisible(SelfCheckoutScreen.WelcomeLabel);
    }
}
