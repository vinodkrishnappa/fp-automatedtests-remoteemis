﻿using System;
using NUnit.Framework;
using Resources.Constants.Images;
using Utils;
using Utils.Components.Components.SelfCheckout;
using Utils.Components.MenuBars.SelfCheckout;
using Utils.Components.Screens.Emis.EmisDependencies;
using Utils.Components.Screens.SelfCheckout;
using static Utils.Utils.ImageHandler;

namespace Tests.SelfCheckout
{
    [TestFixture]
    public class SalesScreenTest : BaseSelfCheckoutTest
    {
        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-6991")]
        public void ExitSCOMode(string itemToAdd)
        {
            // To efui and back
            SelfCheckoutScreen.MenuBar.OpenEFUI();
            EfuiScreen.OpenSelfCheckout();

            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;

            // Add item
            articleBasket.AddItemToBasket(itemToAdd);
            double amountToPay = articleBasket.GetAmountToPay();

            // To efui and back
            SelfCheckoutScreen.MenuBar.OpenEFUI();
            EfuiScreen.OpenSelfCheckout();

            // Verify that total to be paid hasn't changed
            Assert.AreEqual(
                amountToPay,
                articleBasket.GetAmountToPay(),
                "The total amount to pay isn't the same after switching back from eFui to SCO"
            );

            // Navigate back to start screen
            articleBasket.CancelTransactionPopUp.SelectYes();
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-7217")]
        public void IncreasingNumberOfProductsShouldIncreasePrice(string itemToAdd)
        {
            // Open article screen
            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;

            // Add item to basket
            articleBasket.AddItemToBasket(itemToAdd);
            _extentReportsHelper.Log("Added item to basket");
            double amountToPayForOneCola = articleBasket.GetAmountToPay();

            // Add second item to basket
            articleBasket.AddItemToBasket(itemToAdd);
            _extentReportsHelper.Log("Added second item to basket");

            _extentReportsHelper.Log("Verify that the price of two items is the same as the double of the price of one item");
            // Verify that the price of two items is the same as the double of the price of one item
            // (no promotions are active)
            Assert.AreEqual(
                articleBasket.GetAmountToPay(),
                amountToPayForOneCola * 2,
                $"The amount to pay for two {itemToAdd}'s isn't equal to the double of the price of one {itemToAdd}"
            );

            // Navigate back to start screen
            articleBasket.CancelTransactionPopUp.SelectYes();
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-7197")]
        public void RemovingAllProductsShouldReturnUserToStartScreen(string itemToAdd)
        {
            // Add item to basket
            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            _extentReportsHelper.Log("Article screen clicked");

            articleBasket.AddItemToBasket(itemToAdd);
            _extentReportsHelper.Log("Add item to basket");
            Assert.Greater(articleBasket.GetAmountToPay(), 0, $"The price of {itemToAdd} is lower than zero");

            // Remove item from basket
            ClickByImage(CommonImages.CloseX, SelfCheckoutScreen);
            _extentReportsHelper.Log("Remove item from basket");
            SelfCheckoutScreen.VerifyIfElementIsVisible(SelfCheckoutScreen.WelcomeLabel);
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-7126")]
        public void RequestHelp(string itemToAdd)
        {
            // Open help on start screen
            SelfCheckoutScreen.MenuBar.OpenAndCloseHelpPopUp();

            // Open help on sales screen
            SalesScreen salesScreen = SelfCheckoutScreen.SalesScreen;
            SelfCheckoutScreen.MenuBar.OpenAndCloseHelpPopUp();

            // Open help on articleScreen & //TODO: transaction screen
            ArticleBasketComponent articleBasket = salesScreen.ArticleBasket;
            articleBasket.AddItemToBasket(itemToAdd);
            SelfCheckoutScreen.MenuBar.OpenAndCloseHelpPopUp();
            articleBasket.PayWithCreditCardNoReceipt();

            // Navigate back to start screen
            articleBasket.CancelTransactionPopUp.SelectYes();
            _extentReportsHelper.Log("Request Help Test Passed");
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-7198")]
        public void TransactionTimeout_ShouldReturnUserToWelcomeScreen(string itemToAdd)
        {
            // Go to article screen
            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;

            // Add article
            articleBasket.AddItemToBasket(itemToAdd);

            // User should return to welcome screen after being idle for too long
            SelfCheckoutScreen.WaitUntilElementExists(
                () => SelfCheckoutScreen.WelcomeLabel,
                nameof(SelfCheckoutScreen.WelcomeLabel),
                TimeSpan.FromMinutes(3)
            );
        }

        [Test]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC-6990")]
        public void VerifyArticleButtons()
        {
            // Navigate to efui
            SelfCheckoutScreen.MenuBar.OpenEFUI();

            // Open Emis
            EfuiScreen.OpenEmis();

            // Login
            EmisScreen.Login();
            EmisScreen.AddArticles(new Article("Cheetos", 123456));
        }

        [Test]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC-6979")]
        public void VerifyCancelFillingSelection()
        {
            // Go to filling screen and select pump 1
            FuelSelectorComponent<FuelScreen> fuelSelector = SelfCheckoutScreen.FuelScreen.FuelSelector;
            fuelSelector.SelectPump(1);

            // Cancel transaction
            fuelSelector.ArticleBasket.CancelTransactionPopUp.SelectYes();
            SelfCheckoutScreen.VerifyIfElementContainsSpecifiedText(fuelSelector.SelectYourPumpLabel, Constants.SelectYourPump);
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-6985")]
        public void VerifyChangeQuantityinselfcheckout(string itemToAdd)
        {
            // Navigate to salesscreen
            SalesScreen salesScreen = SelfCheckoutScreen.SalesScreen;
            // Add article
            ArticleBasketComponent articleBasket = salesScreen.ArticleBasket;
            articleBasket.AddItemToBasket(itemToAdd);
            double amountToPayForItem = articleBasket.GetAmountToPay();

            // Increase the amount of that specific article
            ClickByImage(SelfCheckoutImages.Plus, SelfCheckoutScreen);
            double amountToPayForItem2 = salesScreen.ArticleBasket.GetAmountToPay();
            Assert.AreEqual(amountToPayForItem2, amountToPayForItem * 2, "Amount to pay should be equal");

            // Decrease the amount of that specific article
            ClickByImage(SelfCheckoutImages.Minus, SelfCheckoutScreen);
            Assert.AreEqual(salesScreen.ArticleBasket.GetAmountToPay(), amountToPayForItem);

            // Select same article from the shop agian should increase the price
            articleBasket.AddItemToBasket(itemToAdd);
            double amountToPayForItem3 = salesScreen.ArticleBasket.GetAmountToPay();
            Assert.AreEqual(amountToPayForItem3, amountToPayForItem * 2);

            // Cancel transaction
            articleBasket.CancelTransactionPopUp.SelectYes();
        }

        [Test, TestCase]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-6977")]
        public void VerifynotificationatRequestHelp()
        {
            // Language- English
            SelfCheckoutScreen.MenuBar.SetLanguage(1).OpenAndCloseHelpPopUp("english");
            _extentReportsHelper.Log("English language selected");

            // Language- French
            SelfCheckoutScreen.MenuBar.SetLanguage(2).OpenAndCloseHelpPopUp("french");
            _extentReportsHelper.Log("French language selected");

            // Language- German
            SelfCheckoutScreen.MenuBar.SetLanguage(3).OpenAndCloseHelpPopUp("german");
            _extentReportsHelper.Log("German language selected");

            // Language- Dutch
            SelfCheckoutScreen.MenuBar.SetLanguage(4).OpenAndCloseHelpPopUp("dutch");
            _extentReportsHelper.Log("Dutch language selected");
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC-6989")]
        public void VerifySelfcheckoutBackwardnavigations(string itemToAdd)
        {
            // Add article
            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            articleBasket.AddItemToBasket(itemToAdd);
            double amountToPayForItem = articleBasket.GetAmountToPay();
            articleBasket.CancelTransactionPopUp.SelectYes();

            Assert.AreEqual(articleBasket.GetAmountToPay(), amountToPayForItem);
            articleBasket.CancelTransactionPopUp.SelectYes();

            // Add article
            articleBasket.AddItemToBasket(itemToAdd);
            articleBasket.CancelTransactionPopUp.SelectYes();
            Assert.AreEqual(articleBasket.GetAmountToPay(), amountToPayForItem);
            _extentReportsHelper.Log("Verify Selfcheckout Backward navigation Test Passed");
        }

        [Test]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC-6994")]
        public void VerifySelfcheckoutInitializationoptions() => Assert.True(SelfCheckoutScreen.VerifyInitializationOptions());

        [Test]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC-6995")]
        public void VerifySelfcheckoutWithoutActiveShift()
        {
            // Navigate to efui
            SelfCheckoutScreen.MenuBar.OpenEFUI();
            // Open selfcheckout without an open shift
            EfuiScreen.CloseShift();
            EfuiScreen.OpenSelfCheckout();
            // Execute till closed tests
            Assert.True(SelfCheckoutScreen.IsTillClosed(), "Till isn't closed");

            // Navigate to efui
            SelfCheckoutScreen.MenuBar.OpenEFUI();
            // Open selfcheckout with an open shift
            EfuiScreen.OpenShift();
            EfuiScreen.OpenSelfCheckout();
        }

        [Test, TestCase]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-6976")]
        public void VerifyStartSelfcheckoutMode()
        {
            // Navigate to efui
            SelfCheckoutScreen.MenuBar.OpenEFUI();

            // Back to SCO
            EfuiScreen.OpenSelfCheckout();
            _extentReportsHelper.Log("StartSelfCheckOut clicked");

            SelfCheckoutScreen.VerifyIfElementIsVisible(SelfCheckoutScreen.WelcomeLabel);
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC-6992")]
        public void VerifySyncTransactionFuelPOSSelfcheckout(string itemToAdd)
        {
            // Add article
            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            double amountToPay = articleBasket.AddItemToBasket(itemToAdd).GetAmountToPay();

            // TODO: Verify of it is also in eFUI (can't be done now, because only checkable through image recognition)

            // Check if both amounts are equal

            // Go back to selfCheckout startscreen
            articleBasket.CancelTransactionPopUp.SelectYes();
        }

        [Test]
        [TestCase("Cola")]
        [Property("Priority", 0)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-7215")]
        public void VerifyThatBasketButtonsWorkCorrectly(string itemToAdd)
        {
            // Go to article screen
            SalesScreen salesScreen = SelfCheckoutScreen.SalesScreen;
            // Add article
            ArticleBasketComponent articleBasket = salesScreen.ArticleBasket;
            articleBasket.AddItemToBasket(itemToAdd);
            double amountToPayForItem = salesScreen.ArticleBasket.GetAmountToPay();

            // Increase the amount of that specific article
            ClickByImage(SelfCheckoutImages.Plus, SelfCheckoutScreen);
            Assert.AreEqual(articleBasket.GetAmountToPay(), amountToPayForItem * 2);

            // Decrease the amount of that specific article
            ClickByImage(SelfCheckoutImages.Minus, SelfCheckoutScreen);
            Assert.AreEqual(articleBasket.GetAmountToPay(), amountToPayForItem);

            // Remove the specific article from the basket
            ClickByImage(CommonImages.CloseX, SelfCheckoutScreen);
            SelfCheckoutScreen.VerifyIfElementIsVisible(SelfCheckoutScreen.WelcomeLabel);
        }

        [Test, TestCase]
        [Property("Priority", 0)]
        [Author(Constants.Attribute_Author_Vinod)]
        [Property(Constants.Attribute_TestCaseId, "TC‌-6973")]
        public void VerifyUserAbleToSwitchLanguages()
        {
            SCOMenuBarComponent menuBarComponent = SelfCheckoutScreen.MenuBar;

            // Language- English
            menuBarComponent.SetLanguage(1).OpenAndCloseHelpPopUp();
            _extentReportsHelper.Log("English language selected");
            ArticleBasketComponent articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            SelfCheckoutScreen.VerifyIfElementContainsSpecifiedText(articleBasket.SelectYourPreferenceTxt, Constants.EnglishSelectYourPreference);
            articleBasket.CancelTransactionPopUp.SelectYes();
            _extentReportsHelper.Log("Text label verified");

            // Language- French
            menuBarComponent.SetLanguage(2).OpenAndCloseHelpPopUp();
            _extentReportsHelper.Log("French language selected");
            articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            SelfCheckoutScreen.VerifyIfElementContainsSpecifiedText(articleBasket.SelectYourPreferenceTxt, Constants.FrenchSelectYourPreference);
            articleBasket.CancelTransactionPopUp.SelectYes();
            _extentReportsHelper.Log("Text label verified");

            // Language- German
            menuBarComponent.SetLanguage(3).OpenAndCloseHelpPopUp();
            _extentReportsHelper.Log("German language selected");
            articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            SelfCheckoutScreen.VerifyIfElementContainsSpecifiedText(articleBasket.SelectYourPreferenceTxt, Constants.GermanSelectYourPreference);
            articleBasket.CancelTransactionPopUp.SelectYes();
            _extentReportsHelper.Log("Text label verified");

            // Language- Dutch
            menuBarComponent.SetLanguage(4).OpenAndCloseHelpPopUp();
            _extentReportsHelper.Log("Dutch language selected");
            articleBasket = SelfCheckoutScreen.SalesScreen.ArticleBasket;
            SelfCheckoutScreen.VerifyIfElementContainsSpecifiedText(articleBasket.SelectYourPreferenceTxt, Constants.DutchSelectYourPreference);
            articleBasket.CancelTransactionPopUp.SelectYes();
            _extentReportsHelper.Log("Text label verified");
        }
    }
}
