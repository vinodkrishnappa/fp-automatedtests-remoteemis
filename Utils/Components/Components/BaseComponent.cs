﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Windows;
using Utils.Utils;

namespace Utils.Components.Components
{
    public class BaseComponent : ScreenHelpers, IHasParentWindow
    {
        private readonly Func<AppiumDriver<WindowsElement>> _getAppiumDriver;

        public BaseComponent(BaseScreen parent)
        {
            ParentWindow = parent.ParentWindow;
            _getAppiumDriver = () => parent.AppiumDriver;
        }

        public BaseComponent(BaseComponent parent)
        {
            ParentWindow = parent.ParentWindow;
            _getAppiumDriver = () => parent.AppiumDriver;
        }

        public override AppiumDriver<WindowsElement> AppiumDriver => _getAppiumDriver();

        public Window ParentWindow { get; }
    }
}
