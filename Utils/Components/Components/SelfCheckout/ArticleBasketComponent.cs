﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Constants.Images;
using Utils.Components.Components.SelfCheckout.PopUps;
using Utils.Components.Screens.SelfCheckout;
using Utils.Utils;

namespace Utils.Components.Components.SelfCheckout
{
    public class ArticleBasketComponent : BaseComponent
    {
        private readonly CancelTransactionPopUp _cancelTransactionPopUp;
        private readonly SelfCheckoutMainScreen _parent;

        public ArticleBasketComponent(SelfCheckoutMainScreen parent) : base(parent)
        {
            _parent = parent;
            _cancelTransactionPopUp = new CancelTransactionPopUp(this);
        }

        public CancelTransactionPopUp CancelTransactionPopUp
        {
            get
            {
                VerifyIfElementIsVisibleAndEnabled(CancelTransactionButton);
                CancelTransactionButton.Click();
                return _cancelTransactionPopUp;
            }
        }

        public WindowsElement ItemAddedLabel => AppiumDriver.FindElementByName("lblItemAdded");
        public WindowsElement SelectYourPreferenceTxt => AppiumDriver.FindElementByXPath("//Pane[@Name='lblSelectArticle']/Text");
        private WindowsElement CancelTransactionButton => AppiumDriver.FindElementByXPath("//Pane[@Name='butvoid']/Button");
        private WindowsElement CreditDebitCardButton => AppiumDriver.FindElementByName("Credit/debit card");
        private WindowsElement PayButton => AppiumDriver.FindElementByName("Pay");
        private WindowsElement PaymentResultLabel => AppiumDriver.FindElementByName("lblPaymentResult");
        private WindowsElement PopupNoButton => AppiumDriver.FindElementByName("butOk");
        private WindowsElement RetryTransactionButton => AppiumDriver.FindElementByName("butRetry");
        private WindowsElement TotalLabel => AppiumDriver.FindElementByName("lblTotal");

        public ArticleBasketComponent AddItemToBasket(string itemToAdd)
        {
            GetArticleButton(itemToAdd).Click();
            return this;
        }

        public double GetAmountToPay() => Convert.ToDouble(TotalLabel.FindElementByXPath("/*[1]/*[1]").Text);

        public WindowsElement GetArticleButton(string articleName) => AppiumDriver.FindElementByName(articleName);

        public SelfCheckoutMainScreen PayWithCreditCardNoReceipt()
        {
            VerifyIfElementIsVisibleAndEnabled(PayButton);
            PayButton.Click();

            VerifyIfElementIsVisibleAndEnabled(PopupNoButton);
            PopupNoButton.Click();

            VerifyIfElementIsVisibleAndEnabled(CreditDebitCardButton);
            CreditDebitCardButton.Click();

            // Wait until the payment result label is visible
            WaitUntilElementExists(
                () => PaymentResultLabel,
                nameof(PaymentResultLabel),
                TimeSpan.FromSeconds(5)
            );

            // If payment successful icon is not found
            if (!ImageHandler.ImageIsFoundOnScreen(SelfCheckoutImages.GreenVerify, this))
            {
                // If unsuccessful icon is found
                if (ImageHandler.ImageIsFoundOnScreen(SelfCheckoutImages.GreyWarning, this))
                {
                    Assert.Fail("Payment wasn't successful");
                }
                // If neither icon is found
                else
                {
                    // TODO: Add screenshot here?
                    Assert.Fail("Unknown payment error");
                }
            }
            RetryTransactionButton.Click();

            // Close screen in a clean way
            ResetScreen();
            return _parent;
        }

        public void ResetScreen()
        {
            // Is articleScreen open?
            // The label can't be grabbed if we are on the startscreen, it will go to the catch if we are

            try
            {
                // Cancel the transaction (should lead to start screen)
                if (ItemAddedLabel.Displayed)
                    CancelTransactionPopUp.SelectYes();
            }
            catch (NoSuchElementException)
            {
                // Ignored
                // We are likely already on the start screen if we get this specific exception
            }
        }

        public bool VerifyInitializationOptions()
        {
            VerifyIfElementIsVisibleAndEnabled(PayButton);
            VerifyIfElementIsVisibleAndEnabled(CancelTransactionButton);
            CancelTransactionPopUp.SelectYes();
            return true;
        }
    }
}
