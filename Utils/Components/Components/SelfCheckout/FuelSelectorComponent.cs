﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Utils;
using Utils.Utils.Enums;

namespace Utils.Components.Components.SelfCheckout
{
    public class FuelSelectorComponent<T> : BaseComponent where T : BaseComponent
    {
        private readonly T _parent;

        public FuelSelectorComponent(T parent, ArticleBasketComponent articleBasket) : base(parent)
        {
            ArticleBasket = articleBasket;
            _parent = parent;
        }

        public ArticleBasketComponent ArticleBasket { get; }
        public WindowsElement SelectYourPumpLabel => AppiumDriver.FindElementByName("lblSelectPump");

        public T ClickButtonByFillingType(FillingTypes fuelType)
        {
            WindowsElement fuelButton = GetFillingButtonByFillingType(fuelType);
            VerifyIfElementIsVisibleAndEnabled(fuelButton);
            fuelButton.Click();
            return _parent;
        }

        public FuelSelectorComponent<T> SelectPump(int pumpNumber)
        {
            VerifyIfElementIsVisible(SelectYourPumpLabel);
            GetPump(pumpNumber).Click();
            return this;
        }

        private WindowsElement GetFillingButtonByFillingType(FillingTypes fillingType) => AppiumDriver.FindElementByXPath($"//Pane[@Name='{fillingType.ToString()}']/..");

        private WindowsElement GetPump(int pumpNumber)
        {
            string pumpToUse = pumpNumber.ToString();
            if (pumpNumber < 10)
                pumpToUse = 0 + pumpNumber.ToString();
            return AppiumDriver.FindElementByName(pumpToUse);
        }
    }
}