﻿using OpenQA.Selenium.Appium.Windows;

namespace Utils.Components.Components.SelfCheckout.PopUps
{
    public class CancelTransactionPopUp : BaseComponent
    {
        public CancelTransactionPopUp(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement NoButton => AppiumDriver.FindElementByXPath("//Pane[@Name='butCancel']/Button");
        private WindowsElement YesButton => AppiumDriver.FindElementByXPath("//Pane[@Name='butOk']/Button");

        public void SelectNo() => NoButton.Click();

        public void SelectYes() => YesButton.Click();
    }
}
