﻿using Utils.Components.Windows;

namespace Utils.Components
{
    public interface IHasParentWindow
    {
        Window ParentWindow { get; }
    }
}
