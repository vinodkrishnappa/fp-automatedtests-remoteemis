﻿using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Utils;

namespace Utils.Components.MenuBars.Emis
{
    public class EmisIconToolbar : BaseComponent
    {
        public EmisIconToolbar(BaseScreen parent) : base(parent)
        {
        }

        public WindowsElement AddButton => AppiumDriver.FindElementByName("Add");
        public WindowsElement CancelRefreshButton => AppiumDriver.FindElementByName("Cancel / Refresh");
        public WindowsElement CloseButton => AppiumDriver.FindElementByName("Close");
        public WindowsElement DeleteButton => AppiumDriver.FindElementByName("Delete");
        public WindowsElement LinkButton => AppiumDriver.FindElementByName("Link");
        public WindowsElement PrintButton => AppiumDriver.FindElementByName("Print");
        public WindowsElement SaveButton => AppiumDriver.FindElementByName("Save");
        public WindowsElement SearchButton => AppiumDriver.FindElementByName("Search");
        public WindowsElement SwitchToForecourtScreenButton => AppiumDriver.FindElementByName("Switch to forecourt screen");
        public WindowsElement UnlinkButton => AppiumDriver.FindElementByName("Unlink");

        
    }
}
