﻿using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus.SubMenus;

namespace Utils.Components.MenuBars.Emis.Menus
{
    public class EmisMenu : BaseComponent
    {
        public EmisMenu(BaseScreen parent) : base(parent)
        {
        }

        public AccessMenu AccessMenu => new AccessMenu(this);
        public ConfigureAndUpdateMenu ConfigureAndUpdateMenu => new ConfigureAndUpdateMenu(this);
        public DiagnosticsMenu DiagnosticsMenu => new DiagnosticsMenu(this);
        public ForeCourtMenu ForeCourtMenu => new ForeCourtMenu(this);
        public PaymentModesMenu PaymentModesMenu => new PaymentModesMenu(this);
        public POSMenu POSMenu => new POSMenu(this);
        public ReportsMenu ReportsMenu => new ReportsMenu(this);
        public ShopArticlesMenu ShopArticlesMenu => new ShopArticlesMenu(this);
        public SystemMenu SystemMenu => new SystemMenu(this);
    }
}
