﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus.AccesMenu_Submenus
{
    public class UserManagementMenu : MenuItem
    {
        public UserManagementMenu(Func<AppiumDriver<WindowsElement>, WindowsElement> getMenuCondition, MenuItem parentMenu) : base(getMenuCondition, parentMenu)
        {
        }

        public MenuItem ProfilesMenuItem => CreateMenuItemChildByName("2 Profiles");
        public MenuItem UsersMenuItem => CreateMenuItemChildByName("1 Users");
    }
}
