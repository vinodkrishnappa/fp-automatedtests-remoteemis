﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus.SubMenus.AccesMenu_Submenus;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class AccessMenu : MenuItem
    {
        public UserManagementMenu _userManagementMenu => new UserManagementMenu((appiumDriver) => appiumDriver.FindElementByName("3 User management"), this) ;

        public AccessMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("1 Access"), menuClass) { }

        public MenuItem ConfigurationInternetBrowserMenuItem => CreateMenuItemChildByName("A Configuration Internet Browser");
        public MenuItem EmisLoginMenuItem => CreateMenuItemChildByName("1 Login");
        public MenuItem LicenseMenuItem => CreateMenuItemChildByName("4 License");
        public MenuItem LogoutMenuItem => CreateMenuItemChildByName("2 Logout");
    }
}
