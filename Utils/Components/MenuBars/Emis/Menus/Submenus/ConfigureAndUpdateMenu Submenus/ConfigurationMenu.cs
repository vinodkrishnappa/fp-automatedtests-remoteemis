﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using System;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus.ConfigureAndUpdateMenu_Submenus
{
    public class ConfigurationMenu : MenuItem
    {
        public ConfigurationMenu(Func<AppiumDriver<WindowsElement>, WindowsElement> getMenuCondition, MenuItem parentMenu) : base(getMenuCondition, parentMenu)
        {
        }

        public MenuItem DeviceInfoMenuItem => CreateMenuItemChildByName("4 Device info");
        public MenuItem ExportConfigurationMenuItem => CreateMenuItemChildByName("2 Export configuration");
        public MenuItem FuelPosReconfigurationMenuItem => CreateMenuItemChildByName("1 Fuel POS (re)configuration");
        public MenuItem ViewConfigurationInformationMenuItem => CreateMenuItemChildByName("3 View configuration information");
    }
}
