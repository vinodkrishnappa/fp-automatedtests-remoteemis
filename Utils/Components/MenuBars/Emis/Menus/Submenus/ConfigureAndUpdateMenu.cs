﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus.SubMenus.ConfigureAndUpdateMenu_Submenus;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class ConfigureAndUpdateMenu : MenuItem
    {
        public ConfigurationMenu _configurationMenu;

        public ConfigureAndUpdateMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("9 Configure and update"), menuClass) => _configurationMenu = new ConfigurationMenu((appiumDriver) => appiumDriver.FindElementByName("A Configuration"), this);

        public MenuItem A4PrintersMenuItem => CreateMenuItemChildByName("3 A4 Printers");
        public MenuItem BackupAndRestoreMenuItem => CreateMenuItemChildByName("C Backup and restore");
        public MenuItem CDROMMenuItem => CreateMenuItemChildByName("D CD-ROM");
        public MenuItem ImportFromHddMenuItem => CreateMenuItemChildByName("F Import from HDD");
        public MenuItem SoftwareUpdateMenuItem => CreateMenuItemChildByName("1 Software update");
        public MenuItem WindowsActivationMenuItem => CreateMenuItemChildByName("G Windows Activation");
    }
}
