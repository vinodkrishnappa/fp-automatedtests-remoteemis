﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus.DiagnosticsMenu_Submenus
{
    public class ShutdownRestartMenu : MenuItem
    {
        public ShutdownRestartMenu(Func<AppiumDriver<WindowsElement>, WindowsElement> getMenuCondition, MenuItem parentMenu) : base(getMenuCondition, parentMenu)
        {
        }

        public MenuItem ConsoleShutdownMenuItem => CreateMenuItemChildByName("1 Console shutdown");
        public MenuItem OSMenuItem => CreateMenuItemChildByName("3 OS");
    }
}
