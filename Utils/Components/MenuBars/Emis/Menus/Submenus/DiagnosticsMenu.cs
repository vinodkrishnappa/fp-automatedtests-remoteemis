﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus.SubMenus.DiagnosticsMenu_Submenus;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class DiagnosticsMenu : MenuItem
    {
        public ShutdownRestartMenu _shutdownRestartMenu;

        public DiagnosticsMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("8 Diagnostics"), menuClass) => _shutdownRestartMenu = new ShutdownRestartMenu((appiumDriver) => appiumDriver.FindElementByName("D Shutdown/Restart"), this);
    }
}
