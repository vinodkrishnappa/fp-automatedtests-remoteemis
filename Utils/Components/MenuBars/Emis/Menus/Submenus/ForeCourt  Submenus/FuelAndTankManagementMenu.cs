﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.Submenus.ForeCourt__Submenus
{
    public class FuelAndTankManagementSubMenu : MenuItem

    {
        public FuelAndTankManagementSubMenu(Func<AppiumDriver<WindowsElement>, WindowsElement> getMenuCondition, MenuItem parentMenu) : base(getMenuCondition, parentMenu)
        {
        }

        public MenuItem CardCodesMenuItem => CreateMenuItemChildByName("9 Card codes");
    }
}
