﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus.Submenus.ForeCourt__Submenus;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class ForeCourtMenu : MenuItem
    {
        public FuelAndTankManagementSubMenu FuelAndTankManagementSubMenu;

        public ForeCourtMenu(BaseComponent menuClass) : base(
            (appiumDriver) => appiumDriver.FindElementByName("3 Forecourt"),
            menuClass
        ) => FuelAndTankManagementSubMenu = new FuelAndTankManagementSubMenu(
            (appiumDriver) => appiumDriver.FindElementByName("1 Fuel and tank management"),
            this
        );
    }
}
