﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class POSMenu : MenuItem
    {
        public POSMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("6 POS"), menuClass)
        {
        }
    }
}
