﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class PaymentModesMenu : MenuItem
    {
        public PaymentModesMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("5 Payment modes"), menuClass)
        {
        }

        public MenuItem LitreCoupons => CreateMenuItemChildByName("5 Litre coupons");
    }
}
