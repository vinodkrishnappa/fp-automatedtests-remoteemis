﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class ReportsMenu : MenuItem
    {
        public ReportsMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("2 Reports"), menuClass)
        {
        }
    }
}
