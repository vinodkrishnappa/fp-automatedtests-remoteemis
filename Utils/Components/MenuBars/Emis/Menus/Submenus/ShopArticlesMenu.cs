﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class ShopArticlesMenu : MenuItem
    {
        public ShopArticlesMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("4 Shop articles"), menuClass)
        {
        }

        public MenuItem ArticleButtons => CreateMenuItemChildByName("2 Article buttons");
        public MenuItem Articles => CreateMenuItemChildByName("1 Articles");
        public MenuItem Promotions => CreateMenuItemChildByName("3 Promotions");
    }
}
