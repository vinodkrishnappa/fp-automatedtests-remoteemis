﻿using Utils.Components.Components;
using Utils.Components.MenuBars.Models;

namespace Utils.Components.MenuBars.Emis.Menus.SubMenus
{
    public class SystemMenu : MenuItem
    {
        public SystemMenu(BaseComponent menuClass) : base((appiumDriver) => appiumDriver.FindElementByName("7 System"), menuClass)
        {
        }
    }
}
