﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;

namespace Utils.Components.MenuBars.Models
{
    public class MenuItem : BaseComponent
    {
        public readonly Func<AppiumDriver<WindowsElement>, WindowsElement> GetMenuCondition;
        private readonly MenuItem _parentMenu;

        public MenuItem(Func<AppiumDriver<WindowsElement>, WindowsElement> getMenuCondition, MenuItem parentMenu) : base(parentMenu)
        {
            GetMenuCondition = getMenuCondition;
            _parentMenu = parentMenu;
        }

        internal MenuItem(Func<AppiumDriver<WindowsElement>, WindowsElement> getMenuCondition, BaseComponent baseComponent) : base(baseComponent) => GetMenuCondition = getMenuCondition;

        internal void Click() => GetMenuCondition(AppiumDriver).Click();

        internal void ClickThroughMenuTree()
        {
            // Create list of menu click events
            List<Action> menuItemClickEvents = new List<Action>
            {
                () => Click(),
                () => _parentMenu.Click(),
            };

            // Get the click events of the parent menus
            MenuItem topMostParent = _parentMenu;
            MenuItem previousParent = _parentMenu;

            while (previousParent._parentMenu != null)
            {
                menuItemClickEvents.Add(() => previousParent.Click());
                previousParent = previousParent._parentMenu;
                if (previousParent != null)
                    topMostParent = previousParent;
            }

            // Reverse the click event list, so that the top menu click event is the first one
            menuItemClickEvents.Reverse();

            // Make sure that the menu is closed (C# appium does not support sending keys without an element yet)
            topMostParent.GetMenuCondition(AppiumDriver).SendKeys(Keys.Alt);

            // Invoke click events
            menuItemClickEvents.ForEach(clickEvent => clickEvent.Invoke());
        }

        internal MenuItem CreateMenuItemChildByName(string name) => new MenuItem((appiumDriver) => appiumDriver.FindElementByName(name), this);
    }
}
