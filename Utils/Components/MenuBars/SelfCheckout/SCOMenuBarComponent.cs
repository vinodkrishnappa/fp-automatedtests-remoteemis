﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Resources.Constants.Images;
using Utils.Components.Components;
using Utils.Utils;

namespace Utils.Components.MenuBars.SelfCheckout
{
    public class SCOMenuBarComponent : BaseComponent
    {
        public SCOMenuBarComponent(BaseScreen parent) : base(parent)
        {
        }

        private WindowsElement AssistTxt => AppiumDriver.FindElementByXPath("//Pane[@Name='lblPrompt2']/Text");
        private WindowsElement HelpButton => AppiumDriver.FindElementByName("Help");
        private WindowsElement HelpPopup => AppiumDriver.FindElementByName("lblPrompt2");
        private WindowsElement LoginButton => AppiumDriver.FindElementByName("Login");
        private WindowsElement Logo => AppiumDriver.FindElementByName("picLogo");
        private WindowsElement PasswordTextField => AppiumDriver.FindElementByName("txtPassword");
        private WindowsElement PleaseWaitTxt => AppiumDriver.FindElementByXPath("//Pane[@Name='lblPrompt1']/Text");

        public SCOMenuBarComponent OpenAndCloseHelpPopUp(string language = "")
        {
            HelpButton.Click();
            Assert.IsTrue(HelpPopup.Displayed);
            switch (language)
            {
                case "english":
                    VerifyIfElementContainsSpecifiedText(PleaseWaitTxt, Constants.EnglishPleaseWait);
                    VerifyIfElementContainsSpecifiedText(AssistTxt, Constants.EnglishAssistText);
                    break;

                case "french":
                    VerifyIfElementContainsSpecifiedText(PleaseWaitTxt, Constants.FrenchPleaseWait);
                    VerifyIfElementContainsSpecifiedText(AssistTxt, Constants.FrenchAssistText);
                    break;

                case "german":
                    VerifyIfElementContainsSpecifiedText(PleaseWaitTxt, Constants.GermanPleasewait);
                    VerifyIfElementContainsSpecifiedText(AssistTxt, Constants.GermanAssistText);
                    break;

                case "dutch":
                    VerifyIfElementContainsSpecifiedText(PleaseWaitTxt, Constants.DutchPleasewait);
                    VerifyIfElementContainsSpecifiedText(AssistTxt, Constants.DutchAssistText);
                    break;
            }

            ImageHandler.ClickByImage(CommonImages.CloseX, this);
            return this;
        }

        public void OpenEFUI()
        {
            Logo.Click();
            SetLanguageToNextEnabledLanguage();
            PasswordTextField.SendKeys("123456");
            LoginButton.Click();
        }

        public SCOMenuBarComponent SetLanguage(int languageButton)
        {
            ChangeLanguage(languageButton);
            return this;
        }

        public bool VerifyInitializationOptions()
        {
            VerifyIfElementIsVisible(Logo);
            VerifyIfElementIsVisibleAndEnabled(LanguageButton(1));
            VerifyIfElementIsVisibleAndEnabled(LanguageButton(2));
            VerifyIfElementIsVisibleAndEnabled(LanguageButton(3));
            VerifyIfElementIsVisibleAndEnabled(LanguageButton(4));
            VerifyIfElementIsVisibleAndEnabled(HelpButton);
            return true;
        }

        private bool ChangeLanguage(int languageButton)
        {
            // Returns true if success
            WindowsElement languageBtn = LanguageButton(languageButton);
            // Enabled isn't reliable for this control
            // TODO: If the language isn't selected the control remains enabled
            if (languageBtn.Enabled)
            {
                languageBtn.Click();
                return true;
            }
            else
            {
                Logger.LogInfo($"Language {languageButton} already enabled, did nothing");
                return false;
            }
        }

        private WindowsElement LanguageButton(int index)
        {
            if (index > 4 || index == 0)
                throw new Exception("Unknown language button - menubar, selfcheckout");
            return AppiumDriver.FindElementByName($"Language{index}");
        }

        private SCOMenuBarComponent SetLanguageToNextEnabledLanguage()
        {
            // TODO: can give false positives, see other comments of ChangeLanguage function
            // developers should make the button disabled instead of disabiling the function underneath

            if (!ChangeLanguage(1) && !ChangeLanguage(2))
            {
                Logger.LogInfo("put breakpoint here, SCOmenubar - bug, uncomment throw when fixed");
                // Comment line below
                throw new Exception("Changing language failed twice, something went wrong - menubar, selfcheckout");
            }
            return this;
        }
    }
}
