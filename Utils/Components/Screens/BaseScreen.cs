﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components;
using Utils.Components.Windows;
using Utils.Components.Windows.Temporary;
using Utils.Utils;

namespace Resources.Components.Screens
{
    public abstract class BaseScreen : ScreenHelpers, IDisposable, IHasParentWindow
    {
        private readonly Func<AppiumDriver<WindowsElement>> _getAppiumDriver;

        protected BaseScreen(Window window, bool disposeTemporaryWindow = true)
        {
            if (window == null)
                Logger.LogWarning($"Can't initialise screen without a window - {GetType()}");

            if (window._appiumDriver == null)
                window.IntializeAppiumDriver();

            DisposeTemporaryWindow = disposeTemporaryWindow;
            ParentWindow = window;
            _getAppiumDriver = () => window._appiumDriver;
        }

        public override AppiumDriver<WindowsElement> AppiumDriver => _getAppiumDriver();
        public bool IsDisposed { get; private set; } = false;
        public Window ParentWindow { get; }
        private bool DisposeTemporaryWindow { get; }

        public void Dispose()
        {
            if (DisposeTemporaryWindow && TemporaryWindows.IsTemporaryWindow(ParentWindow))
                ParentWindow.Dispose();

            IsDisposed = true;
            GC.SuppressFinalize(this);
        }
    }
}
