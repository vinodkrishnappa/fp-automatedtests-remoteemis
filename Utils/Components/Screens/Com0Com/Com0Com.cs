﻿using System;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Com0Com
{
    // NOTE: this class does not work, but can be looked at for an example of a temporary screen
    public static class Com0Com
    {
        public static void ConfigureCom0ComPorts(int amountOfPorts)
        {
            RunPowerShellCommand(@"Start-Process 'C:\Program Files (x86)\com0com\setupg.exe'", TimeSpan.FromSeconds(10));
            new Com0ComScreen()
                .ConfigureComPorts(amountOfPorts)
                .Close();
        }
    }
}
