﻿using System.Linq;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Windows.Temporary;

namespace Utils.Components.Screens.Com0Com
{
    internal class Com0ComScreen : BaseScreen
    {
        public Com0ComScreen() : base(TemporaryWindows.Com0Com)
        {
        }

        private WindowsElement AddPairButton => AppiumDriver.FindElementByXPath("//Button[@Name='Add Pair']");
        private WindowsElement ApplyButton => AppiumDriver.FindElementByXPath("//Button[@Name='Apply']");
        private WindowsElement CloseButton => AppiumDriver.FindElementByXPath("//Button[@Name='Close']");

        public void Close()
        {
            CloseButton.Click();
            Dispose();
        }

        public Com0ComScreen ConfigureComPorts(int amount)
        {
            int existingAmount = AppiumDriver.FindElementsByXPath("//*[contains(text(), 'Virtual Port Pair')]").Count;
            Utils.Utils.Times(amount - existingAmount, ConfigureComPort);
            return this;
        }

        private void ConfigureComPort()
        {
            AddPairButton.Click();
            AppiumDriver.FindElementsByXPath("//*[contains(text(), 'Virtual Port Pair')]").Last().Click();
            AppiumDriver.FindElementsByXPath("//Button[@Name='use Ports class')]").ToList().ForEach(button => button.Click());
            ApplyButton.Click();
        }
    }
}
