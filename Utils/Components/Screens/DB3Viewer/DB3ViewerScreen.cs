﻿using System;
using Resources.Components.Screens;
using Utils.Components.Windows.Temporary;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.DB3Viewer
{
    public class DB3ViewerScreen : BaseScreen
    {
        public DB3ViewerScreen() : base(TemporaryWindows.DB3Viewer)
        {
        }

        public string FuelPosFullVersion
        {
            get
            {
                string fuelPosFullVersion = null;

                // In rare cases the version isn't found correctly, so run it more than once
                WaitUntil(
                    () =>
                    {
                        fuelPosFullVersion = AppiumDriver.FindElementByXPath("//Text[3]").Text;
                        return fuelPosFullVersion.Trim() != "";
                    },
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromSeconds(6),
                    $"Getting the fuelPosFullVersion failed - {nameof(DB3Viewer)})"
                );
                return fuelPosFullVersion;
            }
        }
    }
}
