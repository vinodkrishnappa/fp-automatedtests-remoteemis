﻿using System;
using Resources.Components.Screens;
using Resources.Constants.Images;
using Utils.Components.Windows.Main;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Efui
{
    public class EfuiMainScreen : BaseScreen
    {
        public EfuiMainScreen() : base(MainWindows.Efui)
        {
        }

        public EfuiMainScreen CloseShift()
        {
            // The wait is needed for older slower machines
            ImageHandler.WaitTillImageIsFound(EfuiImages.Operator, TimeSpan.FromSeconds(5), this);
            ImageHandler.ClickByImage(EfuiImages.Operator, this);

            if (ImageHandler.ImageIsFoundOnScreen(EfuiImages.CloseShift, this))
            {
                ImageHandler.ClickByImage(EfuiImages.CloseShift, this);
                // Closing shift doesn't always ask for password, thus only click ok, if ok exists
                AppiumDriver.FindElementByName(ParentWindow._title).SendKeys("123456");
                if (ImageHandler.ImageIsFoundOnScreen(EfuiImages.Ok, this))
                    ImageHandler.ClickByImage(EfuiImages.Ok, this);
                Logger.LogInfo("eFUI Shift closed");
            }
            else
            {
                Logger.LogInfo($"eFUI Shift couldn't be closed, likely was already closed.");
                ImageHandler.ClickByImage(EfuiImages.CancelButton, this);
            }
            return this;
        }

        public void OpenEmis()
        {
            const int retryTimes = 8;

            // The retry is needed for slower machines
            Retry(
                () =>
                {
                    ImageHandler.ClickByImage(EfuiImages.BlueCog, this);
                    if (!MainWindows.Emis.IsFocused)
                        throw new Exception($"Emis didn't exist after trying to open it '{retryTimes}' times - EfuiMainScreen");
                    return true;
                },
                TimeSpan.FromSeconds(2),
                retryTimes
            );
        }

        public void OpenSelfCheckout()
        {
            // The wait is needed for older slower machines
            ImageHandler.WaitTillImageIsFound(EfuiImages.Operator, TimeSpan.FromSeconds(5), this);
            ImageHandler.ClickByImage(EfuiImages.Operator, this);
            ImageHandler.ClickByImage(EfuiImages.StartSelfCheckout, this);
        }

        public EfuiMainScreen OpenShift()
        {
            // The wait is needed for older slower machines
            ImageHandler.WaitTillImageIsFound(EfuiImages.Operator, TimeSpan.FromSeconds(5), this);
            ImageHandler.ClickByImage(EfuiImages.Operator, this);
            ImageHandler.ClickByImage(EfuiImages.OpenShift, this);
            AppiumDriver.FindElementByName(ParentWindow._title).SendKeys("123456");
            ImageHandler.ClickByImage(EfuiImages.Ok, this);
            return this;
        }

        /// <summary>
        /// FuelPOS sometimes disables user interaction. This function can be used to work around that. It waits on the initial data & prepay transactions.
        /// </summary>
        public void WaitUntilEfuiIsReady()
        {
            Logger.LogInfo("Waiting until eFUI is ready...");
            WaitUntil(
                () =>
                {
                    ImageHandler.ClickByImage(EfuiImages.Operator, this);
                    return ImageHandler.ImageIsFoundOnScreen(EfuiImages.CloseShift, this)
                        || ImageHandler.ImageIsFoundOnScreen(EfuiImages.OpenShift, this);
                },
                TimeSpan.FromSeconds(5),
                TimeSpan.FromMinutes(2)
            );
            ImageHandler.ClickByImage(EfuiImages.CancelButton, this);
            Logger.LogInfo("eFUI is ready");
        }
    }
}
