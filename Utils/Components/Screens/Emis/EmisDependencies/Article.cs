﻿namespace Utils.Components.Screens.Emis.EmisDependencies
{
    public class Article
    {
        public Article(string name, int articleNumber)
        {
            Name = name;
            ArticleNumber = articleNumber;
        }

        public int ArticleNumber { get; }
        public string Name { get; }
    }
}
