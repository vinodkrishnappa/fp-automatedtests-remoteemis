﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Resources.Constants.Images;
using Utils.Components.MenuBars.Emis;
using Utils.Components.MenuBars.Emis.Menus;
using Utils.Components.Screens.DB3Viewer;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration;
using Utils.Components.Screens.Emis.ForeCourt;
using Utils.Components.Screens.Emis.PaymentModesSubscreens;
using Utils.Components.Screens.FuelPosSetup;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Components.Windows.Temporary;
using Utils.Utils;
using Utils.Utils.Drivers;
using Utils.Utils.FtpHelper;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.EmisDependencies
{
    public class BaseEmisScreen : BaseScreen
    {
        #region Subscreens

        private readonly BackupAndRestoreScreen _backupAndRestoreScreen;
        private readonly CardsCodesScreen _cardCodesScreen;
        private readonly LitreCouponScreen _litreCouponScreen;
        private readonly LoginScreen _loginScreen;
        private readonly ShutdownRestartScreen _shutdownRestartScreen;

        #endregion Subscreens

        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;
        private readonly EmisMenu _emisMenu;

        public WindowsElement HelpButton => AppiumDriver.FindElementByXPath("//Button[@Name='Help']");
        public WindowsElement NoButton => AppiumDriver.FindElementByName("No");
        private WindowsElement AddIcon => AppiumDriver.FindElementByName("Add");
        private WindowsElement ArticleNumber => AppiumDriver.FindElementByName("Article number (PLU):");
        private WindowsElement ArticleNumberEdit => AppiumDriver.FindElementByXPath("//Edit[@Name='Article number (PLU):']");
        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");
        private WindowsElement NameEdit => AppiumDriver.FindElementByXPath("//Edit[@Name='Name:']");

        #endregion Menus

        public BaseEmisScreen(Window window) : base(window)
        {
            _emisMenu = new EmisMenu(this);
            _emisIconToolbar = new EmisIconToolbar(this);
            _backupAndRestoreScreen = new BackupAndRestoreScreen(this);
            Configuration = new FuelPOSConfigurationScreen(this, _emisIconToolbar);
            _shutdownRestartScreen = new ShutdownRestartScreen(this);
            _loginScreen = new LoginScreen(this, window.GetType()==TemporaryWindows.RemoteEmis.GetType());
            _litreCouponScreen = new LitreCouponScreen(this, _emisMenu);
            _cardCodesScreen = new CardsCodesScreen(this, _emisMenu);
        }

        public CardsCodesScreen CardCodesScreen { get; }
        public FuelPOSConfigurationScreen Configuration { get; }

        /// <summary>
        /// Details screen (should be in a separate screen, but only has one control for now)
        /// </summary>
        public WindowsElement DetailsTreeView => AppiumDriver.FindElementByXPath("//DataItem[@Name='Station']/..");

        public LitreCouponScreen LitreCouponScreen { get; }

        public void AddArticles(params Article[] articles)
        {
            _emisMenu.ShopArticlesMenu.Articles.ClickThroughMenuTree();
            Thread.Sleep(5000);
            CancelButton.Click();
            AddIcon.Click();
            foreach (Article article in articles)
            {
                ArticleNumber.SendKeys(article.ArticleNumber.ToString());
                ArticleNumberEdit.SendKeys(Keys.Enter);
                NameEdit.SendKeys(article.Name);
            }
        }

        // TODO: get better FTP permissions, moving the file with powershell isn't a good longterm solution
        // Much of the logic in this method should move to FtpHelper.
        public void ApplyFallback(string fallbackToUsePath)
        {
            // FTP the new fallback
            // Remove leftover fallback data
            RunPowerShellCommand(@"Remove -Recurse C:\Fallback", TimeSpan.FromSeconds(15));

            // not authorized to make the fallback dir, so use another one
            string ftpDirectory = "/Pro_bo/EBOC/Fallback";
            ftpDirectory = "/Pro_bo/EBOC";

            FtpHelper.UploadFileToEBOC(fallbackToUsePath, FtpHelper.s_primaryFtpUser);

            // Convert ftp path to windows path
            string fileName = Path.GetFileName(fallbackToUsePath);
            string source = ftpDirectory.Replace("/", "\\");

            // Move the fallback to the correct location
            RunPowerShellCommand($"Move-Item -Path 'C:{source}\\{fileName}' 'C:\\Fallback\\{fileName}'", TimeSpan.FromSeconds(5));

            // Apply the fallback
            _emisMenu.ConfigureAndUpdateMenu.BackupAndRestoreMenuItem.ClickThroughMenuTree();
            _backupAndRestoreScreen.RestoreBackup();

            // Wait until the box is restarted (reattach the connection afterwards)
            // TODO: launch.bat, (testbuild fuelpos) after the connection is made, it is a good idea to wait until the till is closed icon is no longer present
        }

        /// <summary>
        /// If a previous testcase has ended inside one of the eMis menus, by design or because of an error
        /// Then this method can be used to ATTEMPT to resset the system back to the eMis BaseScreen
        /// If it fails to do so it will throw an error as we assume that all Emis Tests -
        /// should start from the BaseScreen, I recommend to include this in the Teardown for all Emis Tests.
        /// </summary>
        public void AttemptToReturnToEMisBaseScreen()
        {
            try
            {
                //If the Help Button is enabled we are already on the Emis BaseScreen
                if (!HelpButton.Enabled)
                {
                    //This should cover most situations
                    //, feel free to add additional scenario's as you come across them.
                    if (_emisIconToolbar.CloseButton.Enabled)
                    {
                        _emisIconToolbar.CloseButton.Click();

                        //To click No when asked to save the changes
                        //Just catch the exception if the button does not exist, more often than not
                        //the No button is not required to return to the Base Screen
                        try
                        {
                            if (NoButton.Enabled)
                                NoButton.Click();
                        }
                        catch (Exception)
                        {
                            // Ignored
                        }
                    }
                    //Validate if the return to Base Screen succeeded
                    if (HelpButton.Enabled)
                    {
                        Logger.LogInfo("Returned to the eMis Base Screen");
                    }
                    else
                    {
                        Logger.LogInfo("Failed to return to the eMis Base Screen tp reset test environment.");
                        throw new Exception("Failed to return to the eMis Base Screen to reset test environment.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"Unable to navigate back to the eMis Basescreen \n {Logger.GetExceptionInfo(ex)}");
            }
        }

        public Dictionary<string, string> GetFuelPosBuildVersionsFromFullBuildNumber(string fuelPosFullVersion)
        {
            string minorVersionFinder = fuelPosFullVersion.Split('.')[2];
            string minorVersion = minorVersionFinder.Substring(Math.Max(0, minorVersionFinder.Length - 4));
            string majorVersion = fuelPosFullVersion.Split('.')[0];

            return new Dictionary<string, string>
            {
                ["FuelPosFullVersion"] = fuelPosFullVersion,
                ["FuelPosMajorVersion"] = majorVersion,
                ["FuelPosMinorVersion"] = minorVersion,
                ["FuelPosVersionShort"] = $"{majorVersion}.{minorVersion}"
            };
        }

        public Dictionary<string, string> GetFuelPosDetails()
        {
            DB3ViewerScreen Db3Viewer = new DB3ViewerScreen();
            string fuelPosFullVersion = Db3Viewer.FuelPosFullVersion;
            Db3Viewer.Dispose();

            Dictionary<string, string> fuelPosData = GetFuelPosBuildVersionsFromFullBuildNumber(fuelPosFullVersion);
            ImageHandler.ClickByImage(WindowsIcons.PowerOff, this);
            return fuelPosData;
        }

        public void GoToFuelPosConfiguration() => _emisMenu.ConfigureAndUpdateMenu._configurationMenu.FuelPosReconfigurationMenuItem.ClickThroughMenuTree();

        public FuelPosSetupScreen GoToFuelPosSetup()
        {
            _emisMenu.DiagnosticsMenu._shutdownRestartMenu.ConsoleShutdownMenuItem.ClickThroughMenuTree();
            return _shutdownRestartScreen.WaitUntilLoaded().OpenFuelPosSetup();
        }

        public void Login()
        {
            //_emisMenu.AccessMenu.EmisLoginMenuItem.ClickThroughMenuTree();

            _loginScreen.Login();
        }

        public void OpenEfui() => ImageHandler.ClickByImage(WindowsIcons.ComputerWithArrows, this);

        public void Reboot()
        {
            Stopwatch stopWatch = Stopwatch.StartNew();
            _emisMenu.DiagnosticsMenu._shutdownRestartMenu.ConsoleShutdownMenuItem.ClickThroughMenuTree();
            _shutdownRestartScreen.WaitUntilLoaded().Reboot();
            DriverConnection.WaitUntilFuelPosIsDown();
            stopWatch.Stop();
            Logger.LogInfo($"Selecting reboot & waiting until FuelPOS went down took {stopWatch.Elapsed}");

            DriverConnection.Reconnect(true);
        }

        public void SpecificCleanupStepsPerMUTFile(string MutName)
        {
            //Additional cases to be added further down the road.
            switch (MutName)
            {
                //Validate if the Litre Coupons were all actually deleted to prevent the reoccurance of FP-7212
                case "LC_MUT.001":
                    AttemptToReturnToEMisBaseScreen();
                    if (!LitreCouponScreen.ValidateNumberOfLitreCoupons(0))
                    {
                        //Return to the Base Screen before throwing the exception.
                        AttemptToReturnToEMisBaseScreen();
                        throw new Exception("Number of Litre Coupons is not as expected, see test logs.");
                    }
                    break;
            }
        }
    }
}
