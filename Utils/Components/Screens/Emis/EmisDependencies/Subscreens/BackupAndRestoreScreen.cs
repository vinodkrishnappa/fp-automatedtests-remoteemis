﻿using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;

namespace Utils.Components.Screens.Emis
{
    public class BackupAndRestoreScreen : BaseComponent
    {
        public BackupAndRestoreScreen(BaseScreen parent) : base(parent)
        {
        }

        private WindowsElement RestoreButton => AppiumDriver.FindElementByXPath("//Button[@Name='Restore']");

        private WindowsElement YesButton => AppiumDriver.FindElementByName("Yes");

        public BackupAndRestoreScreen RestoreBackup()
        {
            RestoreButton.Click();
            YesButton.Click();
            return this;
        }
    }
}