﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.Enums;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration
{
    public class FuelPOSConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly DeviceScreen _deviceScreen;
        private readonly GeneralScreen _generalScreen;
        private LanguageSelectionScreen _languageSelectionScreen;

        #endregion Subscreens

        public FuelPOSConfigurationScreen(BaseScreen parent, EmisIconToolbar emisIconToolBar) : base(parent)
        {
            _emisIconToolbar = emisIconToolBar;
            _generalScreen = new GeneralScreen(this, emisIconToolBar);
            _deviceScreen = new DeviceScreen(DeviceTypes.CISorINT, this, _emisIconToolbar);
        }

        public WindowsElement FinishButton => AppiumDriver.FindElementByName("Finish");
        public FuelConfigurationScreen FuelConfiguration => _generalScreen.FuelConfiguration;

        public void CloseConfigurationScreen()
        {
            _emisIconToolbar.CloseButton.Click();
            FinishButton.Click();
        }

        public FuelPOSConfigurationScreen ConfigureNozzle(int pumpNumber, int nozzleNumber, string fuelType)
        {
            _generalScreen
                .TankGroupsAndNozzles
                .ConfigureNozzle(pumpNumber, nozzleNumber)
                .SetFuelType(fuelType);
            Logger.LogInfo($"Added and configured a nozzle with fuel {fuelType} for pump {pumpNumber}");
            return this;
        }

        public FuelPOSConfigurationScreen ConfigureTankGroup(int tankNumber, string fuelType)
        {
            _generalScreen
                .TankGroupsAndNozzles
                .ConfigureTankGroup(tankNumber)
                .SetFuelType(fuelType);
            Logger.LogInfo($"Added and configured tankg group {tankNumber} with fuel {fuelType}.");
            return this;
        }

        public void SaveConfiguration() => ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

        public void SaveConfigurationAndActivateForecourt()
        {
            AppiumWebElement GetOkButtonActivateForecourt() => AppiumDriver.FindElementByName("OK");

            _emisIconToolbar.SaveButton.Click();

            WaitUntil(
               () => DoesElementExist(GetOkButtonActivateForecourt),
               TimeSpan.FromSeconds(1),
               TimeSpan.FromSeconds(10),
               "Waiting for activate forecourt pop up to show took too long."
            );

            GetOkButtonActivateForecourt().Click();
            Logger.LogInfo("Saved configuration and activated forecourt configuration.");

            WaitUntil(
               () => DoesElementExist(() => _emisIconToolbar.CloseButton),
               TimeSpan.FromSeconds(1),
               TimeSpan.FromSeconds(10),
               "Waiting for the close button to exist to show took too long."
            );
        }

        public void SaveConfigurationWithoutActivating()
        {
            AppiumWebElement GetCancelButton() => AppiumDriver.FindElementByName("Cancel");

            _emisIconToolbar.SaveButton.Click();

            ClickUntillElementNoLongerExists(GetCancelButton, 15);

            Logger.LogInfo("Saved Dispensing Configuration but did not Activate.");
        }

        public ChargePointConnectorScreen SetupChargePointConnectors() => _generalScreen.SetupChargePointConnectors();

        public CISPeripheralsConfigurationScreen SetupCISPeripheralsConfiguration() => _generalScreen.SetupCISPeripheralsScreen();

        public DispensingConfigurationScreen SetupDispensingConfiguration() => _generalScreen.SetupDispensingConfiguration();

        public void SetupFuelAndArticleProgramming()
        {
            _generalScreen.FuelAndArticleProgramming
                .SetComposedArticlesCheckbox()
                .SetFreeGroupCodeInputCheckbox();
            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);
        }

        public void SetupFuelConfiguration() => _generalScreen.FuelConfiguration.AddFuel();

        public HardwareAndSoftwareScreen SetupHardwareAndSoftware() => _deviceScreen.SetupHardwareAndSoftware();

        public void SetupLanguageSelection()
        {
            _languageSelectionScreen = _generalScreen.LanguageSelection;
            AppiumWebElement GetLanguage() => AppiumDriver.FindElementByName("Default customer language:");
            WaitUntil(
               () => DoesElementExist(GetLanguage),
               TimeSpan.FromSeconds(1),
               TimeSpan.FromSeconds(10),
               "Waiting for the language page to be displayed took too long."
            );
            _languageSelectionScreen
                .SetAllLanguages(Languages.s_english
                , Languages.s_german, Languages.s_french, Languages.s_dutch);
            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);
        }

        public LoyaltyPromotionDiscountConfigurationScreen SetupLoyaltyPromotionDiscountConfiguration() => _generalScreen.SetupLoyaltyPromotionDiscountConfiguration();

        public OnlineHostConfigurationScreen SetupOnlineHostConfiguration() => _generalScreen.SetupOnlineHostConfiguration();

        public OPTConfigurationScreen SetupOPTConfiguration() => _generalScreen.SetupOPTConfiguration();

        public PaymentModeConfigurationScreen SetupPaymentModeConfiguration() => _generalScreen.SetupPaymentModeConfiguration();

        public POSPeripheralsConfigurationScreen SetupPOSPeripheralsConfiguration() => _generalScreen.SetupPOSPeripheralsScreen();

        public DispensingConfigurationScreen SetupPriceSignConfiguration() => _generalScreen.SetupPriceSignConfiguration();

        public PumpsConfigurationScreen SetupPumpsConfiguration() => _generalScreen.SetupPumpsConfiguration();

        public SalesOptionsConfigurationScreen SetupSalesOptionsConfiguration() => _generalScreen.SetupSalesOptionsConfiguration();

        public SerialPortAssignmentScreen SetupSerialPortAssignment() => _deviceScreen.SetupSerialPortAssignment();

        public void SetupStationInformation()
        {
            _generalScreen.StationInformation
                .SetStationName("Clean Install - Automation - Test Station")
                .SetAddress("Everdongenlaan 31", "2300", "Turnhout")
                .SetFirstLogo()
                .SetVATNumber("0411905847");

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);
        }

        public void SetupTankGroupsAndNozzlesConfiguration(string fuelType)
        {
            _generalScreen
                .TankGroupsAndNozzles
                .ConfigureTankGroup(1)
                .SetFuelType(fuelType);

            _generalScreen
                .TankGroupsAndNozzles
                .ConfigureNozzle(1, 1)
                .SetFuelType(fuelType);
        }

        public TariffConfigurationScreen SetupTariffConfiguration() => _generalScreen.SetupTariffConfiguration();

        public TariffProgrammingScreen SetupTariffProgramming() => _generalScreen.SetupTariffProgramming();
    }
}
