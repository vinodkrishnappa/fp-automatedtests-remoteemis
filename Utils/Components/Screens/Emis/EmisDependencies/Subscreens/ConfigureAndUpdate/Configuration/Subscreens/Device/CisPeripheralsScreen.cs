﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class CisPeripheralsScreen : BaseComponent
    {
        public CisPeripheralsScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement IQCryptoVGACheckBox => AppiumDriver.FindElementByName("IQ/Crypto VGA");

        public CisPeripheralsScreen SetIQCryptoVGACheckBox(bool check = true)
        {
            CheckboxHelper.SetCheckbox(IQCryptoVGACheckBox, check);
            return this;
        }
    }
}
