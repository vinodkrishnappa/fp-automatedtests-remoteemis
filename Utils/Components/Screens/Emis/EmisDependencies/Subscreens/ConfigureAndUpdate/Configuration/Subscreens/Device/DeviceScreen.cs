﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class DeviceScreen : BaseComponent
    {
        private readonly DeviceTypes _deviceType;

        #region Subscreens

        private readonly CisPeripheralsScreen _cisPeripheralsScreen;
        private readonly HardwareAndSoftwareScreen _hardwareAndSoftwareScreen;
        private readonly SerialPortAssignmentScreen _serialPortAssignmentScreen;
        public readonly PosPeripheralsScreen _posPeripheralsScreen;

        #endregion Subscreens

        public DeviceScreen(DeviceTypes deviceType, BaseComponent parent, EmisIconToolbar emisIconToolbar) : base(parent)
        {
            _deviceType = deviceType;
            _cisPeripheralsScreen = new CisPeripheralsScreen(this);
            _hardwareAndSoftwareScreen = new HardwareAndSoftwareScreen(this, emisIconToolbar);
            _posPeripheralsScreen = new PosPeripheralsScreen(this);
            _serialPortAssignmentScreen = new SerialPortAssignmentScreen(this, emisIconToolbar);
        }

        private WindowsElement CisPeripheralsItem => AppiumDriver.FindElementsByName("CIS peripherals")[(int)_deviceType];

        //private WindowsElement LoyaltyTerminalItem => AppiumDriver.FindElementsByName("Loyalty terminal")[(int)_deviceType];
        //private WindowsElement PaymentTerminalItem => AppiumDriver.FindElementsByName("Payment terminal")[(int)_deviceType];
        //private WindowsElement PaymentTerminalNamesItem => AppiumDriver.FindElementsByName("Payment terminal names")[(int)_deviceType];
        //private WindowsElement TerminalIpConfigurationItem => AppiumDriver.FindElementsByName("Terminal IP configuration")[(int)_deviceType];
        private WindowsElement HardwareAndSoftwareItemByDevice => AppiumDriver.FindElementsByName("Hardware and software")[(int)_deviceType];

        private WindowsElement HardwareAndSoftwareItem => AppiumDriver.FindElementByXPath("//DataItem[@Name='Hardware and software']");

        //private WindowsElement NetworkConfiguration => AppiumDriver.FindElementsByName("Network configuration")[(int)_deviceType];

        //private WindowsElement NetworkRoutes => AppiumDriver.FindElementsByName("Network routes")[(int)_deviceType];

        private WindowsElement PosPeripheralsItem => AppiumDriver.FindElementsByName("POS peripherals")[(int)_deviceType];

       //private WindowsElement SerialPortAssignment => AppiumDriver.FindElementsByName("Serial port assignment")[(int)_deviceType];

        private WindowsElement SerialPortAssignment => AppiumDriver.FindElementByXPath("//DataItem[@Name='Serial port assignment']");


        public void AssignSerialPortsInt() => _serialPortAssignmentScreen.AssignSerialPortsInt();

        public void SetDeviceForComport(string ComporNumber, string SetToDevice) => _serialPortAssignmentScreen.SetDeviceForComport(ComporNumber, SetToDevice);

        public void SetupCISPeripherals()
        {
            CisPeripheralsItem.Click();
            _cisPeripheralsScreen.SetIQCryptoVGACheckBox();
        }

        public void SetupPOSPeripherals()
        {
            PosPeripheralsItem.Click();
            _posPeripheralsScreen.SetPinPad();
        }

        public HardwareAndSoftwareScreen SetupHardwareAndSoftware()
        {
            HardwareAndSoftwareItem.Click();
            return _hardwareAndSoftwareScreen;
        }

        public SerialPortAssignmentScreen SetupSerialPortAssignment()
        {
            SerialPortAssignment.Click();
            return _serialPortAssignmentScreen;
        }
    }
}
