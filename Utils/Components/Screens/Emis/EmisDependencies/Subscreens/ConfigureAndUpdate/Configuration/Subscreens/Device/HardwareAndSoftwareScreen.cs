﻿using System;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class HardwareAndSoftwareScreen : BaseComponent
    {
        private readonly EmisIconToolbar _emisIconToolbar;

        public HardwareAndSoftwareScreen(BaseComponent parent, EmisIconToolbar emisIconToolBar) : base(parent)
        {
            _emisIconToolbar = emisIconToolBar;
        }

        private WindowsElement TouchScreenComboBox => AppiumDriver.FindElementByXPath("//Text[@Name='Touchscreen:']/../ComboBox");

        private WindowsElement AutoDetectButton => AppiumDriver.FindElementByName("Auto detect");

        private WindowsElement YesButton => AppiumDriver.FindElementByName("Yes");

        public HardwareAndSoftwareScreen SetTouchScreen(string touchScreenToSelect)
        {
            WaitUntil(
                () => DoesElementExist(() => AutoDetectButton),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(20),
                "Waiting for Hardware and Software window to open took too long."
            );

            ComboBoxHelper.SetComboBox(TouchScreenComboBox, touchScreenToSelect);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }

        public HardwareAndSoftwareScreen AutoDetectSerialPorts()
        {
            WaitUntil(
                () => DoesElementExist(() => AutoDetectButton),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(20),
                "Waiting for Hardware and Software window to open took too long."
            );

            AutoDetectButton.Click();
            YesButton.Click();
            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);
            return this;
        }
    }
}
