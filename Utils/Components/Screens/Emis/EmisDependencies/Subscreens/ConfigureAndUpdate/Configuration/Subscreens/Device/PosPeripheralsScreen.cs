﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class PosPeripheralsScreen : BaseComponent
    {
        public PosPeripheralsScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement PINPadComboBox => AppiumDriver.FindElementByName("PIN Pad:");

        public PosPeripheralsScreen SetPinPad(string model = "Verifone VX820")
        {
            if (model == null)
                throw new System.ArgumentNullException(nameof(model));

            PINPadComboBox.Click();
            PINPadComboBox.SendKeys("Verifone VX820");
            return this;
        }
    }
}
