﻿using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils;
using Utils.Utils.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class SerialPortAssignmentScreen : BaseComponent
    {
        private readonly EmisIconToolbar _emisIconToolbar;

        public SerialPortAssignmentScreen(BaseComponent parent, EmisIconToolbar emisIconToolBar) : base(parent)
        {
            _emisIconToolbar = emisIconToolBar;
        }

        private WindowsElement NoButton => AppiumDriver.FindElementById("No");

        public SerialPortAssignmentScreen SetDeviceForComport(string ComPort, string DeviceToSelect)
        {
            int maxTries = 15;
            int counter = 0;
            bool isSelected = false;
            AppiumWebElement currentDevice = AppiumDriver.FindElementByName(ComPort).FindElementByName("Device");

            //Cycles through the drop down list options until the device to select is selected.
            while (counter < maxTries && !isSelected)
            {
                currentDevice.Click();
                currentDevice.SendKeys(Keys.ArrowDown);
                isSelected = AppiumDriver.FindElementByName(ComPort).FindElementByName(DeviceToSelect).Selected;
                counter++;
            }

            if (isSelected)
            {
                Logger.LogInfo($"For ComPort '{ComPort}' succesfully selected Device: '" + DeviceToSelect + "' in the serial port assignment.");
                ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);
                //The adding of the serial ports takes some time in the back end
                //If we do not wait for this the ports will not be selectable in the next step.
                Thread.Sleep(5000);
            }
            else
            {
                Logger.LogInfo($"For ComPort '{ComPort}' unable to select Device: '" + DeviceToSelect + "' in the serial port assignment, device was not found as an option.");
                NoButton.Click();
            }
            return this;
        }
        
        public SerialPortAssignmentScreen AssignSerialPortsInt()
        {
            SetDeviceForComport("3", "PIN Pad");
            SetDeviceForComport("5", "Dispensing port 1");
            SetDeviceForComport("4", " ");
            SetDeviceForComport("6", " ");
            // EmisIconToolbar.SaveButton.Click();
            return this;
        }
    }
}
