﻿namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.Enums
{
    // Acts like an Enum, but for strings
    public sealed class Languages
    {
        // Define your values here
        public static readonly Languages s_dutch = new Languages("Nederlands");
        public static readonly Languages s_english = new Languages("English");
        public static readonly Languages s_french = new Languages("Français");
        public static readonly Languages s_german = new Languages("Deutsch");

        // Enum logic
        private Languages(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(Languages lang) => lang.Value;

        internal static Languages Parse(string enumKey) => typeof(Languages).GetField(enumKey).GetValue(null) as Languages;
    }
}
