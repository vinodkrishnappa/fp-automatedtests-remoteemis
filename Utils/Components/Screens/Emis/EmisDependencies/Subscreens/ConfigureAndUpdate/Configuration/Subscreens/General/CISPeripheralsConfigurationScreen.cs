﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class CISPeripheralsConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        public CISPeripheralsConfigurationScreen(BaseComponent parent, EmisIconToolbar emisIconToolbar) : base(parent) => _emisIconToolbar = emisIconToolbar;

        public WindowsElement CarWashComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Back Office Computer:']");

        public WindowsElement CryptoCheckBox => AppiumDriver.FindElementByName("IQ/Crypto VGA");

        public CISPeripheralsConfigurationScreen SetCarwash(string carWashName)
        {
            WaitUntil(
                () => CarWashComboBox.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Fuel an Article Programming screen to load took too long."
            );

            ComboBoxHelper.SetComboBox(CarWashComboBox, carWashName);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            Logger.LogInfo("Succesfuly set car wash type to '" + carWashName + "'.");
            return this;
        }

        public CISPeripheralsConfigurationScreen SetIQCryptoVGA(bool check)
        {
            AppiumWebElement GetCryptoCheckBox() => AppiumDriver.FindElementByName("IQ/Crypto VGA");

            WaitUntil(
                () => DoesElementExist(GetCryptoCheckBox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Crypto checkbox to be displayed took too long."
            );

            CheckboxHelper.SetCheckbox(CryptoCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            Logger.LogInfo("Succesfuly set the checkbox IQ/Crypto VGA to'" + check + "'.");
            return this;
        }
    }
}
