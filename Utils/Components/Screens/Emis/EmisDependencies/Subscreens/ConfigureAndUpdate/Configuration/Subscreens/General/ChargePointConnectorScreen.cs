﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class ChargePointConnectorScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly ChargePointConnectorsConfiguration _chargePointConnectorsConfiguration;

        #endregion Subscreens

        public ChargePointConnectorScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _chargePointConnectorsConfiguration = new ChargePointConnectorsConfiguration(this);
            _emisIconToolbar = toolbar;
        }

        private WindowsElement AddButton => AppiumDriver.FindElementByName("Add");

        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");

        public ChargePointConnectorScreen AddConnector(int connectorNumber)
        {
            //It only finds charge points that follow the naming convention
            //"Connector 'connectorNumber' (Tariff 'connectorNumber')"
            AppiumWebElement GetChargePointConnector() => ChargePointItem(connectorNumber).FindElementByXPath("//DataItem[@Name='Connector " + connectorNumber + " (Tariff " + connectorNumber + ")']");

            WaitUntil(
                () => ChargePointItem(connectorNumber).Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Charge Point Connector tab to be visible took too long."
            );

            bool chargePointConnectorExists = DoesElementExist(GetChargePointConnector);
            if (!chargePointConnectorExists)
            {
                ChargePointItem(connectorNumber).Click();
                _emisIconToolbar.AddButton.Click();
                _chargePointConnectorsConfiguration.ConfigureChargePointConnector(connectorNumber);
            }
            Logger.LogInfo("Added and configured Connector: " + connectorNumber);

            return this;
        }

        private WindowsElement ChargePointItem(int connectorNumber) => AppiumDriver.FindElementByName("Charge point " + connectorNumber);
    }
}
