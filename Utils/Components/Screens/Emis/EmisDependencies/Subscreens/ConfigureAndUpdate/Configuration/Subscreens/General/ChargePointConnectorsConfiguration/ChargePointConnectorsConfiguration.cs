﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class ChargePointConnectorsConfiguration : BaseComponent
    {
        public ChargePointConnectorsConfiguration(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement comboBoxConnectorSubType => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Subtype:']");
        private WindowsElement comboBoxConnectorType => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Type:']");
        private WindowsElement comboBoxTariffName => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Tariff:']");
        private WindowsElement textBoxConnectorName => AppiumDriver.FindElementByXPath("//Edit[@Name='Connector name:']");

        public ChargePointConnectorsConfiguration ConfigureChargePointConnector(int connectorNumber)
        {
            WaitUntil(
                () => textBoxConnectorName.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Charge Point Connector Configuration panel is visible took too long."
            );

            //This field seems to be hard to address, you can confirm it is enabled first
            //but still the sendkeys can fail, this loop retries a couple of times
            string connectorName = null;
            int counter = 0;
            while (connectorName == null && counter < 10)
            {
                Thread.Sleep(1000);
                textBoxConnectorName.SendKeys("Connector " + connectorNumber);
                connectorName = textBoxConnectorName.GetAttribute("Value.Value");
                counter++;
            }
            Assert.That(connectorName != null, "Failed to fill in the Connector Name textfield in the Charge Point Connector tab. This field is know to be difficult to address.");

            ComboBoxHelper.SetComboBox(comboBoxTariffName, $"Tariff {connectorNumber}");

            //To create some variency between connectors 1,2 & 3,4
            if (connectorNumber < 2)
            {
                ComboBoxHelper.SetComboBox(comboBoxConnectorType, "DC");

                ComboBoxHelper.SetComboBox(comboBoxConnectorSubType, "CCS");
            }
            else
            {
                ComboBoxHelper.SetComboBox(comboBoxConnectorType, "AC");

                ComboBoxHelper.SetComboBox(comboBoxConnectorSubType, "Type 2");
            }
            return this;
        }
    }
}
