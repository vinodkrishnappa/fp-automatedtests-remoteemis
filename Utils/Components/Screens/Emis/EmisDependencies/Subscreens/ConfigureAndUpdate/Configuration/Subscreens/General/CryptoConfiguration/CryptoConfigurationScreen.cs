﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils;
using Utils.Utils.ElementHelpers;
using Utils.Utils.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.CryptoConfiguration
{
    public class CryptoConfigurationScreen : BaseComponent
    {
        public CryptoConfigurationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement WeeklyTimetableTab => AppiumDriver.FindElementByName("Weekly timetable");

        private WindowsElement ConfigurationTab => AppiumDriver.FindElementByName("Configuration");


        private WindowsElement OnCheckBox => AppiumDriver.FindElementByName("On");

        private WindowsElement TypeComboBox = null;
        private WindowsElement TerminalComboBox = null;
        private WindowsElement ModeComboBox = null;

        private WindowsElement EMVKernelComboBox = null;
        private WindowsElement PrinterTypeComboBox = null;
        private WindowsElement BarCodeScannerTypeComboBox = null;
        private WindowsElement BarCodeScannerConfigurationComboBox = null;
        private WindowsElement RFIDReaderTypeComboBox = null;
        private WindowsElement RFIDReaderConfiurationComboBox = null;


        private IReadOnlyCollection<WindowsElement> OPTGeneralComboBoxList => AppiumDriver.FindElementsByXPath("//Text[@Name='Terminal:']/..//ComboBox");

        private IReadOnlyCollection<WindowsElement> OPTConfigurationComboBoxList => AppiumDriver.FindElementsByXPath("//Text[@Name='Hardware type:']/..//ComboBox");


        private WindowsElement TimetableFirstCell => AppiumDriver.FindElementByXPath("//DataItem[@Name='00:00 - 01:00']/Edit[@Name='Sunday']/Edit");

        private WindowsElement TimetablePopUpEndTime => AppiumDriver.FindElementByXPath("//Edit[@Name='Start time:']");
    
        private WindowsElement TimetablePopUpAllDays => AppiumDriver.FindElementByXPath("//CheckBox[@Name='All days']");

        private WindowsElement TimetablePopUpAddButton => AppiumDriver.FindElementByXPath("//Pane[@Name='End time:']/Button[@Name='Add']");

        public CryptoConfigurationScreen ConfigureTerminal(int terminalNumber, string terminalType)
        {
            SetOPTGeneralComboBoxes();

            //The value in the combo box is followed by 2 spaces for some reason, hence the trim.
            if (!TerminalComboBox.GetAttribute("Value.Value").Trim().Equals($"OPT {terminalNumber}"))
            {
                // PC type to pos1
                ComboBoxHelper.SetComboBox(TerminalComboBox, $"OPT {terminalNumber}");
            }

            // Type to Mux
            ComboBoxHelper.SetComboBox(TypeComboBox, terminalType);
            //Workaround for issue FP-7569
            TypeComboBox.Click();
            TypeComboBox.SendKeys(Keys.Enter);

            //Set Mode combobox to Normal
            ComboBoxHelper.SetComboBox(ModeComboBox, "Normal");

            //Check current status On
            CheckboxHelper.SetCheckbox(OnCheckBox, true);


            WeeklyTimetableTab.Click();

            SetWholeTimetableToGreen();

            ConfigurationTab.Click();

            SetOPTConfigurationComboBoxes();

            ComboBoxHelper.SetComboBox(PrinterTypeComboBox, "AXIOHM");

            ComboBoxHelper.SetComboBox(EMVKernelComboBox, "CVGA TOKEMV 5");

            ComboBoxHelper.SetComboBox(BarCodeScannerTypeComboBox, "Motorola");
            ComboBoxHelper.SetComboBox(BarCodeScannerConfigurationComboBox, "Integrated");

            //Open issue on this combobox, FP-7576, once this option is selected no other can be selected any more.
            ComboBoxHelper.SetComboBox(RFIDReaderTypeComboBox, "XAC C150S");
            ComboBoxHelper.SetComboBox(RFIDReaderConfiurationComboBox, "Down");

            return this;
        }

        public void SetWholeTimetableToGreen()
        {
            TimetableFirstCell.Click();
            TimetableFirstCell.SendKeys(Keys.Enter);

            TimetablePopUpEndTime.SendKeys("23:59");
            TimetablePopUpEndTime.SendKeys(Keys.Enter);

            //First click is to get out of the previous window.
            TimetablePopUpAllDays.Click();
            TimetablePopUpAllDays.Click();

            TimetablePopUpAddButton.Click();
        }

        public void SetOPTGeneralComboBoxes()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in OPTGeneralComboBoxList)
            {
                switch (counter)
                {
                    case 0:
                        TerminalComboBox = currentElement;
                        break;
                    case 1:
                        ModeComboBox = currentElement;
                        break;
                    case 2:
                        TypeComboBox = currentElement;
                        break;
                }
                counter++;
            }
        }

        //1= 
        public void SetOPTConfigurationComboBoxes()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in OPTConfigurationComboBoxList)
            {
                switch (counter)
                {
                    case 0:
                        //Unknown = currentElement;
                        break;
                    case 1:
                        BarCodeScannerConfigurationComboBox = currentElement;
                        break;
                    case 2:
                        BarCodeScannerTypeComboBox = currentElement;
                        break;
                    case 3:
                        RFIDReaderConfiurationComboBox = currentElement;
                        break;
                    case 4:
                        RFIDReaderTypeComboBox = currentElement;
                        break;
                    case 5:
                        //Shared BNA: = currentElement;
                        break;
                    case 6:
                        //BNA Type = currentElement;
                        break;
                    case 7:
                        //BNA Voucher starting method: = currentElement;
                        break;
                    case 8:
                        //Hardware type = currentElement;
                        break;
                    case 9:
                        EMVKernelComboBox = currentElement;
                        break;
                    case 10:
                        PrinterTypeComboBox = currentElement;
                        break;
                    case 11:
                        //Touchscreen = currentElement;
                        break;
                    case 12:
                        //PinPad Text = currentElement;
                        break;
                    case 13:
                        //PinPad = currentElement;
                        break;
                }
                counter++;
            }
        }
    }
}
