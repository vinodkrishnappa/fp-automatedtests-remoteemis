﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class EVChargersConfigurationScreen : BaseComponent
    {
        public EVChargersConfigurationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement NodeTextBox => AppiumDriver.FindElementByName("Node:");
        private WindowsElement PCComboBox => AppiumDriver.FindElementByName("PC:");
        private WindowsElement TypeComboBox => AppiumDriver.FindElementByName("Type:");

        public EVChargersConfigurationScreen ConfigureChargePoint()
        {
            //Currently the default configuration for  a Charge Point is good for the Clean Install Tests.
            return this;
        }
    }
}
