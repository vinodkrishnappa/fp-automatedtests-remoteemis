﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;
using Utils.Utils.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class FIPFCDPumpConfigurationScreen : BaseComponent
    {
        public FIPFCDPumpConfigurationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement NodeTextBox => AppiumDriver.FindElementByName("Node:");
        private WindowsElement PCComboBox => AppiumDriver.FindElementByName("PC:");
        private WindowsElement TypeComboBox => AppiumDriver.FindElementByName("Type:");
        private WindowsElement PortComboBox => AppiumDriver.FindElementByName("Port:");

        public FIPFCDPumpConfigurationScreen ConfigurePump(int pumpNumber)
        {
            // PC type to pos1
            ComboBoxHelper.SetComboBox(PCComboBox, DispensingConfigurations.s_pc_pos_1);

            // Type to Mux
            ComboBoxHelper.SetComboBox(TypeComboBox, DispensingConfigurations.s_type_mux);

            // Node to pump number
            NodeTextBox.SendKeys(pumpNumber.ToString());
            return this;
        }
    }
}
