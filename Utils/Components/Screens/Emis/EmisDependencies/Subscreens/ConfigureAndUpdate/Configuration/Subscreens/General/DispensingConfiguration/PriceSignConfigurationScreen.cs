﻿using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;
using Utils.Utils.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class PriceSignConfigurationScreen : BaseComponent
    {
        public PriceSignConfigurationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement NodeTextBox = null;
        private WindowsElement PCComboBox = null;
        private WindowsElement TypeComboBox = null;

        private IReadOnlyCollection<WindowsElement> PriceSignConfigurationComboBoxList => AppiumDriver.FindElementsByXPath("//Pane[@Name='Fusion refresh time:']//ComboBox");
        private IReadOnlyCollection<WindowsElement> PriceSignConfigurationEditList => AppiumDriver.FindElementsByXPath("//Pane[@Name='Fusion refresh time:']//Pane/Edit");

        public PriceSignConfigurationScreen ConfigurePriceSign(int PriceSignNumber)
        {
            //Required because the elements in this screen have no name or other consistent way
            //of being identified.
            SetPSGComboBoxElements();
            SetPSGEditElements();

            // PC type to pos1
            ComboBoxHelper.SetComboBox(PCComboBox, DispensingConfigurations.s_pc_pos_1);

            // Type to Mux
            ComboBoxHelper.SetComboBox(TypeComboBox, DispensingConfigurations.s_type_mux);

            // The node for the Price sign should be set to 33 for PSG1 en 34 for PSG2
            NodeTextBox.SendKeys((32 + PriceSignNumber).ToString());

            return this;
        }

        //0 = subtype, 1= port, 2 = type, 3 = PC
        public void SetPSGComboBoxElements()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in PriceSignConfigurationComboBoxList)
            {
                switch (counter)
                {
                    case 2:
                        TypeComboBox = currentElement;
                        break;
                    case 3:
                        PCComboBox = currentElement;
                        break;
                }
                counter++;
            }
        }
    
        //0 = subnode, 1 = node
        public void SetPSGEditElements()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in PriceSignConfigurationEditList)
            {
                switch (counter)
                {
                    case 1:
                        NodeTextBox = currentElement;
                        break;
                }
                counter++;
            }
        }
    }
}
