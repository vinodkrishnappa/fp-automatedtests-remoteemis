﻿using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;
using Utils.Utils.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class TankLevelGaugeConfigurationScreen : BaseComponent
    {
        public TankLevelGaugeConfigurationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement NodeTextBox = null;
        private WindowsElement PCComboBox = null;
        private WindowsElement TypeComboBox = null;

        private IReadOnlyCollection<WindowsElement> TLGConfigurationComboBoxList => AppiumDriver.FindElementsByXPath("//Text[@Name='Subtype:']/..//ComboBox");
        private IReadOnlyCollection<WindowsElement> TLGConfigurationEditList => AppiumDriver.FindElementsByXPath("//Text[@Name='Subtype:']/..//Pane/Edit");

        public TankLevelGaugeConfigurationScreen ConfigureTLG()
        {
            //Required because the elements in this screen have no name or other consistent way
            //of being identified.
            setTLGComboBoxElements();
            setTLGEditElements();

            // PC type to pos1
            PCComboBox.SendKeys("P");
            ComboBoxHelper.SetComboBox(PCComboBox, DispensingConfigurations.s_pc_pos_1);

            // Type to LON
            ComboBoxHelper.SetComboBox(TypeComboBox, DispensingConfigurations.s_type_lon);

            // Node to 1
            NodeTextBox.SendKeys("1");

            return this;
        }

        public void setTLGComboBoxElements()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in TLGConfigurationComboBoxList)
            {
                switch (counter)
                {
                    case 2:
                        TypeComboBox = currentElement;
                        break;
                    case 3:
                        PCComboBox = currentElement;
                        break;
                }
                counter++;
            }
        }

        public void setTLGEditElements()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in TLGConfigurationEditList)
            {
                switch (counter)
                {
                    case 1:
                        NodeTextBox = currentElement;
                        break;
                }
                counter++;
            }
        }
    }
}
