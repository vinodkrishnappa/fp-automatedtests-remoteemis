﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class DispensingConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly EVChargersConfigurationScreen _evChargersConfigurationScreen;
        private readonly FIPFCDPumpConfigurationScreen _fipFcdPumpConfigurationScreen;
        private readonly PriceSignConfigurationScreen _priceSignConfigurationScreen;
        private readonly TankLevelGaugeConfigurationScreen _tankLevelGaugeConfigurationScreen;

        #endregion Subscreens

        public DispensingConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _priceSignConfigurationScreen = new PriceSignConfigurationScreen(this);
            _tankLevelGaugeConfigurationScreen = new TankLevelGaugeConfigurationScreen(this);
            _fipFcdPumpConfigurationScreen = new FIPFCDPumpConfigurationScreen(this);
            _evChargersConfigurationScreen = new EVChargersConfigurationScreen(this);
            _emisIconToolbar = toolbar;
        }

        private WindowsElement AddItemButton => AppiumDriver.FindElementByName("-->");
        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");
        private WindowsElement EVChargersConfigurationItem => AppiumDriver.FindElementByName("EV Chargers");
        private WindowsElement FIPFCDConfigurationItem => AppiumDriver.FindElementByName("FIP/FCD configuration");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement PriceSignConfigurationItem => AppiumDriver.FindElementByName("Price sign configuration");
        private WindowsElement TankLevelGaugeConfigurationItem => AppiumDriver.FindElementByName("Tank Level Gauge configuration");

        public DispensingConfigurationScreen ConfigureChargePoint(int chargePointNumber)
        {
            AppiumWebElement GetCharger() => EVChargersConfigurationItem.FindElementByName($"Charge point {chargePointNumber}");

            WaitUntil(
                () => EVChargersConfigurationItem.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the EV Chargers tab to be visible took too long."
            );

            bool chargePointExists = DoesElementExist(GetCharger);
            if (!chargePointExists)
            {
                EVChargersConfigurationItem.Click();
                _emisIconToolbar.AddButton.Click();
                AddItemButton.Click();
                OkButton.Click();
            }
            GetCharger().Click();
            _evChargersConfigurationScreen.ConfigureChargePoint();
            Logger.LogInfo("Added and configured EV Charge point: " + chargePointNumber);

            return this;
        }

        public DispensingConfigurationScreen ConfigurePriceSign(int PriceSignNumber)
        {
            AppiumWebElement GetPriceSign(int Number) => PriceSignConfigurationItem.FindElementByName($"PSG {Number}");

            WaitUntil(
                () => PriceSignConfigurationItem.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Price Sign tab to be visible took too long."
            );

            bool PriceSignExists = DoesElementExist(() => GetPriceSign(PriceSignNumber));
            if (!PriceSignExists)
            {
                PriceSignConfigurationItem.Click();
                _emisIconToolbar.AddButton.Click();
                AddItemButton.Click();
                OkButton.Click();
            }
            GetPriceSign(PriceSignNumber).Click();
            _priceSignConfigurationScreen.ConfigurePriceSign(PriceSignNumber);
            Logger.LogInfo($"Added and configured Price Sign '{PriceSignNumber}");

            return this;
        }

        public DispensingConfigurationScreen ConfigurePump(int pumpNumber)
        {
            AppiumWebElement GetPump() => FIPFCDConfigurationItem.FindElementByName($"Pump {pumpNumber}");

            WaitUntil(
                () => FIPFCDConfigurationItem.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Pump tab to be visible took too long."
            );

            bool pumpExists = DoesElementExist(GetPump);
            if (!pumpExists)
            {
                FIPFCDConfigurationItem.Click();
                _emisIconToolbar.AddButton.Click();
                AddItemButton.Click();
                OkButton.Click();
                GetPump().Click();
                _fipFcdPumpConfigurationScreen.ConfigurePump(pumpNumber);
            }
            Logger.LogInfo("Added and configured Pump: " + pumpNumber);

            return this;
        }

        public DispensingConfigurationScreen ConfigureTankLevelGauge()
        {
            AppiumWebElement GetTLG() => TankLevelGaugeConfigurationItem.FindElementByName("TLG");

            WaitUntil(
                () => TankLevelGaugeConfigurationItem.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the TLG tab to be visible took too long."
            );

            bool tlgExists = DoesElementExist(GetTLG);
            if (!tlgExists)
            {
                TankLevelGaugeConfigurationItem.Click();
                _emisIconToolbar.AddButton.Click();
                AddItemButton.Click();
                OkButton.Click();
            }
            GetTLG().Click();
            _tankLevelGaugeConfigurationScreen.ConfigureTLG();

            Logger.LogInfo("Added and configured TLG");
            return this;
        }

        public void SetupPumpsOneAndTwo() => ConfigurePump(1).ConfigurePump(2);
    }
}
