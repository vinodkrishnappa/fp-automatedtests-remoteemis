﻿using System;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class FuelAndArticleProgrammingScreen : BaseComponent
    {
        public FuelAndArticleProgrammingScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement ComposedArticlesCheckBox => AppiumDriver.FindElementByName("Composed articles");
        private WindowsElement FreeGroupCodeInputCheckBox => AppiumDriver.FindElementByName("Free group code input");

        public FuelAndArticleProgrammingScreen SetComposedArticlesCheckbox(bool check = true)
        {
            WaitUntil(
                () => DoesElementExist(() => ComposedArticlesCheckBox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Fuel an Article Programming screen to load took too long."
            );

            CheckboxHelper.SetCheckbox(ComposedArticlesCheckBox, check);

            Logger.LogInfo("Succesfuly set the 'Composed Articles' checbox to '" + check + "'.");
            return this;
        }

        public FuelAndArticleProgrammingScreen SetFreeGroupCodeInputCheckbox(bool check = true)
        {
            CheckboxHelper.SetCheckbox(FreeGroupCodeInputCheckBox, check);
            Logger.LogInfo("Succesfuly set the 'Free Group Code Input' checbox to '" + check + "'.");
            return this;
        }
    }
}
