﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class FuelConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        public FuelConfigurationScreen(BaseComponent parent, EmisIconToolbar emisIconToolbar) : base(parent) => _emisIconToolbar = emisIconToolbar;

        public WindowsElement CardCodeTextBox => AppiumDriver.FindElementByXPath("//Text[@Name='Card code:']/../Pane/Edit");
        public WindowsElement CodesTabItem => AppiumDriver.FindElementByXPath("//TabItem[@Name='Codes']");
        public WindowsElement FuelConfigurationItem => AppiumDriver.FindElementByName("Fuel configuration");
        public WindowsElement GeneralTabItem => AppiumDriver.FindElementByXPath("//TabItem[@Name='General']");
        private WindowsElement AddButton => AppiumDriver.FindElementByName("Add");
        private WindowsElement BasePriceTextField => AppiumDriver.FindElementByXPath("//Text[@Name='Price activation 1']/following-sibling::*");
        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");
        private WindowsElement FuelNumberComboBox => AppiumDriver.FindElementByName("Fuel number:");
        private WindowsElement FuelsItem => AppiumDriver.FindElementByName("Fuels");
        private IReadOnlyCollection<WindowsElement> ListOfAllFuels => AppiumDriver.FindElementsByXPath("//DataItem[@Name='Fuels']//DataItem");
        private WindowsElement MaxPriceChangeAllowedTextBox => AppiumDriver.FindElementByXPath("//Text[@Name='Maximum price change allowed %:']/../Pane/Edit");
        private WindowsElement PriceActivationCombobox => AppiumDriver.FindElementByXPath("//ComboBox[@Name=':']");

        public FuelConfigurationScreen AddFuel(int price = 5)
        {
            FuelsItem.Click();
            _emisIconToolbar.AddButton.Click();
            PriceActivationCombobox.Click();
            BasePriceTextField.SendKeys(price.ToString());
            AddButton.Click();
            CancelButton.Click();
            return this;
        }

        /// <summary>
        /// Dynamically adds a fuel, if it already exists we only reconfigure it.
        /// </summary>
        /// <param name="fuelName"></param>
        /// <returns></returns>
        public FuelConfigurationScreen AddFuel(string fuelName)
        {
            WaitUntil(
                () => DoesElementExist(() => FuelsItem),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(20),
                "Waiting for Fuel Configuration window to open took too long."
            );

            //If the fuel does not exist, add a new one and then configure it.
            if (!CheckIfFuelExists(fuelName))
            {
                FuelsItem.Click();
                _emisIconToolbar.AddButton.Click();

                //Sets the fuel settings based on the Fuel Name
                SetFuelSettings(fuelName);

                AddButton.Click();

                CancelButton.Click();

                Logger.LogInfo($"Succesfuly added and configured Fuel '{fuelName}'.");
            }
            //If the fuel already exists, don't add a new one but reconfigure the existing one.
            else
            {
                GetFuel(fuelName).Click();

                //Sets the fuel settings based on the Fuel Name
                SetFuelSettings(fuelName);

                Logger.LogInfo($"Fuel '{fuelName}' already existed. reconfigured it.");
            }
            return this;
        }

        /// <summary>
        /// Counts the number of Card Codes in the list on the Codes tab in the Fuel Configuration
        /// </summary>
        /// <returns></returns>
        public int CountNumberOfCardCodes()
        {
            int actualNumberOfCardCodes = 1;
            string currentCardCode = null;
            bool endOfList = false;

            //Used to store the CardCodes which are already counted, to not count duplicates
            List<string> foundCardCodesList = new List<string>();

            //Counts the first, by default selected Card code
            currentCardCode = CardCodeTextBox.GetAttribute("Value.Value");

            //We only store the Code part of the Card code in the list to make the comparisson easier
            //Format eg. '125 - Super'

            try
            {
                foundCardCodesList.Add(currentCardCode.Split(new string[] { " - " }, StringSplitOptions.None)[0]);
            }
            catch (Exception)
            {
                //If the default field is already empty (And will throw an exception when trying to split
                //that means the whole list is empty

                actualNumberOfCardCodes--;
                endOfList = true;
            }

            //Loops through the rest of the dropdown list using the arrow keys
            //The elements in this list item are not reachable through xpath so we use this method
            //When a card code is encountered that is not yet counted we add to the counter
            //The list contains duplicates of the same card code, only ordered differently, number first or name first
            //We dont count duplicates.

            while (!endOfList && actualNumberOfCardCodes < 15)
            {
                try
                {
                    //Selects the next Card Code from the dropdown list
                    CardCodeTextBox.SendKeys(Keys.Down);
                    CardCodeTextBox.SendKeys(Keys.Enter);
                    currentCardCode = CardCodeTextBox.GetAttribute("Value.Value");
                    foreach (string currentFoundCardCode in foundCardCodesList)
                    {
                        if (currentCardCode.Contains(currentFoundCardCode))
                        {
                            endOfList = true;
                            actualNumberOfCardCodes--;
                        }
                    }

                    //We only store the Code part of the Card code in the list to make the comparisson easier
                    //Format eg. '125 - Super'

                    foundCardCodesList.Add(currentCardCode.Split(new string[] { " - " }, StringSplitOptions.None)[0]);
                    actualNumberOfCardCodes++;
                }
                catch (Exception)
                {
                    endOfList = true;
                }
            }

            return actualNumberOfCardCodes;
        }

        public WindowsElement FirstListedFuel()
        {
            try
            {
                return AppiumDriver.FindElementByXPath("//DataItem[@Name='Fuels']/DataItem");
            }
            catch (Exception e)
            {
                TestContext.Progress.Write("Failed to find a configured Fuel in the Fuel Configuration settings, a Fuel is required for this test.");
                throw new Exception($"Failed to find a configured Fuel in the Fuel Configuration settings, a Fuel is required for this test.\n {e.Message} \n {e.StackTrace}");
            }
        }

        public void SaveFuelsWithWaitForPopUp()
        {
            //AppiumWebElement GetFuelPricesActivatedPopUpText() => AppiumDriver.FindElementByName("Fuel prices and/or discounts activated");

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            Logger.LogInfo("Waiting for a maximum of 35 seconds for the 'Fuel prices activated' Pop-Up, so it does not intervene with the following steps.");
            Thread.Sleep(30000);

            //Not yet able to get this popup, it seems to be outside of eMis
            //Hardcoding a wait for now as this is a very consistent time to wait anyway.
            /*
            //This consistently takes 30 seconds to show.
            WaitUntilUtil.WaitUntil(
                () => DoesElementExist(GetFuelPricesActivatedPopUpText),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(35),
                "Waiting for Fuel Price Activation Pop Up took to long"
            );
            */
        }

        public void SetFuelSettings(string fuelName)
        {
            WaitUntil(
                () => DoesElementExist(() => FuelNumberComboBox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(20),
                "Waiting for Fuel Configuration window to open took too long."
            );

            switch (fuelName)
            {
                case Constants.FuelNameSuper:
                    FuelNumberComboBox.SendKeys("1");
                    BasePriceTextField.SendKeys("1.1");
                    MaxPriceChangeAllowedTextBox.SendKeys("10");
                    break;

                case Constants.FuelNameSuperPlus:
                    FuelNumberComboBox.SendKeys("2");
                    BasePriceTextField.SendKeys("1.2");
                    MaxPriceChangeAllowedTextBox.SendKeys("20");
                    break;

                case Constants.FuelNameDiesel:
                    FuelNumberComboBox.SendKeys("3");
                    BasePriceTextField.SendKeys("1.3");
                    MaxPriceChangeAllowedTextBox.SendKeys("30");
                    break;

                case Constants.FuelNameLPG:
                    FuelNumberComboBox.SendKeys("4");
                    BasePriceTextField.SendKeys("1.4");
                    MaxPriceChangeAllowedTextBox.SendKeys("40");
                    break;

                default:
                    FuelNumberComboBox.SendKeys("1");
                    BasePriceTextField.SendKeys("1.5");
                    MaxPriceChangeAllowedTextBox.SendKeys("50");
                    break;
            }
            PriceActivationCombobox.Click();
            //Confirm the selection from the dropdownlist.
            PriceActivationCombobox.SendKeys(Keys.Enter);
        }

        /// <summary>
        /// Validates the number of CardCodes in the Fuel Configuration tab of the Fuel Pos configuration settings, 9-a-1
        /// This check is used after sending in the CRDC MUT file, there was an issue where the added
        /// Card codes would not be visible in this Fuel configuration tab.
        /// </summary>
        /// <param name="expectedNumberOfCardCodes"></param>
        /// <returns></returns>
        public bool ValidateNumberOfCardCodesInFuelSettings(int expectedNumberOfCardCodes)
        {
            int actualNumberOfCardCodes = 0;

            //Navigate to the Codes tab of the Fuel Configuration
            FuelConfigurationItem.Click();
            FirstListedFuel().Click();

            //This click takes about 15 seconds, reason unknown, tried several workarounds but none seem appropriate.
            CodesTabItem.Click();

            //Counts the number of card codes by tabbing through the list item
            actualNumberOfCardCodes = CountNumberOfCardCodes();

            if (actualNumberOfCardCodes == expectedNumberOfCardCodes)
            {
                TestContext.Progress.Write("Expected number of Card Codes in Fuel Settings MATCHES actual number, Expected: "
                    + expectedNumberOfCardCodes + " | Actual: " + actualNumberOfCardCodes);
                return true;
            }
            else
            {
                TestContext.Progress.Write("Expected number of Card Codes in Fuel Settings DOES NOT match actual number, Expected: "
                    + expectedNumberOfCardCodes + " | Actual: " + actualNumberOfCardCodes);
                return false;
            }
        }

        /// <summary>
        /// Gets all the existing fuels and checks if any have a matching name.
        /// </summary>
        /// <param name="fuelName"></param>
        /// <returns></returns>
        private bool CheckIfFuelExists(string fuelName)
        {
            bool exists = false;
            foreach (WindowsElement currentFuel in ListOfAllFuels)
            {
                if (currentFuel.GetAttribute("Name").Equals(fuelName))
                    exists = true;
            }
            return exists;
        }

        private WindowsElement GetFuel(string fuelName) => AppiumDriver.FindElementByName(fuelName);
    }
}
