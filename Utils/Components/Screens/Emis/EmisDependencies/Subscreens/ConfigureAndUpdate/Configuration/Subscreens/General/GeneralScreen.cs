﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class GeneralScreen : BaseComponent
    {
        #region Subscreens

        private readonly ChargePointConnectorScreen _chargePointConnectorScreen;
        private readonly CISPeripheralsConfigurationScreen _cisPeripheralsConfigurationScreen;
        private readonly DispensingConfigurationScreen _dispensingConfigurationScreen;
        private readonly EmisIconToolbar _emisIconToolbar;
        private readonly FuelAndArticleProgrammingScreen _fuelAndArticleProgrammingScreen;
        private readonly FuelConfigurationScreen _fuelConfigurationScreen;
        private readonly LanguageSelectionScreen _languageSelectionScreen;
        private readonly LoyaltyPromotionDiscountConfigurationScreen _loyaltyPromotionDiscountScreen;
        private readonly OnlineHostConfigurationScreen _onlineHostConfigurationScreen;
        private readonly OPTConfigurationScreen _optConfigurationScreen;
        private readonly PaymentModeConfigurationScreen _paymentModeConfigurationScreen;
        private readonly POSPeripheralsConfigurationScreen _posPeripheralsConfigurationScreen;
        private readonly PumpsConfigurationScreen _pumpModeConfigurationScreen;
        private readonly SalesOptionsConfigurationScreen _salesOptionsConfigurationScreen;
        private readonly StationInformationScreen _stationInformationScreen;
        private readonly TankGroupsAndNozzlesScreen _tankGroupsAndNozzlesScreen;
        private readonly TariffConfigurationScreen _tariffConfigurationScreen;
        private readonly TariffProgrammingScreen _tariffProgrammingScreen;

        #endregion Subscreens

        public GeneralScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _fuelAndArticleProgrammingScreen = new FuelAndArticleProgrammingScreen(this);
            _stationInformationScreen = new StationInformationScreen(this);
            _paymentModeConfigurationScreen = new PaymentModeConfigurationScreen(this, toolbar);
            _onlineHostConfigurationScreen = new OnlineHostConfigurationScreen(this, toolbar);
            _salesOptionsConfigurationScreen = new SalesOptionsConfigurationScreen(this, toolbar);
            _loyaltyPromotionDiscountScreen = new LoyaltyPromotionDiscountConfigurationScreen(this, toolbar);
            _tankGroupsAndNozzlesScreen = new TankGroupsAndNozzlesScreen(this, toolbar);
            _pumpModeConfigurationScreen = new PumpsConfigurationScreen(this, toolbar);
            _dispensingConfigurationScreen = new DispensingConfigurationScreen(this, toolbar);
            _optConfigurationScreen = new OPTConfigurationScreen(this, toolbar);
            _tariffProgrammingScreen = new TariffProgrammingScreen(this, toolbar);
            _tariffConfigurationScreen = new TariffConfigurationScreen(this, toolbar);
            _chargePointConnectorScreen = new ChargePointConnectorScreen(this, toolbar);
            _fuelConfigurationScreen = new FuelConfigurationScreen(this, toolbar);
            _languageSelectionScreen = new LanguageSelectionScreen(this);
            _posPeripheralsConfigurationScreen = new POSPeripheralsConfigurationScreen(this, toolbar);
            _cisPeripheralsConfigurationScreen = new CISPeripheralsConfigurationScreen(this, toolbar);
            _emisIconToolbar = toolbar;
        }

        public FuelAndArticleProgrammingScreen FuelAndArticleProgramming
        {
            get
            {
                WaitUntil(
                () => FuelAndArticleProgrammingItem.Displayed,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Fuel an Article Programming screen to load took too long."
                );
                Thread.Sleep(1000);

                FuelAndArticleProgrammingItem.Click();
                return _fuelAndArticleProgrammingScreen;
            }
        }

        public FuelConfigurationScreen FuelConfiguration
        {
            get
            {
                FuelConfigurationItem.Click();
                return _fuelConfigurationScreen;
            }
        }

        public LanguageSelectionScreen LanguageSelection
        {
            get
            {
                LanguageSelectionItem.Click();
                return _languageSelectionScreen;
            }
        }

        public StationInformationScreen StationInformation
        {
            get
            {
                StationInformationItem.Click();
                return _stationInformationScreen;
            }
        }

        public TankGroupsAndNozzlesScreen TankGroupsAndNozzles
        {
            get
            {
                TankGroupsAndNozzlesItem.Click();
                return _tankGroupsAndNozzlesScreen;
            }
        }

        private WindowsElement ChargePointConnectorsItem => AppiumDriver.FindElementByName("Charge point connectors");
        private WindowsElement CISPeripheralsItem => AppiumDriver.FindElementByName("CIS peripherals");
        private WindowsElement DispensingConfigurationItem => AppiumDriver.FindElementByName("Dispensing configuration");
        private WindowsElement FuelAndArticleProgrammingItem => AppiumDriver.FindElementByName("Fuel and article programming");
        private WindowsElement FuelConfigurationItem => AppiumDriver.FindElementByName("Fuel configuration");
        private WindowsElement LanguageSelectionItem => AppiumDriver.FindElementByName("Language selection");

        //This element is not clickable, probably because the name is so long that the click falls outside the area.
        private WindowsElement LoyaltyPromotionDiscountConfigurationItem => AppiumDriver.FindElementByName("Loyalty/promotion/discount");

        private WindowsElement OKButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement OnlineHostConfigurationItem => AppiumDriver.FindElementByName("On-line host");
        private WindowsElement OPTConfigurationItem => AppiumDriver.FindElementByName("OPT configuration");
        private WindowsElement PaymentModeConfigurationItem => AppiumDriver.FindElementByName("Payment modes");
        private WindowsElement POSPeripheralsItem => AppiumDriver.FindElementByName("POS peripherals");
        private WindowsElement PumpsItem => AppiumDriver.FindElementByName("Pumps");
        private WindowsElement SalesOptionsConfigurationItem => AppiumDriver.FindElementByXPath("//DataItem[@Name='Sales options']");
        private WindowsElement StationInformationItem => AppiumDriver.FindElementByName("Station information");
        private WindowsElement TankGroupsAndNozzlesItem => AppiumDriver.FindElementByName("Tank groups and nozzles");
        private WindowsElement TariffConfigurationItem => AppiumDriver.FindElementByName("Tariff configuration");
        private WindowsElement TariffProgrammingItem => AppiumDriver.FindElementByName("Tariff programming");

        public ChargePointConnectorScreen SetupChargePointConnectors()
        {
            ChargePointConnectorsItem.Click();
            return _chargePointConnectorScreen;
        }

        public CISPeripheralsConfigurationScreen SetupCISPeripheralsScreen()
        {
            CISPeripheralsItem.Click();
            return _cisPeripheralsConfigurationScreen;
        }

        public DispensingConfigurationScreen SetupDispensingConfiguration()
        {
            DispensingConfigurationItem.Click();
            return _dispensingConfigurationScreen;
        }

        public LoyaltyPromotionDiscountConfigurationScreen SetupLoyaltyPromotionDiscountConfiguration()
        {
            //The loyalty promotion discount list item is not clickable, using this workaround instead.
            SalesOptionsConfigurationItem.Click();
            SalesOptionsConfigurationItem.SendKeys(Keys.ArrowDown);

            WaitUntil(
                () => _emisIconToolbar.AddButton.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the add button to become enabled took too long."
            );

            return _loyaltyPromotionDiscountScreen;
        }

        public OnlineHostConfigurationScreen SetupOnlineHostConfiguration()
        {
            OnlineHostConfigurationItem.Click();

            WaitUntil(
                () => DoesElementExist(() => OKButton),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the OK button in the 'No online hosts configured pop up' to exist took too long."
            );
            OKButton.Click();

            return _onlineHostConfigurationScreen;
        }

        public OPTConfigurationScreen SetupOPTConfiguration()
        {
            OPTConfigurationItem.Click();
            return _optConfigurationScreen;
        }

        public PaymentModeConfigurationScreen SetupPaymentModeConfiguration()
        {
            PaymentModeConfigurationItem.Click();
            return _paymentModeConfigurationScreen;
        }

        public POSPeripheralsConfigurationScreen SetupPOSPeripheralsScreen()
        {
            POSPeripheralsItem.Click();
            return _posPeripheralsConfigurationScreen;
        }

        public DispensingConfigurationScreen SetupPriceSignConfiguration()
        {
            DispensingConfigurationItem.Click();
            return _dispensingConfigurationScreen;
        }

        public PumpsConfigurationScreen SetupPumpsConfiguration()
        {
            PumpsItem.Click();
            return _pumpModeConfigurationScreen;
        }

        public SalesOptionsConfigurationScreen SetupSalesOptionsConfiguration()
        {
            SalesOptionsConfigurationItem.Click();

            WaitUntil(
                () => _emisIconToolbar.AddButton.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the OK button in the 'No online hosts configured pop up' to exist took too long."
            );

            return _salesOptionsConfigurationScreen;
        }

        public TariffConfigurationScreen SetupTariffConfiguration()
        {
            TariffConfigurationItem.Click();
            return _tariffConfigurationScreen;
        }

        public TariffProgrammingScreen SetupTariffProgramming()
        {
            TariffProgrammingItem.Click();
            return _tariffProgrammingScreen;
        }
    }
}
