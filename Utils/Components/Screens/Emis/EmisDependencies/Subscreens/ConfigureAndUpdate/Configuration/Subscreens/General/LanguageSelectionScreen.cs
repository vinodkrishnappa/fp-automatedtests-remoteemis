﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.Enums;
using Utils.Utils.ElementHelpers;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class LanguageSelectionScreen : BaseComponent
    {
        public LanguageSelectionScreen(BaseComponent parent) : base(parent)
        {
        }

        public LanguageSelectionScreen SetLanguageFour(Languages language) => SetLanguage(3, language);

        public LanguageSelectionScreen SetLanguageOne(Languages language) => SetLanguage(0, language);

        public LanguageSelectionScreen SetLanguageThree(Languages language) => SetLanguage(2, language);

        public LanguageSelectionScreen SetLanguageTwo(Languages language) => SetLanguage(1, language);

        private IReadOnlyCollection<WindowsElement> LanguageComboBoxList => AppiumDriver.FindElementsByXPath("//Text[@Name='Default customer language:']/..//ComboBox");

        private WindowsElement DefaultTicketLanguageComboBox = null;
        private WindowsElement DefaultCustomerLanguageComboBox = null;
        private WindowsElement LanguageOneComboBox = null;
        private WindowsElement LanguageTwoComboBox = null;
        private WindowsElement LanguageThreeComboBox = null;
        private WindowsElement LanguageFourComboBox = null;

        private LanguageSelectionScreen SetLanguage(int comboboxIndex, Languages language)
        {
            string repeatedString = string.Concat(Enumerable.Repeat("/following-sibling::*", comboboxIndex));
            string xPath = string.Format("//Text[@Name='Default customer language:']{0}", repeatedString);
            WindowsElement comboBox = AppiumDriver.FindElementByXPath(xPath);

            comboBox.Click();
            comboBox.SendKeys(language);
            return this;
        }

        public void SetAllLanguages(string language1, string language2, string language3, string language4)
        {
            SetLanguageComboBoxes();

            ComboBoxHelper.SetComboBox(LanguageOneComboBox, language1);
            ComboBoxHelper.SetComboBox(LanguageTwoComboBox, language2);
            ComboBoxHelper.SetComboBox(LanguageThreeComboBox, language3);
            ComboBoxHelper.SetComboBox(LanguageFourComboBox, language4);
        }

        public void SetLanguageComboBoxes()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in LanguageComboBoxList)
            {
                switch (counter)
                {
                    case 0:
                        DefaultTicketLanguageComboBox = currentElement;
                        break;
                    case 1:
                        DefaultCustomerLanguageComboBox = currentElement;
                        break;
                    case 2:
                        LanguageOneComboBox = currentElement;
                        break;
                    case 3:
                        LanguageTwoComboBox = currentElement;
                        break;
                    case 4:
                        LanguageThreeComboBox = currentElement;
                        break;
                    case 5:
                        LanguageFourComboBox = currentElement;
                        break;
                }
                counter++;
            }
        }
    }
}
