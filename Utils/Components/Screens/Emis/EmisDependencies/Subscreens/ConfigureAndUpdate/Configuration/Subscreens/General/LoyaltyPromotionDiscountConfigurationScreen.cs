﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils.ElementHelpers;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class LoyaltyPromotionDiscountConfigurationScreen : BaseComponent
    {
        private readonly EmisIconToolbar _emisIconToolbar;

        public LoyaltyPromotionDiscountConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _emisIconToolbar = toolbar;
        }

        private WindowsElement DiscountVouchersCheckBox => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Discount vouchers']");
        private WindowsElement LoyaltyDiscountComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Loyalty discount/points on individual items:']");
        private WindowsElement PromotionsComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Promotions:']");

        public LoyaltyPromotionDiscountConfigurationScreen SetDiscountVouchers(bool check)
        {
            CheckboxHelper.SetCheckbox(DiscountVouchersCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }

        public LoyaltyPromotionDiscountConfigurationScreen SetLoyaltyDiscountOnIndividualItems(string loyaltySelection)
        {
            ComboBoxHelper.SetComboBox(LoyaltyDiscountComboBox, loyaltySelection);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }

        public LoyaltyPromotionDiscountConfigurationScreen SetPromotions(string promotionSelection)
        {
            ComboBoxHelper.SetComboBox(PromotionsComboBox, promotionSelection);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }
    }
}
