﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.CryptoConfiguration;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class OPTConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly CryptoConfigurationScreen _cryptoConfigurationScreen;

        #endregion Subscreens

        public OPTConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _cryptoConfigurationScreen = new CryptoConfigurationScreen(this);
            _emisIconToolbar = toolbar;
        }

        private WindowsElement AddItemButton => AppiumDriver.FindElementByName("-->");
        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement OPTConfiguration => AppiumDriver.FindElementByName("OPT configuration");

        public OPTConfigurationScreen ConfigureOutdoorPaymentTeminal(int terminalNumber, string terminalType)
        {
            AppiumWebElement GetTerminal() => OPTConfiguration.FindElementByName($"{ terminalType } { terminalNumber.ToString()} (Cards)");

            WaitUntil(
                () => OPTConfiguration.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the OPT configuration screen to open took too long."
            );

            if (!DoesElementExist(GetTerminal))
            {
                OPTConfiguration.Click();
                _emisIconToolbar.AddButton.Click();

                _cryptoConfigurationScreen.ConfigureTerminal(terminalNumber, terminalType);

                OkButton.Click();
                CancelButton.Click();
            }
            else
            {
                _cryptoConfigurationScreen.ConfigureTerminal(terminalNumber, terminalType);
            }

            Logger.LogInfo("Added and configured OPT: " + terminalNumber);

            return this;
        }

        //Required because the Click areas of the different tree items heavily overlap
        //This causes the Pump list item to be clicked sometimes when we want to
        //Click the charge point item.
        public void GoToCharePointsListItem(int terminalNumber, string terminalType)
        {
            AppiumWebElement GetPumpsItem() => OPTConfiguration.FindElementByXPath
                ($"//DataItem[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']/DataItem[@Name='Pumps']");
            AppiumWebElement GetPump() => OPTConfiguration.FindElementByXPath
                ($"//DataItem[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']//DataItem[@Name='Pump 1']");

            GetPumpsItem().Click();

            //If the pumps tree is unfolded, press the left arrow to fold it up.
            if (DoesElementExist(GetPump) && GetPump().Displayed)
            {
                GetPumpsItem().SendKeys(Keys.ArrowLeft);
            }

            //Then navigate from the Pumps item one down to the Charge points item.
            GetPumpsItem().SendKeys(Keys.ArrowDown);
        }

        public OPTConfigurationScreen LinkChargePointToOPT(int chargePointNumber, int terminalNumber, string terminalType)
        {
            AppiumWebElement GetTerminal() => OPTConfiguration.FindElementByName($"{ terminalType } { terminalNumber.ToString()} (Cards)");

            AppiumWebElement GetChargePoint() => OPTConfiguration.FindElementByXPath
                ($"//DataItem[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']//DataItem[@Name='Charge point {chargePointNumber}']");
            AppiumWebElement GetChargePointInAddWindow() => AppiumDriver.FindElementByXPath
                ($"//Window[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']//ListItem[@Name='Charge point {chargePointNumber}']");

            Assert.That(DoesElementExist(GetTerminal), $"Unable to add charge point to OPT, OPT does not exist: { terminalType } { terminalNumber.ToString()} (Cards)");

            if (!DoesElementExist(GetChargePoint))
            {
                //Required because the Click areas of the different tree items heavily overlap
                //This causes the Pump list item to be clicked sometimes when we want to
                //Click the charge point item.
                GoToCharePointsListItem(terminalNumber, terminalType);

                WaitUntil(
                () => _emisIconToolbar.LinkButton.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Link button to be enabled took too long."
                );
                _emisIconToolbar.LinkButton.Click();

                GetChargePointInAddWindow().Click();
                AddItemButton.Click();
                OkButton.Click();
                Logger.LogInfo($"Linked charge point {chargePointNumber} to OPT {terminalType} {terminalNumber}");
            }
            return this;
        }

        public OPTConfigurationScreen LinkPumpToOPT(int pumpNumber, int terminalNumber, string terminalType)
        {
            AppiumWebElement GetTerminal() => OPTConfiguration.FindElementByName($"{ terminalType } { terminalNumber.ToString()} (Cards)");
            AppiumWebElement GetPumpsItem() => OPTConfiguration.FindElementByXPath
                ($"//DataItem[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']/DataItem[@Name='Pumps']");
            AppiumWebElement GetPump() => OPTConfiguration.FindElementByXPath
                ($"//DataItem[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']//DataItem[@Name='Pump {pumpNumber}']");
            AppiumWebElement GetPumpInAddWindow() => AppiumDriver.FindElementByXPath
                ($"//Window[@Name='{ terminalType } { terminalNumber.ToString()} (Cards)']//ListItem[@Name='Pump {pumpNumber}']");

            Assert.That(DoesElementExist(GetTerminal), $"Unable to add pump to OPT, OPT does not exist: { terminalType } { terminalNumber.ToString()} (Cards)");

            if (!DoesElementExist(GetPump))
            {
                GetPumpsItem().Click();

                WaitUntil(
                () => _emisIconToolbar.LinkButton.Enabled,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Link button to be enabled took too long."
                );
                _emisIconToolbar.LinkButton.Click();

                GetPumpInAddWindow().Click();
                AddItemButton.Click();
                OkButton.Click();
                Logger.LogInfo($"Linked pump {pumpNumber} to OPT {terminalType} {terminalNumber}");
            }
            return this;
        }
    }
}
