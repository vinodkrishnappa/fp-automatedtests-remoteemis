﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils.ElementHelpers;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class OnlineHostConfigurationScreen : BaseComponent
    {
        private readonly EmisIconToolbar _emisIconToolbar;

        public OnlineHostConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _emisIconToolbar = toolbar;
        }

        private WindowsElement EVoucherTypeComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='E-voucher type:']");

        private WindowsElement LoyaltySchemeComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Primary loyalty scheme:']");

        private WindowsElement OasePayCheckBox => AppiumDriver.FindElementByXPath("//CheckBox[@Name='OASE_PAY']");
        private WindowsElement OnlineServicesTab => AppiumDriver.FindElementByXPath("//TabItem[@Name='On-line services']");
        private WindowsElement YesButton => AppiumDriver.FindElementByName("Yes");

        public OnlineHostConfigurationScreen NavigateToOnlineServicesTab()
        {
            OnlineServicesTab.Click();

            return this;
        }

        public OnlineHostConfigurationScreen SetEvoucherType(string eVoucherType)
        {
            ComboBoxHelper.SetComboBox(EVoucherTypeComboBox, eVoucherType);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }

        public OnlineHostConfigurationScreen SetLoyaltyScheme(string loyaltyScheme)
        {
            ComboBoxHelper.SetComboBox(LoyaltySchemeComboBox, loyaltyScheme);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            if (DoesElementExist(() => YesButton))
            {
                //To confirm we want to save the changes with a restart when we leave the config.
                YesButton.Click();
            }

            return this;
        }

        public OnlineHostConfigurationScreen SetOasePay(bool check)
        {
            CheckboxHelper.SetCheckbox(OasePayCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            if (DoesElementExist(() => YesButton))
            {
                //To confirm we want to save the changes with a restart when we leave the config.
                YesButton.Click();
            }

            return this;
        }
    }
}
