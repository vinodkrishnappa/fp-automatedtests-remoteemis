﻿using System;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class POSPeripheralsConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        public POSPeripheralsConfigurationScreen(BaseComponent parent, EmisIconToolbar emisIconToolbar) : base(parent) => _emisIconToolbar = emisIconToolbar;

        public WindowsElement CustomerDisplayComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Customer display:']");
        public WindowsElement PinPadComboBox => AppiumDriver.FindElementByXPath("//ComboBox[@Name='PIN Pad:']");
        private WindowsElement YesButton => AppiumDriver.FindElementByName("Yes");

        public POSPeripheralsConfigurationScreen SetCustomerDisplay(string customerDisplayToSelect)
        {
            WaitUntil(
                () => DoesElementExist(() => CustomerDisplayComboBox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the PinPad combobox exists took too long."
            );

            ComboBoxHelper.SetComboBox(CustomerDisplayComboBox, customerDisplayToSelect);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            if (DoesElementExist(() => YesButton))
            {
                //To confirm we want to save the changes with a restart when we leave the config.
                YesButton.Click();
            }

            return this;
        }

        public POSPeripheralsConfigurationScreen SetPinPad(string pinPadToSelect)
        {
            WaitUntil(
                () => DoesElementExist(() => PinPadComboBox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the PinPad combobox exists took too long."
            );

            ComboBoxHelper.SetComboBox(PinPadComboBox, pinPadToSelect);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            if (DoesElementExist(() => YesButton))
            {
                //To confirm we want to save the changes with a restart when we leave the config.
                YesButton.Click();
            }

            return this;
        }
    }
}
