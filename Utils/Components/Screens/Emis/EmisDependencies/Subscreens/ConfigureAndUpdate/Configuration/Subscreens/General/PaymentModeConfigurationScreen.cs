﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class PaymentModeConfigurationScreen : BaseComponent
    {
        private readonly EmisIconToolbar _emisIconToolbar;

        public PaymentModeConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _emisIconToolbar = toolbar;
        }

        private WindowsElement ExtraPaymentModeCheckBox => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Extra payment modes']");

        private WindowsElement LitreCouponsCheckBox => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Litre coupons']");

        private WindowsElement OfflinePaymentVouchersCheckBox => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Off-line payment vouchers']");

        public PaymentModeConfigurationScreen SetExtraPaymentModes(bool check)
        {
            AppiumWebElement GetExtraPaymentModesCheckbox() => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Extra payment modes']");

            WaitUntil(
                () => DoesElementExist(GetExtraPaymentModesCheckbox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Extra Payment modes checkbox to appear enabled took too long."
                );

            CheckboxHelper.SetCheckbox(ExtraPaymentModeCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }

        public PaymentModeConfigurationScreen SetLitreCoupons(bool check)
        {
            AppiumWebElement GetLitreCouponsCheckBox() => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Litre coupons']");

            WaitUntil(
                () => DoesElementExist(GetLitreCouponsCheckBox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Litre Coupons checkbox to appear enabled took too long."
                );

            CheckboxHelper.SetCheckbox(LitreCouponsCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }

        public PaymentModeConfigurationScreen SetPaymentVouchers(bool check)
        {
            AppiumWebElement GetPaymentVouchersCheckbox() => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Off-line payment vouchers']");

            WaitUntil(
                () => DoesElementExist(GetPaymentVouchersCheckbox),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Payment Vouchers checkbox to appear enabled took too long."
                );

            CheckboxHelper.SetCheckbox(OfflinePaymentVouchersCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }
    }
}
