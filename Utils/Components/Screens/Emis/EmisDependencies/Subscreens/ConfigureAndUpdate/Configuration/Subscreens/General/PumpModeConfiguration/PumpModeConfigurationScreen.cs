﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class PumpModeConfigurationScreen : BaseComponent
    {
        public PumpModeConfigurationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement SelectRowButton(int currentRow) => AppiumDriver.FindElementByXPath("//HeaderItem[@Name='Pump']/../..//DataItem[" + currentRow + "]");

        public void SetAutomaticReleaseForPumpRow(WindowsElement currentElement)
        {
            currentElement.Click();
        }

    }
}
