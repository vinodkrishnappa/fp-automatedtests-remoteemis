﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration;
using Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class PumpsConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly PumpModeConfigurationScreen _pumpModeConfigurationScreen;

        #endregion Subscreens

        public PumpsConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _pumpModeConfigurationScreen = new PumpModeConfigurationScreen(this);
            _emisIconToolbar = toolbar;
        }

        private WindowsElement PumpsConfigurationItem() => AppiumDriver.FindElementByName("Pumps");

        //private int NumberOfPumpRows => AppiumDriver.FindElementsByXPath("//HeaderItem[@Name='Pump']/../..//DataItem").Count;
        private IReadOnlyCollection<WindowsElement> PumpRows => AppiumDriver.FindElementsByXPath("//HeaderItem[@Name='Pump']/../..//DataItem");

        private WindowsElement GetPumpAutomaticReleaseCell(int rowNumber) => AppiumDriver.FindElementByXPath("//HeaderItem[@Name='Pump']/../..//DataItem[" + rowNumber + "]/ComboBox/Edit");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");

        public PumpsConfigurationScreen SetAutomaticReleaseForAllPumps()
        {
            AppiumWebElement GetCancelButton() => AppiumDriver.FindElementByName("Cancel");

            PumpsConfigurationItem().Click();
            int numberOfRows = PumpRows.Count;
            WindowsElement currentLocationInTable = null;

            for(int currentRow = 1; currentRow < numberOfRows+1; currentRow++)
            {
                currentLocationInTable = GetPumpAutomaticReleaseCell(currentRow);
                currentLocationInTable.Click();
                currentLocationInTable.SendKeys("A");

                //Right arrow to the Automatic release textbox in the Night Mode panel
                for(int j = 0; j < 7; j++)
                    currentLocationInTable.SendKeys(Keys.ArrowRight);

                currentLocationInTable.SendKeys("A");

                Logger.LogInfo("Configured Automatic Release for Pump: " + currentRow);
            }

            _emisIconToolbar.SaveButton.Click();
            OkButton.Click();

            ClickUntillElementNoLongerExists(GetCancelButton, 15);

            Logger.LogInfo("Configured automatic realease for all pumps");
            return this;
        }

    }
}
