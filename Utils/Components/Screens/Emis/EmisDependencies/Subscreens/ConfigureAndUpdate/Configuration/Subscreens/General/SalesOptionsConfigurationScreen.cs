﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Utils.ElementHelpers;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class SalesOptionsConfigurationScreen : BaseComponent
    {
        private readonly EmisIconToolbar _emisIconToolbar;

        public SalesOptionsConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _emisIconToolbar = toolbar;
        }

        private WindowsElement SelfCheckoutAllowedCheckBox => AppiumDriver.FindElementByXPath("//CheckBox[@Name='Self-checkout allowed']");

        public SalesOptionsConfigurationScreen SetSelfCheckoutAllowed(bool check)
        {
            CheckboxHelper.SetCheckbox(SelfCheckoutAllowedCheckBox, check);

            ClickUntillDisabledWithRetry(_emisIconToolbar.SaveButton, 15);

            return this;
        }
    }
}
