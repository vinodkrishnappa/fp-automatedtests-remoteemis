﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class StationInformationScreen : BaseComponent
    {
        public StationInformationScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement LogoComboxBox => AppiumDriver.FindElementByName("Logo:");

        private WindowsElement StationCity => AppiumDriver.FindElementByXPath("//Edit[@Name='City:']");
        private WindowsElement StationName => AppiumDriver.FindElementByXPath("//Edit[@Name='Station name:']");
        private WindowsElement StationPostalCode => AppiumDriver.FindElementByXPath("//Edit[@Name='Postal code:']");
        private WindowsElement StationStreet => AppiumDriver.FindElementByXPath("//Edit[@Name='Station address:']");
        private WindowsElement StationVATNumberInputField => AppiumDriver.FindElementByXPath("//Text[@Name='Station VAT number:']/following-sibling::*");

        public StationInformationScreen SetAddress(string street, string postalCode, string city)
        {
            AppiumWebElement GetStationStreet() => AppiumDriver.FindElementByXPath("//Edit[@Name='Station address:']");

            WaitUntil(
               () => DoesElementExist(GetStationStreet),
               TimeSpan.FromSeconds(1),
               TimeSpan.FromSeconds(10),
               "Waiting for the station information page to be displayed took too long."
            );

            StationStreet.SendKeys(street);
            StationPostalCode.SendKeys(postalCode);
            StationCity.SendKeys(city);
            return this;
        }

        public StationInformationScreen SetFirstLogo()
        {
            LogoComboxBox.Click();
            LogoComboxBox.SendKeys(Keys.Down);
            return this;
        }

        public StationInformationScreen SetStationName(string stationName)
        {
            AppiumWebElement GetStationStreet() => AppiumDriver.FindElementByXPath("//Edit[@Name='Station address:']");

            WaitUntil(
               () => DoesElementExist(GetStationStreet),
               TimeSpan.FromSeconds(1),
               TimeSpan.FromSeconds(10),
               "Waiting for the station information page to be displayed took too long."
            );

            StationName.SendKeys(stationName);
            return this;
        }

        public StationInformationScreen SetVATNumber(string vatNumber = "123456749")
        {
            StationVATNumberInputField.SendKeys(vatNumber);
            return this;
        }
    }
}
