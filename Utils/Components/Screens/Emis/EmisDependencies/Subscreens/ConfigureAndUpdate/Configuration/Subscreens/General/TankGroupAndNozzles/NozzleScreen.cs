﻿using System.Collections.Generic;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.TankGroupAndNozzles
{
    public class NozzleScreen : BaseComponent
    {
        public NozzleScreen(BaseComponent parent) : base(parent)
        {
        }

        private IReadOnlyCollection<WindowsElement> NozzleConfigurationComboBoxes => AppiumDriver.FindElementsByXPath("//Text[@Name='Fuel:']/../ComboBox");

        private WindowsElement NozzleFuelComboBox = null;

        //private WindowsElement NozzleTankGroupComboBox = null;

        public NozzleScreen SetFuelType(string fuelType)
        {
            SetNozzleConfigurationComboBoxes();
            ComboBoxHelper.SetComboBox(NozzleFuelComboBox, fuelType);
            return this;
        }

        public void SetNozzleConfigurationComboBoxes()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in NozzleConfigurationComboBoxes)
            {
                switch (counter)
                {
                    case 0:
                        //NozzleTankGroupComboBox = currentElement;
                        break;
                    case 1:
                        NozzleFuelComboBox = currentElement;
                        break;
                }
                counter++;
            }
        }
    }
}
