﻿using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;
using Utils.Utils.Enums;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.TankGroupAndNozzles
{
    public class TankGroupScreen : BaseComponent
    {
        public TankGroupScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement TankGroupFuelComboBox => AppiumDriver.FindElementByName("Each tank is equipped with a separate probe.");

        public TankGroupScreen SetFuelType(string fuelType)
        {
            ComboBoxHelper.SetComboBox(TankGroupFuelComboBox, fuelType);
            return this;
        }
    }
}
