﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.TankGroupAndNozzles;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class TankGroupsAndNozzlesScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly NozzleScreen _nozzleScreen;
        private readonly TankGroupScreen _tankGroupScreen;

        #endregion Subscreens

        public TankGroupsAndNozzlesScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _emisIconToolbar = toolbar;
            _nozzleScreen = new NozzleScreen(this);
            _tankGroupScreen = new TankGroupScreen(this);
        }

        private WindowsElement AddItemButton => AppiumDriver.FindElementByName("-->");
        private WindowsElement NozzelConfigurationItem => AppiumDriver.FindElementByName("Nozzle configuration");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement TankGroupConfigurationItem => AppiumDriver.FindElementByName("Tank group configuration");

        public NozzleScreen ConfigureNozzle(int pump, int nozzle)
        {
            AppiumWebElement GetPump() => NozzelConfigurationItem.FindElementByName($"Pump {pump}");

            WaitUntil(
                () => NozzelConfigurationItem.Displayed,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the EV Chargers tab to be visible took too long."
            );

            if (!DoesElementExist(GetPump))
            {
                throw new Exception($"Pump {pump} not found in nozzle config!!");
            }

            AppiumWebElement GetNozzle() => GetPump().FindElementByName($"Nozzle {nozzle}");
            bool nozzleExists = DoesElementExist(GetNozzle);
            if (!nozzleExists)
            {
                GetPump().Click();
                _emisIconToolbar.AddButton.Click();
                AddItemButton.Click();
                OkButton.Click();
            }
            GetNozzle().Click();
            return _nozzleScreen;
        }

        public TankGroupScreen ConfigureTankGroup(int tankGroup)
        {
            AppiumWebElement GetTankGroup() => TankGroupConfigurationItem.FindElementByName($"Tank group {tankGroup}");

            WaitUntil(
                () => DoesElementExist(() => TankGroupConfigurationItem),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for tank group configuration tab to be visible took too long."
            );

            bool tankGroupExists = DoesElementExist(GetTankGroup);
            if (!tankGroupExists)
            {
                TankGroupConfigurationItem.Click();
                _emisIconToolbar.AddButton.Click();
                AddItemButton.Click();
                OkButton.Click();
            }
            GetTankGroup().Click();

            return _tankGroupScreen;
        }
    }
}
