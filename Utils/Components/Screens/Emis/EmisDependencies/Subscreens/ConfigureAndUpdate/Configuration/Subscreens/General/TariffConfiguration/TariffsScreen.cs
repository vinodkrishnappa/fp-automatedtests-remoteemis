﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils.ElementHelpers;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration
{
    public class TariffsScreen : BaseComponent
    {
        private WindowsElement _textBoxApplyParking;
        private WindowsElement _textBoxEnergyUnitPrice;
        private WindowsElement _textBoxNonWMDiscount;
        private WindowsElement _textBoxParkingTimeFee;
        private WindowsElement _textBoxStartChargingFee;

        public TariffsScreen(BaseComponent parent) : base(parent)
        {
        }

        private WindowsElement ComboBoxPriceActivation => AppiumDriver.FindElementByXPath("//ComboBox[@Name='Price activation 1']");

        //There seems to be no other way to address all the textfields in this window,
        //some of these textboxes are nameless and have no direct parent
        //This collection contains all the textboxes on the configure tarrifs pop up
        //list item index: 0 = apply parking - 1 = parking fee - 2 = non-wm discount
        //3 = start charging fee - 4 = energy unit price - 10 = name
        private IReadOnlyCollection<WindowsElement> TariffConfigurationTextFieldList => AppiumDriver.FindElementsByXPath("//Group[@Name='New price set']//Edit");

        private WindowsElement TextBoxName => AppiumDriver.FindElementByXPath("//Text[@Name='Name:']/following-sibling::Edit");

        public TariffsScreen ConfigureTariff(int tariffNumber)
        {
            WaitUntil(
                () => TextBoxName.Displayed,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the Tariff configuration tab to be visible took too long."
            );

            //Special way of getting the elements in the pop-up
            SetTariffProgrammingElements();

            TextBoxName.SendKeys("Tariff " + tariffNumber);

            ComboBoxHelper.SetComboBox(ComboBoxPriceActivation, "Immediate");

            //Using the tariffNumber as a base for the values we enter so that tariff 1 is different from tariff 2 etc.
            //Prefered over random number gen as it is more traceable
            _textBoxEnergyUnitPrice.SendKeys(tariffNumber.ToString());
            _textBoxNonWMDiscount.SendKeys((tariffNumber * 10).ToString());
            _textBoxStartChargingFee.SendKeys((tariffNumber * 3).ToString());
            _textBoxApplyParking.SendKeys((tariffNumber * 20).ToString());
            _textBoxParkingTimeFee.SendKeys((tariffNumber * 2).ToString());
            return this;
        }

        //There seems to be no other way to reach the textfields in this window,
        //some of these textboxes are nameless and have no direct parent
        //This collection contains all the textboxes on the configure tarrifs pop up
        //list item index: 0 = apply parking - 1 = parking fee - 2 = non-wm discount
        //3 = start charging fee - 4 = energy unit price
        public void SetTariffProgrammingElements()
        {
            int counter = 0;
            foreach (WindowsElement currentElement in TariffConfigurationTextFieldList)
            {
                switch (counter)
                {
                    case 0:
                        _textBoxApplyParking = currentElement;
                        break;

                    case 1:
                        _textBoxParkingTimeFee = currentElement;
                        break;

                    case 2:
                        _textBoxNonWMDiscount = currentElement;
                        break;

                    case 3:
                        _textBoxStartChargingFee = currentElement;
                        break;

                    case 4:
                        _textBoxEnergyUnitPrice = currentElement;
                        break;
                }
                counter++;
            }
        }
    }
}
