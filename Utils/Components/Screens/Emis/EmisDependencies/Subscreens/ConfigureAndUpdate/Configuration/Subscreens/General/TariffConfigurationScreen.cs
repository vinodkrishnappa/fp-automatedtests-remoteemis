﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class TariffConfigurationScreen : BaseComponent
    {
        #region Menus

        private readonly EmisIconToolbar _emisIconToolbar;

        #endregion Menus

        #region Subscreens

        private readonly TariffsScreen _tariffsScreen;

        #endregion Subscreens

        public TariffConfigurationScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
            _tariffsScreen = new TariffsScreen(this);
            _emisIconToolbar = toolbar;
        }

        private WindowsElement AddButton => AppiumDriver.FindElementByName("Add");
        private WindowsElement CancelButton => AppiumDriver.FindElementByName("Cancel");
        private WindowsElement TariffsConfigurationItem => AppiumDriver.FindElementByName("Tariffs");

        public TariffConfigurationScreen AddTariff(int tariffNumber)
        {
            AppiumWebElement GetTariff() => TariffsConfigurationItem.FindElementByName($"{tariffNumber} - Tariff {tariffNumber}");

            WaitUntil(
                () => DoesElementExist(() => TariffsConfigurationItem),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Waiting for the tarrif configuration tab to be visible took too long."
            );

            bool tariffExists = DoesElementExist(GetTariff);
            if (!tariffExists)
            {
                TariffsConfigurationItem.Click();
                _emisIconToolbar.AddButton.Click();
                _tariffsScreen.ConfigureTariff(tariffNumber);
                AddButton.Click();
                CancelButton.Click();
            }
            else
            {
                GetTariff().Click();
                //If the tariff exists we still do the config to be sure that it is set correctly.
                _tariffsScreen.ConfigureTariff(tariffNumber);
            }
            Logger.LogInfo("Added and configured Tariff: " + tariffNumber);

            return this;
        }
    }
}
