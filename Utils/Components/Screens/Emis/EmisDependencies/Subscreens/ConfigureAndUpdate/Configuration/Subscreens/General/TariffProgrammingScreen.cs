﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis;
using Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens.General.DispensingConfiguration;
using Utils.Utils;

namespace Utils.Components.Screens.Emis.ConfigureAndUpdate.Configuration.Subscreens
{
    public class TariffProgrammingScreen : BaseComponent
    {
        #region Menus

        #endregion Menus

        #region Subscreens
        #endregion Subscreens

        public TariffProgrammingScreen(BaseComponent parent, EmisIconToolbar toolbar) : base(parent)
        {
        }

        public TariffProgrammingScreen ConfigureChargePoint()
        {
            TestContext.Progress.Write("To be completed later");
            return this;
        }
        
    }
}
