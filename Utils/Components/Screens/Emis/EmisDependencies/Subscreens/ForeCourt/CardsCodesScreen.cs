﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.ForeCourt
{
    public class CardsCodesScreen : BaseComponent
    {
        private readonly EmisMenu _emisMenu;

        public CardsCodesScreen(BaseScreen parent, EmisMenu emisMenu) : base(parent) => _emisMenu = emisMenu;

        public IEnumerable<WindowsElement> CardCodesList() => AppiumDriver.FindElementsByXPath("//DataItem[@Name='Card codes']/*");

        public WindowsElement CardCodesListItem() => AppiumDriver.FindElementByXPath("//DataItem[@Name='Card codes']");

        public string GetCardCodeTextByRow(int rowNumber) => AppiumDriver
                .FindElementByXPath("//DataItem[@Name='Card codes']/DataItem[" + rowNumber + "]")
                .GetAttribute("Value.Value");

        //Get the number of actual card codes, it checks if the name does not contain 'CLEANUP' as these are the empty
        //Card codes that get put in place in the Cleanup, because Card codes can not be deleted (As far as I know)

        public int GetNumberOfCardCodesByName()
        {
            int NumberOfCardCodes = 0;

            foreach (WindowsElement currentElement in CardCodesList())
            {
                if (!currentElement.GetAttribute("Name").Contains("CLEANUP"))
                    NumberOfCardCodes++;
            }

            return NumberOfCardCodes;
        }

        //Counts the actual number of Card Codes in eMis and compares the number to the expected count.

        public bool ValidateNumberOfCardCodes(int expectedNumberOfCardCodes)
        {
            int actualNumberOfCardCodes = 1;

            _emisMenu.ForeCourtMenu.FuelAndTankManagementSubMenu.CardCodesMenuItem.ClickThroughMenuTree();

            WaitUntil(
                () => CardCodesListItem().Displayed,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Wait for file to exist in remote directory FAILED, file was not found before the maxWaitTime"
            );

            //This listitem requires a double click to open, this is not supported by default by AppiumDriver
            //The Right Arrow accomplishes the same thing and opens the menu.

            CardCodesListItem().SendKeys(Keys.Right);
            actualNumberOfCardCodes = GetNumberOfCardCodesByName();

            if (actualNumberOfCardCodes == expectedNumberOfCardCodes)
            {
                Logger.LogInfo($"Expected number of Card Codes MATCHES actual number \nExpected: {expectedNumberOfCardCodes} \nActual: {actualNumberOfCardCodes}");
                return true;
            }
            else
            {
                Logger.LogInfo($"Expected number of Card Codes DOES NOT match actual number \nExpected: {expectedNumberOfCardCodes} \nActual: {actualNumberOfCardCodes}");
                return false;
            }
        }
    }
}
