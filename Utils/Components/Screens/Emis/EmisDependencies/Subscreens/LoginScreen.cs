﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Utils;
using Utils.Utils.ConfigurationManager;
using Utils.Utils.SettingsInterfaces;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis
{
    internal class LoginScreen : BaseComponent
    {
        #region configs

        private static ISecrets SecretsConfig => ConfigurationManager.Config.Secrets;

        #endregion configs

        private readonly bool _isRemoteEmisScreen;

        public LoginScreen(BaseScreen parent, bool IsRemoteEmisScreen = false) : base(parent) => _isRemoteEmisScreen = IsRemoteEmisScreen;

        private WindowsElement AccessCodeTextField => AppiumDriver.FindElementByXPath($"/*[1]/*[1]/*[1]/*[2]");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement UserTextField => AppiumDriver.FindElementByXPath($"/*[1]/*[1]/*[1]/*[3]");

        private WindowsElement StationTextField => AppiumDriver.FindElementByXPath("//Edit[@AutomationId='1001']");
        private WindowsElement User => AppiumDriver.FindElementByXPath("//Edit[@AutomationId='m_TbName']");
        private WindowsElement AccessCode => AppiumDriver.FindElementByXPath("//Edit[@AutomationId='m_TbPassword']");

        private void EmisLogin()
        {
            WaitUntilElementExists(() => UserTextField, nameof(UserTextField), TimeSpan.FromSeconds(5));
            WaitUntilElementEnabled(() => UserTextField, nameof(UserTextField), TimeSpan.FromSeconds(5));
            // Sometimes selenium types too fast, therefore this retry function
            Retry(
                () =>
                {
                    UserTextField.Clear();
                    UserTextField.SendKeys(SecretsConfig.EmisUsername);
                    if (!UserTextField.Text.Equals(SecretsConfig.EmisUsername))
                        throw new InvalidOperationException("Login username not correct");
                },
                TimeSpan.FromSeconds(1),
                3
            );
            AccessCodeTextField.SendKeys(SecretsConfig.EmisPassword);
            OkButton.Click();

            // Second small warning window (probably does not exist on every system)
            Retry(
                () =>
                {
                    // TODO: image recognition fails with following if => if (ImageHandler.ImageIsFoundOnScreen(WindowsIcons.Info, this))
                    if (DoesElementExist(() => OkButton))
                        OkButton.Click();
                    else
                        throw new NoSuchElementException("Image not found");
                },
                TimeSpan.FromSeconds(2),
                8, // This might seem like a lot of retries, but it is needed for slow machines, max 16 seconds
                () => Logger.LogInfo("No second info dialog found after login")
            );

        }


        private void RemoteEmisLogin()
        {
            var temp2 = ParentWindow.ReturnWindowAsWindowsElement; 
            //(//input[@id="search_query"])[2]
            var temp = AppiumDriver.FindElementsByXPath("//");

            var temp1 = AppiumDriver.FindElementByXPath("//Edit[@AutomationId='1001']");

            WaitUntilElementExists(() => StationTextField, nameof(StationTextField), TimeSpan.FromSeconds(5));
            WaitUntilElementExists(() => StationTextField, nameof(StationTextField), TimeSpan.FromSeconds(5));
            Retry(
                () =>
                {
                    StationTextField.Clear();
                    StationTextField.SendKeys(SecretsConfig.StationNumber);
                    UserTextField.SendKeys(SecretsConfig.EmisUsername);
                    if (!UserTextField.Text.Equals(SecretsConfig.EmisUsername))
                        throw new InvalidOperationException("Login username not correct");
                },
                TimeSpan.FromSeconds(1),
                3
                );

            AccessCodeTextField.SendKeys(SecretsConfig.EmisPassword);
            OkButton.Click();
        }


        public void Login()
        {
            if (_isRemoteEmisScreen)
            {
                RemoteEmisLogin();
            }
            else
            {
                EmisLogin();
            }
        }


    }
}
