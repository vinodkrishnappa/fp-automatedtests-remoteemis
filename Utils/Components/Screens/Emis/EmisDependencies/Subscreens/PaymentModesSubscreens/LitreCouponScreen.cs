﻿using System;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.MenuBars.Emis.Menus;
using Utils.Utils;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis.PaymentModesSubscreens
{
    public class LitreCouponScreen : BaseComponent
    {
        private readonly EmisMenu _emisMenu;

        public LitreCouponScreen(BaseScreen parent, EmisMenu emisMenu) : base(parent) => _emisMenu = emisMenu;

        /// <summary>
        /// Gets the Name of the Litre Coupon on the specified row and checks if it is not empty
        /// </summary>
        /// <param name="rowNumber"></param>
        public bool CheckIfLitreCodeNameIsFilled(int rowNumber) => string.IsNullOrEmpty(GetLitreCodeNameByRow(rowNumber));

        public string GetLitreCodeNameByRow(int rowNumber) => AppiumDriver
                .FindElementByXPath($"//Tree/DataItem[{rowNumber}]/Edit[@Name='Name']")
                .GetAttribute("Value.Value");

        public WindowsElement NameColumnHeader() => AppiumDriver.FindElementByXPath("//HeaderItem[@Name='Name']");

        //[" + rowNumber + "]
        public bool ValidateNumberOfLitreCoupons(int expectedNumberOfLitreCoupons)
        {
            int RowCounter = 1;
            int FilledRowCounter = 0;

            _emisMenu.PaymentModesMenu.LitreCoupons.ClickThroughMenuTree();
            WaitUntil(
                () => NameColumnHeader().Displayed,
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(10),
                "Wait for file to exist in remote directory FAILED, file was not found before the maxWaitTime"
            );

            //There are 10 Litre Coupon rows, we check the value of each to see if it is empty or filled
            while (RowCounter <= 10)
            {
                if (CheckIfLitreCodeNameIsFilled(RowCounter))
                    FilledRowCounter++;
                RowCounter++;
            }

            string messagePiece = FilledRowCounter == expectedNumberOfLitreCoupons ? "MATCHES" : "DOES NOT match";
            Logger.LogInfo(
                string.Format(
                    "Expected number of Litre Coupons {0} actual number, Expected: {1} | Actual: {2}",
                    messagePiece,
                    expectedNumberOfLitreCoupons,
                    FilledRowCounter
                )
            );
            return FilledRowCounter == expectedNumberOfLitreCoupons;
        }
    }
}
