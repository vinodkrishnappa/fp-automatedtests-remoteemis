﻿using System;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.Screens.FuelPosSetup;
using Utils.Components.Windows.Temporary;
using static Utils.Utils.Utils;

namespace Utils.Components.Screens.Emis
{
    internal class ShutdownRestartScreen : BaseComponent
    {
        public ShutdownRestartScreen(BaseScreen parent) : base(parent)
        {
        }

        private WindowsElement FuelPosSetupRadioButton => AppiumDriver.FindElementByXPath("//RadioButton[@Name='Fuel POS setup']");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");
        private WindowsElement RebootRadioButton => AppiumDriver.FindElementByXPath("//RadioButton[@Name='Reboot']");

        public FuelPosSetupScreen OpenFuelPosSetup()
        {
            FuelPosSetupRadioButton.Click();
            OkButton.Click();

            // Kill setup, because it does not have the keyboard argument
            TemporaryWindows.FuelPosSetup.WaitUntilIsFocused(TimeSpan.FromMinutes(2));
            KillProcess("fuelpossetup");

            // Launch setup, with good arguments
            RunPowerShellCommand(@"C:\fuelpossetup.exe -k", TimeSpan.FromSeconds(20));
            return new FuelPosSetupScreen();
        }

        public ShutdownRestartScreen Reboot()
        {
            RebootRadioButton.Click();
            OkButton.Click();
            return this;
        }

        public ShutdownRestartScreen WaitUntilLoaded()
        {
            WaitUntilElementEnabled(
                () => RebootRadioButton,
                nameof(RebootRadioButton),
                TimeSpan.FromSeconds(10)
            );
            return this;
        }
    }
}
