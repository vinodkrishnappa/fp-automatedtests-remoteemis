﻿using Utils.Components.Screens.Emis.EmisDependencies;
using Utils.Components.Windows.Main;

namespace Utils.Components.Screens.Emis
{
    public sealed class EmisMainScreen : BaseEmisScreen
    {
        /// <summary>
        /// Implement all emis related functionality in EmisDependencies/BaseEmisScreen. Do not implement it here!
        /// </summary>
        public EmisMainScreen() : base(MainWindows.Emis)
        {
        }
    }
}
