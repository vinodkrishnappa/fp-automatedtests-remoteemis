﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources.Components.Screens;
using Utils.Components.Screens.Emis.EmisDependencies;
using Utils.Components.Windows.Temporary;

namespace Utils.Components.Screens.RemoteEmis
{
    public class RemoteEmisScreen: BaseEmisScreen
    {
        public RemoteEmisScreen() : base(TemporaryWindows.RemoteEmis)
        {
        }

    }
}
