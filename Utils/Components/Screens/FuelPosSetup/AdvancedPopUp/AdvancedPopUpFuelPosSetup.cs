﻿using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.Screens.FuelPosSetup.AdvancedPopUp.Tabs;

namespace Utils.Components.Screens.FuelPosSetup.AdvancedPopUp

{
    public class AdvancedPopUpFuelPosSetup : BaseComponent
    {
        public AdvancedPopUpFuelPosSetup(BaseScreen baseScreen) : base(baseScreen)
        {
        }

        private WindowsElement CloseWindow => AppiumDriver.FindElementByXPath("//Button[@Name='Cancel']");
        private WindowsElement ImportStationDataTab => AppiumDriver.FindElementByXPath("//TabItem[@Name='Import station data']");
        private WindowsElement StagingTab => AppiumDriver.FindElementByXPath("//TabItem[@Name='Staging']");

        public void Close() => CloseWindow.Click();

        public ImportStationDataTabAdvancedPopUpFuelPosSetup OpenImportStationDataTab()
        {
            ImportStationDataTab.Click();
            return new ImportStationDataTabAdvancedPopUpFuelPosSetup(this);
        }

        public StagingTabAdvancedPopUpFuelPosSetup OpenStagingTab()
        {
            StagingTab.Click();
            return new StagingTabAdvancedPopUpFuelPosSetup(this);
        }
    }
}
