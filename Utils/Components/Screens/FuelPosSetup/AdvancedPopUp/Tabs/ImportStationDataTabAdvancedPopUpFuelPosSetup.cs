﻿using Utils.Components.Components;

namespace Utils.Components.Screens.FuelPosSetup.AdvancedPopUp.Tabs

{
    public class ImportStationDataTabAdvancedPopUpFuelPosSetup : BaseComponent
    {
        public ImportStationDataTabAdvancedPopUpFuelPosSetup(BaseComponent baseComponent) : base(baseComponent)
        {
        }
    }
}
