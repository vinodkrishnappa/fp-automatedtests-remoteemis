﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Components.Components;
using Utils.Utils;
using Utils.Utils.Drivers;

namespace Utils.Components.Screens.FuelPosSetup.AdvancedPopUp.Tabs

{
    public class StagingTabAdvancedPopUpFuelPosSetup : BaseComponent
    {
        private readonly BaseComponent _parent;

        public StagingTabAdvancedPopUpFuelPosSetup(BaseComponent baseComponent) : base(baseComponent) => _parent = baseComponent;

        private WindowsElement CleanButton => AppiumDriver.FindElementByXPath("//Button[@Name='Clean']");

        public StagingTabAdvancedPopUpFuelPosSetup Clean()
        {
            AppiumDriver.FindElementByName("Advanced").SendKeys(Keys.LeftAlt + Keys.LeftShift + Keys.LeftControl + "t");
            CleanButton.Click();
            // TODO: Replace wait with DriverUtils.WaitUntilWindowDoesNotExist(); the windowtitle is pretty hard to detect, but it might be the same which makes this impossible to do with implict wait for now
            Thread.Sleep(TimeSpan.FromSeconds(30));
            return this;
        }

        public BaseComponent Close() => _parent;
    }
}
