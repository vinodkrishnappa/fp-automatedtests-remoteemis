﻿using System;
using System.IO;
using System.Linq;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Screens.FuelPosSetup.AdvancedPopUp;
using Utils.Components.Windows.Temporary;
using Utils.Utils;
using Utils.Utils.Drivers;

namespace Utils.Components.Screens.FuelPosSetup
{
    public class FuelPosSetupScreen : BaseScreen
    {
        public FuelPosSetupScreen() : base(TemporaryWindows.FuelPosSetup)
        {
        }

        private WindowsElement AdvancedButton => AppiumDriver.FindElementByXPath("//Button[@Name='Advanced']");

        private WindowsElement CloseWindow => AppiumDriver.FindElementByXPath("//Button[@Name='Cancel']");

        private WindowsElement InstallationSourceTextBox => AppiumDriver.FindElementByXPath("//Edit[last()]");

        private WindowsElement InstallButton => AppiumDriver.FindElementByXPath("//Button[@Name='Install']");

        public void Close()
        {
            CloseWindow.Click();
            Dispose();
        }

        public void Install(string fuelPosBuildPath, bool clean = true)
        {
            if (clean)
                OpenAdvancedPopUp().OpenStagingTab().Clean();

            string fuelPosPackage = Path.GetFileName(
                Directory
                    .GetFiles($"{Path.Combine(fuelPosBuildPath, @"installable\packages")}", "FuelPos_*")
                    .First()
            );

            string fuelPosPackagePath = Path.Combine(
                DriverConnection
                    .GetVmInUse()
                    .AddTransientSharedFolder(fuelPosBuildPath),
                $@"installable\packages\{fuelPosPackage}"
            );

            InstallationSourceTextBox.SendKeys(fuelPosPackagePath);
            InstallButton.Click();

            Logger.LogInfo("Waiting until fuelPos setup is finised");
            TemporaryWindows.FuelPosSetup.WaitUntilDoesNotExists(TimeSpan.FromMinutes(5));
            DriverConnection.Reconnect(true);
            Dispose();
        }

        public AdvancedPopUpFuelPosSetup OpenAdvancedPopUp()
        {
            AdvancedButton.Click();
            return new AdvancedPopUpFuelPosSetup(this);
        }
    }
}
