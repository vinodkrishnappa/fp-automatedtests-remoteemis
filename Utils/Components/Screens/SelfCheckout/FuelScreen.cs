﻿using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.Components.SelfCheckout;
using Utils.Components.MenuBars.SelfCheckout;

namespace Utils.Components.Screens.SelfCheckout
{
    public class FuelScreen : BaseComponent
    {
        private readonly SCOMenuBarComponent _menuBarComponent;

        public FuelScreen(BaseScreen parent, SCOMenuBarComponent menuBarComponent, ArticleBasketComponent articleBasketComponent) : base(parent)
        {
            ArticleBasket = articleBasketComponent;
            FuelSelector = new FuelSelectorComponent<FuelScreen>(this, ArticleBasket);
            _menuBarComponent = menuBarComponent;
        }

        public ArticleBasketComponent ArticleBasket { get; }

        public FuelSelectorComponent<FuelScreen> FuelSelector { get; }

        private WindowsElement ArticlesWithoutBarCodeButton => AppiumDriver.FindElementByName("butNoBarcode");

        public FuelScreen SelectArticlesWithoutBarCode()
        {
            ArticlesWithoutBarCodeButton.Click();
            return this;
        }
    }
}
