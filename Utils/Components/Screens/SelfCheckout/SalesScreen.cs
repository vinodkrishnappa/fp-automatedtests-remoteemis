﻿using Resources.Components.Screens;
using Utils.Components.Components;
using Utils.Components.Components.SelfCheckout;
using Utils.Components.MenuBars.SelfCheckout;

namespace Utils.Components.Screens.SelfCheckout
{
    public class SalesScreen : BaseComponent
    {
        private readonly FuelSelectorComponent<SalesScreen> _fuelSelector;
        private readonly SCOMenuBarComponent _menuBarComponent;

        public SalesScreen(BaseScreen parent, SCOMenuBarComponent menuBarComponent, ArticleBasketComponent articleBasketComponent) : base(parent)
        {
            ArticleBasket = articleBasketComponent;
            _fuelSelector = new FuelSelectorComponent<SalesScreen>(this, ArticleBasket);
            _menuBarComponent = menuBarComponent;
        }

        public ArticleBasketComponent ArticleBasket { get; }

        // public WindowsElement ArticlesButton => AppiumDriver.FindElementByName("butNoBarcode");
        // public WindowsElement FuelButton => AppiumDriver.FindElementByName("butFuel");
        // public WindowsElement RetryTransactionButton => AppiumDriver.FindElementByName("butRetry");
    }
}
