﻿using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Resources.Constants.Images;
using Utils.Components.Components.SelfCheckout;
using Utils.Components.MenuBars.SelfCheckout;
using Utils.Components.Windows.Main;
using Utils.Utils;

namespace Utils.Components.Screens.SelfCheckout
{
    public class SelfCheckoutMainScreen : BaseScreen
    {
        #region Subscreens

        private readonly FuelScreen _fuelScreen;
        private readonly SalesScreen _salesScreen;

        #endregion Subscreens

        #region Elements

        public WindowsElement ArticlesButton => AppiumDriver.FindElementByName("butNoBarCode");
        public WindowsElement FuelButton => AppiumDriver.FindElementByName("butFuel");
        public WindowsElement ScanButton => AppiumDriver.FindElementByName("butScanner");
        public WindowsElement WelcomeLabel => AppiumDriver.FindElementByName("lblWelcome");
        private WindowsElement OneOfOurStaffText => AppiumDriver.FindElementByXPath("//Pane[@Name='lblStartByScan']/Text");
        private WindowsElement TillClosed => AppiumDriver.FindElementByXPath("//Pane[@Name='lblTillClosedHelp']/Text");

        #endregion Elements

        public SelfCheckoutMainScreen() : base(MainWindows.SelfCheckout)
        {
            MenuBar = new SCOMenuBarComponent(this);
            ArticleBasketComponent articleBasketComponent = new ArticleBasketComponent(this);

            _salesScreen = new SalesScreen(this, MenuBar, articleBasketComponent);
            _fuelScreen = new FuelScreen(this, MenuBar, articleBasketComponent);
        }

        public FuelScreen FuelScreen
        {
            get
            {
                VerifyIfElementIsVisibleAndEnabled(FuelButton);
                FuelButton.Click();
                return _fuelScreen;
            }
        }

        public SCOMenuBarComponent MenuBar { get; }

        public SalesScreen SalesScreen
        {
            get
            {
                VerifyIfElementIsVisibleAndEnabled(ArticlesButton);
                ArticlesButton.Click();
                return _salesScreen;
            }
        }

        public bool IsTillClosed()
        {
            if (ImageHandler.ImageIsFoundOnScreen(SelfCheckoutImages.RedCone, this))
            {
                VerifyIfElementContainsSpecifiedText(TillClosed, Constants.TillClosed);
                VerifyIfElementContainsSpecifiedText(OneOfOurStaffText, Constants.OneofOurStaffMsg);
                return true;
            }
            return false;
        }

        public void OpenEfui() => MenuBar.OpenEFUI();

        public bool VerifyInitializationOptions()
        {
            VerifyIfElementIsVisible(WelcomeLabel);
            MenuBar.VerifyInitializationOptions();
            VerifyIfElementIsVisibleAndEnabled(ArticlesButton);
            VerifyIfElementIsVisibleAndEnabled(FuelButton);
            VerifyIfElementIsVisibleAndEnabled(ScanButton);
            FuelScreen.ArticleBasket.VerifyInitializationOptions();
            _salesScreen.ArticleBasket.VerifyInitializationOptions();
            return true;
        }
    }
}
