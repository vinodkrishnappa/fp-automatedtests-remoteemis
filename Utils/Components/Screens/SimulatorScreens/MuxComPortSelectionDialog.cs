﻿using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Windows.Temporary;

namespace Utils.SimulatorScreens
{
    public class MuxComPortSelectionDialog : BaseScreen
    {
        public MuxComPortSelectionDialog() : base(TemporaryWindows.ComPortSelection)
        {
        }

        private WindowsElement ComPortTextBox => AppiumDriver.FindElementByXPath("//Text[@Name='COM Port']/following-sibling::*");
        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");

        public void SelectComPort(int comPort)
        {
            ComPortTextBox.Clear();
            ComPortTextBox.SendKeys(comPort.ToString());

            OkButton.Click();
            Dispose();
        }
    }
}
