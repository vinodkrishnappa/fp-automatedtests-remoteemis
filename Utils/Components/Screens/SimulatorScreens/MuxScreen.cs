﻿using System.Threading;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Windows.Simulator;
using Utils.SimulatorScreens;

namespace Utils.Screens
{
    public class MuxScreen : BaseScreen
    {
        public MuxScreen() : base(SimulatorWindows.Mux)
        {
        }

        private WindowsElement OkButton => AppiumDriver.FindElementByName("OK");

        public void Configure(int comPort)
        {
            OkButton.Click();
            new MuxComPortSelectionDialog().SelectComPort(comPort);
        }

        public void GetFuel(int pumpNumber)
        {
            GetPump(pumpNumber).Click();
            Thread.Sleep(1000);
            GetPump(pumpNumber).Click();
        }

        public WindowsElement GetPump(int pumpNumber) => AppiumDriver.FindElementByXPath($"/*[1]/*[1]/*[{pumpNumber}]");
    }
}
