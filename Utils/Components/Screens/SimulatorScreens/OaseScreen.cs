﻿using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Windows.Simulator;

namespace Utils.Screens
{
    public class OaseScreen : BaseScreen
    {
        public OaseScreen() : base(SimulatorWindows.Oase)
        {
        }

        private WindowsElement StartButton => AppiumDriver.FindElementByName("START");
        private WindowsElement StopButton => AppiumDriver.FindElementByName("STOP");

        public void Configure() => StartOase();

        public void StartOase()
        {
            if (StartButton.Enabled)
                StartButton.Click();
        }

        public void StopOase()
        {
            if (StopButton.Enabled)
                StopButton.Click();
        }
    }
}
