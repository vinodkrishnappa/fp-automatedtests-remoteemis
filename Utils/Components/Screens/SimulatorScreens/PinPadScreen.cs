﻿using System;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Utils.Components.Windows.Simulator;
using Utils.Utils.Enums;
using static Utils.Utils.Utils;

namespace Utils.Screens
{
    public class PinPadScreen : BaseScreen
    {
        public PinPadScreen() : base(SimulatorWindows.PinPad)
        {
        }

        private WindowsElement ActivateCardButton => AppiumDriver.FindElementByName("Activate selected");
        private WindowsElement CardSimulationModePane => AppiumDriver.FindElementByXPath("//*[@Name='Card simulation mode']");
        private WindowsElement ComPortComboBox => AppiumDriver.FindElementByXPath("//*[@Name='Port']/ComboBox");
        private WindowsElement StartButton => AppiumDriver.FindElementByName("START");
        private WindowsElement StopButton => AppiumDriver.FindElementByName("STOP");

        public void Configure(int comPort, PinPadCards pinPadCardToActivate, PinPadCardSimulationModes cardSimulationMode)
        {
            SelectComPort(comPort);
            SetCardSimulationMode(cardSimulationMode);

            ActivateCard(pinPadCardToActivate);
            StartButton.Click();
        }

        public void RunBeforeSimulatorLaunch()
        {
            // For some reason starting pinPad from the commandline uses the fpApplic user, instead of the Fuel-Pos user
            // Remove "%localappdata%/Tokheim_Dover_Fuel_So" to avoid errors (clear cache)
            const string removeLocalAppData_Command = @"Remove-Item -Recurse -Force 'C:\Users\fpApplic\AppData\Local\Tokheim,_Dover_Fueling_So' -ErrorAction Ignore";
            // Copy the cardprofiles which aren't present yet (as admin)
            const string copyCardProfiles_IfTheyAreNotPresent_Command = @"robocopy 'C:\Users\Fuel-Pos\Documents\CardProfiles' 'C:\Users\fpApplic\Documents\CardProfiles'";
            // The fpApplic user isn't accessible without admin privileges
            const string doAsAdminCommand = "Start-Process PowerShell -Verb RunAs";

            // Full command
            string powerShellCommand = $"{doAsAdminCommand} \"{removeLocalAppData_Command}; {copyCardProfiles_IfTheyAreNotPresent_Command}\"";
            RunPowerShellCommand(powerShellCommand, TimeSpan.FromSeconds(3));
        }

        public void Stop() => StopButton.Click();

        private void ActivateCard(PinPadCards pinPadCard)
        {
            if (pinPadCard != PinPadCards.s_none)
            {
                AppiumDriver.FindElementByName(pinPadCard.Value).Click();
                ActivateCardButton.Click();
            }
        }

        private void SelectComPort(int comPort)
        {
            string portToUse = $"COM{comPort}";
            if (ComPortComboBox.GetAttribute("Value.Value") != portToUse)
            {
                ComPortComboBox.Click();
                ComPortComboBox.FindElementByName(portToUse).Click();
            }
        }

        private void SetCardSimulationMode(PinPadCardSimulationModes cardSimulationMode) => CardSimulationModePane.FindElementByName(cardSimulationMode.Value).Click();
    }
}
