﻿using Utils.Components.Screens.Efui;
using Utils.Utils;
using Utils.Utils.Drivers;

namespace Utils.Components.Windows.Main
{
    public class EfuiWindow : Window
    {
        public EfuiWindow(string windowTitle = "", string executablePath = "") : base(windowTitle, executablePath) => AddRunAfterCreatingDriverAction(() => WaitOnEfuiReady());

        /// <summary>
        /// Efui needs to wait until ready, wait on intial data & wait until prepay transactions is finished
        /// </summary>
        private void WaitOnEfuiReady()
        {
            // Set the resolution if it isn't set (due to missing appium features it is placed here,
            // see comments in DriverConnection.IntializeDrivers & ImageHandler.SetResolution)
            if (!ImageHandler.ResolutionIsSet)
                ImageHandler.SetResolution(this);

            DriverConnection.GetScreen<EfuiMainScreen>().WaitUntilEfuiIsReady();
        }
    }
}
