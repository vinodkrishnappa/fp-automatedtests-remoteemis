﻿using Utils.Components.Screens.Efui;
using Utils.Utils;
using Utils.Utils.Drivers;

namespace Utils.Components.Windows.Main
{
    public class EmisWindow : Window
    {
        /// <summary>
        /// Emis has to be opened before we can bind it to the driver session
        /// This requires the Efui driver, since we open Emis through Efui
        /// </summary>
        /// <param name="windowTitle"></param>
        /// <param name="executablePath"></param>
        public EmisWindow(string windowTitle = "", string executablePath = "") : base(windowTitle, executablePath) => AddRunBeforeCreatingDriverAction(() => OpenEmis());

        private void OpenEmis()
        {
            if (DriverConnection.GetDriver(MainWindows.Efui) == null)
                Logger.LogWarning($"'{MainWindows.Efui._title}' is a requirement for '{_title}' but, wasn't created");
            else
                DriverConnection.GetScreen<EfuiMainScreen>().OpenEmis();
        }
    }
}
