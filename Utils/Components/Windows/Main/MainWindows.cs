﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Utils.Components.Windows.Main
{
    public sealed class MainWindows
    {
        private static readonly Dictionary<string, Window> s_windows = new Dictionary<string, Window>();

        public static Window Efui => GetWindow(nameof(Efui), () => new EfuiWindow("eFUI"));
        public static Window Emis => GetWindow(nameof(Emis), () => new EmisWindow("eMIS"));
        public static Window SelfCheckout => GetWindow(nameof(SelfCheckout), () => new SelfCheckoutWindow("Self Checkout"));

        public static bool IsMainWindow(Window window) => typeof(MainWindows)
            .GetProperties(BindingFlags.Public | BindingFlags.Static)
            .Select(property => property.GetValue(null))
            .Any(property => (property as Window)?.Equals(window) ?? false);

        private static Window GetWindow(string uniqueIdentifier, Func<Window> makeNewWindow)
        {
            if (s_windows.ContainsKey(uniqueIdentifier))
                return s_windows[uniqueIdentifier];

            Window window = makeNewWindow();
            s_windows.Add(uniqueIdentifier, window);
            return window;
        }
    }
}
