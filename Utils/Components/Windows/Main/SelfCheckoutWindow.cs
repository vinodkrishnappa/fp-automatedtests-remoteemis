﻿using Utils.Components.Screens.SelfCheckout;
using Utils.Utils.Drivers;

namespace Utils.Components.Windows.Main
{
    public class SelfCheckoutWindow : Window
    {
        /// <summary>
        /// SelfCheckout requires to login otherwise it will block everything
        /// </summary>
        /// <param name="windowTitle"></param>
        /// <param name="executablePath"></param>
        public SelfCheckoutWindow(string windowTitle = "", string executablePath = "") : base(windowTitle, executablePath) => AddRunAfterCreatingDriverAction(() => DriverConnection.GetScreen<SelfCheckoutMainScreen>().OpenEfui());
    }
}
