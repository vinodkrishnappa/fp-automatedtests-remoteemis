﻿using Utils.Screens;
using Utils.Utils.Drivers;

namespace Utils.Components.Windows.Simulator
{
    public class MuxWindow : Window
    {
        public MuxWindow(string title = "", string executablePath = "", int comPort = 0) : base(title, executablePath) => AddRunBeforeBindingDriverAction(() => DriverConnection.GetScreen<MuxScreen>().Configure(comPort));
    }
}
