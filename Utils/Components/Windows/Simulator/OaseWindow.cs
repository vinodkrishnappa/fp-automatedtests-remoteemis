﻿using Utils.Screens;
using Utils.Utils.Drivers;

namespace Utils.Components.Windows.Simulator
{
    public class OaseWindow : Window
    {
        public OaseWindow(string title = "", string executablePath = "") : base(title, executablePath) => AddRunBeforeBindingDriverAction(() => DriverConnection.GetScreen<OaseScreen>().Configure());
    }
}
