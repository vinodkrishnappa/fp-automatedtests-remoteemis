﻿using Utils.Screens;
using Utils.Utils.Drivers;
using Utils.Utils.Enums;

namespace Utils.Components.Windows.Simulator
{
    public class PinPadWindow : Window
    {
        public PinPadWindow(string title = "", string executablePath = "", int comPort = 0, PinPadCards pinPadCardToActivate = null, PinPadCardSimulationModes cardSimulationMode = null) : base(title, executablePath)
        {
            AddRunBeforeBindingDriverAction(() => DriverConnection.GetScreen<PinPadScreen>().Configure(comPort, pinPadCardToActivate, cardSimulationMode));
            AddRunBeforeCreatingDriverAction(() => DriverConnection.GetScreen<PinPadScreen>().RunBeforeSimulatorLaunch());
        }
    }
}
