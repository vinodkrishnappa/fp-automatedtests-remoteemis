﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Utils.Utils.ConfigurationManager;
using Utils.Utils.Enums;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Components.Windows.Simulator
{
    public sealed class SimulatorWindows
    {
        private static readonly Dictionary<string, Window> s_windows = new Dictionary<string, Window>();

        public static Window Mux => GetWindow(nameof(Mux), () => new MuxWindow(SimulatorSettings.MuxSimulatorWindowTitle, SimulatorSettings.MuxSimulatorPath, SimulatorSettings.MuxSimulatorComPort));
        public static Window Oase => GetWindow(nameof(Oase), () => new OaseWindow(SimulatorSettings.OaseSimulatorWindowTitle, SimulatorSettings.OaseSimulatorPath));

        public static Window PinPad => GetWindow(nameof(PinPad), () => new PinPadWindow(
            SimulatorSettings.PinPadSimulatorWindowTitle,
            SimulatorSettings.PinPadSimulatorPath,
            SimulatorSettings.PinPadSimulatorComPort,
            PinPadCards.Parse(SimulatorSettings.PinPadSimulatorPinPadCard),
            PinPadCardSimulationModes.Parse(SimulatorSettings.PinPadSimulatorCardSimulationMode))
        );

        private static ISimulatorSettings SimulatorSettings => ConfigurationManager.Config.SimulatorSettings;

        public static bool IsSimulatorWindow(Window window) => typeof(SimulatorWindows)
            .GetProperties(BindingFlags.Public | BindingFlags.Static)
            .Select(property => property.GetValue(null))
            .Any(property => (property as Window)?.Equals(window) ?? false);

        private static Window GetWindow(string uniqueIdentifier, Func<Window> makeNewWindow)
        {
            if (s_windows.ContainsKey(uniqueIdentifier))
                return s_windows[uniqueIdentifier];

            Window window = makeNewWindow();
            s_windows.Add(uniqueIdentifier, window);

            return window;
        }
    }
}
