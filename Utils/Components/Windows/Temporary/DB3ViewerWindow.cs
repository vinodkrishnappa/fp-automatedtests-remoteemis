﻿using System;
using static Utils.Utils.Utils;

namespace Utils.Components.Windows.Temporary
{
    public class DB3ViewerWindow : Window
    {
        public DB3ViewerWindow(string windowTitle = "", string executablePath = "") : base(windowTitle, executablePath) => AddRunBeforeCreatingDriverAction(() => LaunchDB3Viewer());

        private void LaunchDB3Viewer() => RunPowerShellCommand(@"C:\FPD\SYSTEM\SoftwareVersions.db3", TimeSpan.FromSeconds(5));
    }
}
