﻿using System;
using System.Diagnostics;
using Utils.Utils.SettingsInterfaces;
using Utils.Utils.ConfigurationManager;
using Utils.Utils.Drivers;
using Utils.Components.Screens.RemoteEmis;
using Utils.Components.Windows.Main;
using static Utils.Utils.Utils;

namespace Utils.Components.Windows.Temporary
{
    public class RemoteEmisWindow : Window
    {
        public RemoteEmisWindow(string title = "", string executablePath = "", string installerPath = "") : base(title, executablePath)
        {
            if (installerPath != "")
            {
                AddRunBeforeCreatingDriverAction(() => InstallRemoteEmis(installerPath));
                AddRunBeforeCreatingDriverAction(() => LaunchRemoteEmis());
            }
        }

        public static void InstallRemoteEmis(string args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                FileName = "msiexec.exe",
                Arguments = string.Format("/qn /i \"{0}\" ALLUSERS=1", args),
                Verb = "runas"
            };
            var process = Process.Start(startInfo);
            process.WaitForExit();
        }


        public static void LaunchRemoteEmis()
        {
            //const string eMISPath = @"C:\ProgramData\Tokheim\eMIS\";
            //string AppName = "emis.exe";
            //RunPowerShellCommand($"cd {eMISPath}; {AppName}", TimeSpan.FromSeconds(5)); 

            const string eMISPath = @"C:\ProgramData\Tokheim\eMIS\";
            string AppName = "emis.exe";
            var process = new Process();
            process.StartInfo = new ProcessStartInfo()
                {
                    UseShellExecute = true,
                    WorkingDirectory = eMISPath,
                    FileName = AppName
                };
             process.Start();
        }


        public static void UninstallRemoteEmis(string args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                FileName = "msiexec.exe",
                Arguments = string.Format("/qn /x \"{0}\" ALLUSERS=1", args),
                Verb = "runas"
            };
            var process = Process.Start(startInfo);
            process.WaitForExit();
        }




        protected static RemoteEmisScreen RemoteEmisScreen => DriverConnection.GetScreen<RemoteEmisScreen>();


    }
}
