﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Utils.Utils.ConfigurationManager;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Components.Windows.Temporary
{
    /// <summary>
    /// Temporary windows get disposed if a screen which uses them gets disposed, unless dispose temporary window is set to false (see BaseScreen)
    /// </summary>
    public sealed class TemporaryWindows
    {
        private static readonly Dictionary<string, Window> s_windows = new Dictionary<string, Window>();
        private static IRemoteEmisSettings RemoteEmisSettings => ConfigurationManager.Config.RemoteEmisSettings;

        public static Window Com0Com => GetWindow(nameof(Com0Com), () => new Window("Setup for com0com"));
        public static Window ComPortSelection => GetWindow(nameof(ComPortSelection), () => new Window("COM Port selection"));
        public static Window DB3Viewer => GetWindow(nameof(DB3Viewer), () => new DB3ViewerWindow(@"C:\FPD\SYSTEM\SoftwareVersions.db3 - DB3Viewer"));
        public static Window FuelPosLoader => GetWindow(nameof(FuelPosLoader), () => new Window("Fuel-POS Loader"));
        public static Window FuelPosSetup => GetWindow(nameof(FuelPosSetup), () => new Window("Fuel-POS Setup"));
        public static Window RecoveryReboot => GetWindow(nameof(RecoveryReboot), () => new Window("KeepCnsl"));
        public static Window RemoteEmis => GetWindow(nameof(RemoteEmis), () => new RemoteEmisWindow(RemoteEmisSettings.RemoteEmisWindowTitle, RemoteEmisSettings.RemoteEmisPath, Constants.RemoteEmisDefault));


        public static Window IntermarcheRemoteEmis => GetWindow(nameof(RemoteEmis), () => new RemoteEmisWindow(RemoteEmisSettings.RemoteEmisWindowTitle, RemoteEmisSettings.RemoteEmisPath, Constants.RemoteEmisDefault));

        public static bool IsTemporaryWindow(Window window) => typeof(TemporaryWindows)
            .GetProperties(BindingFlags.Public | BindingFlags.Static)
            .Select(property => property.GetValue(null))
            .Any(property => (property as Window)?.Equals(window) ?? false);

        private static Window GetWindow(string uniqueIdentifier, Func<Window> makeNewWindow)
        {
            if (s_windows.ContainsKey(uniqueIdentifier))
            {
                if (!s_windows[uniqueIdentifier].IsDisposed)
                    return s_windows[uniqueIdentifier];
                else
                    s_windows.Remove(uniqueIdentifier);
            }

            Window window = makeNewWindow();
            s_windows.Add(uniqueIdentifier, window);
            return window;
        }
    }
}
