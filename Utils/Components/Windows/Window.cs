﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Utils.Utils;
using Utils.Utils.Drivers;
using static Utils.Utils.Utils;

namespace Utils.Components.Windows
{
    public class Window : IEquatable<Window>, IDisposable
    {
        private static WindowsDriver<WindowsElement> s_desktopDriver;
        private Action _runAfterCreatingDriver;
        private Action _runBeforeBindingDriver;
        private Action _runBeforeCreatingDriver;

        public Window(string title = "", string executablePath = "")
        {
            _title = title;
            _executablePath = executablePath;
        }

        public AppiumDriver<WindowsElement> _appiumDriver { get; private set; }

        public string _executablePath { get; }
        public string _title { get; }
        public bool Exists => ReturnWindowAsWindowsElement != null;
        public bool IsDisposed { get; private set; } = false;

        public bool IsFocused
        {
            get
            {
                bool isWindowFocused = false;

                // Return true means, wait is complete, not that the window exists
                WaitUntil(
                    () =>
                    {
                        try
                        {
                            isWindowFocused = s_desktopDriver.FindElementByName(_title).Displayed;
                            return true;
                        }
                        // No window found
                        catch (NoSuchElementException)
                        {
                            isWindowFocused = false;
                            return true;
                        }
                        // The HttpTimeOut exception can happen during an update. It can mean that the connection broke.
                        // The SessionTerminated exception can happen during an update.
                        catch (WebDriverException exception)
                            when (WebDriverExceptionFinder.IsHttpTimeOutException(exception) || WebDriverExceptionFinder.IsSessionTerminatedException(exception))
                        {
                            Logger.LogVerboseInfo($"IsWindowFocused '{_title}' threw (safe to ignore) \n {Logger.GetExceptionInfo(exception)}");
                        }
                        catch (Exception e) { throw new Exception($"IsWindowFocused threw unexpected exception \n {Logger.GetExceptionInfo(e)}"); }
                        return false;
                    },
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromMinutes(5),
                    "IsWindowFocused timed out"
                );
                return isWindowFocused;
            }
        }

        public WindowsElement ReturnWindowAsWindowsElement
        {
            get
            {
                WindowsElement window = null;

                // Return true means, wait is complete, not that the window exists
                WaitUntil(
                    () =>
                    {
                        try
                        {
                            window = s_desktopDriver.FindElementByName(_title);
                            return true;
                        }
                        // No window found
                        catch (NoSuchElementException) { return true; }
                        // The HttpTimeOut exception can happen during an update. It can mean that the connection broke.
                        // The SessionTerminated exception can happen during an update.
                        catch (WebDriverException exception)
                            when (WebDriverExceptionFinder.IsHttpTimeOutException(exception) || WebDriverExceptionFinder.IsSessionTerminatedException(exception))
                        {
                            Logger.LogVerboseInfo($"DoesWindowExist '{_title}' threw (safe to ignore) \n {Logger.GetExceptionInfo(exception)}");
                        }
                        catch (Exception e) { throw new Exception($"DoesWindowExist threw unexpected exception \n {Logger.GetExceptionInfo(e)}"); }
                        return false;
                    },
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromMinutes(5),
                    "DoesWindowExist timed out"
                );
                return window;
            }
        }

        public static void SetDesktopDriver(WindowsDriver<WindowsElement> windowsDriver) => s_desktopDriver = windowsDriver;

        public void AddRunAfterCreatingDriverAction(Action action) => _runAfterCreatingDriver += action;

        public void AddRunBeforeBindingDriverAction(Action action) => _runBeforeBindingDriver += action;

        public void AddRunBeforeCreatingDriverAction(Action action) => _runBeforeCreatingDriver += action;

        public void Dispose()
        {
            IsDisposed = true;
            GC.SuppressFinalize(this);
        }

        public bool Equals(Window other) => other._title == _title && other._executablePath == _executablePath && other._appiumDriver == _appiumDriver;

        public void IntializeAppiumDriver()
        {
            _runBeforeCreatingDriver?.Invoke();
            _appiumDriver = DriverConnection.CreateDriver(this, _runBeforeBindingDriver);
            _runAfterCreatingDriver?.Invoke();
        }

        public void WaitUntilDoesNotExists(TimeSpan timeout) => WaitUntilExist(TimeSpan.FromSeconds(1), timeout, false);

        public void WaitUntilExists(TimeSpan timeout) => WaitUntilExist(TimeSpan.FromSeconds(1), timeout, true);

        public void WaitUntilIsFocused(TimeSpan timeout) => WaitUntil(() => IsFocused, TimeSpan.FromSeconds(1), timeout);

        private void WaitUntilExist(TimeSpan frequencyToCheck, TimeSpan timeOut, bool expectExist = true) => WaitUntil(
            () => expectExist == Exists,
            frequencyToCheck,
            timeOut,
            expectExist ? $"Could not find window '{_title}' within the timespan" : $"Could still find window '{_title}' within the timespan"
        );
    }
}
