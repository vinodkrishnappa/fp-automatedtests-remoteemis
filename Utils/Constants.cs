﻿namespace Utils
{
    public static class Constants
    {
        #region Test Method Attributes

        public const string Attribute_Author_Jim = "Jim Verschueren";
        public const string Attribute_Author_Robin = "Robin Wils";
        public const string Attribute_Author_Vinod = "Vinod Krishnappa";
        public const string Attribute_TestCaseId = "TestCaseId";
        public const string releaseBuild = "false";
        public const string RemoteEmisversion = "eMIS v60.02.334170";
        public const string s_versionToUpdateToFullBuildNumber = "60.01.0000002.0000334159";
        public const string TestCategoryFunctional = "Release";
        public const string TestCategorySmoke = "Smoke";

        #endregion Test Method Attributes

        #region Paths

        public const string FtpDeleteAccessSystemAccountFile = "SYSTEM.DEL";
        public const string FtpDirectoryActivate = "/ACTIVATE";
        public const string FtpDirectoryEBOC = "/Pro_bo/EBOC";
        public const string FtpDirectoryFPD = "/FPD";
        public const string FtpDirectoryImages = "/images/ART_KEYS";
        public const string FtpDirectoryProBoText = "/Pro_bo/text";
        public const string FtpDownloadAccessSystemAccountFile = "SYSTEM.DLC";
        public const string FtpFpdFinishedAsc = "FINISHED.ASC";
        public const string FtpFpdRestartAsc = "RESTART.ASC";
        public const string FtpUploadAccessSystemAccountFile = "SYSTEM.ULC";
        public const string ImagesSourceDirectoryOnPDrive = @"P:\Testing\Mutation files\images\ART_KEYS";
        public const string RemoteEmisDefault = @"C:\FuelPosAutomation\ReadyForTest\GroupFuelPos\FuelPos\60.01.0000002.0000334159\installable\eMisInstaller\Default\eMisSetup.msi";
        public const string VboxDirectory = @"C:\Program Files\Oracle\VirtualBox";

        #endregion Paths

        #region Constants

        public const string DefaultArtMutFile2Name = "ART_MUT.002";
        public const string DefaultArtMutFile3Name = "ART_MUT.003";
        public const string DefaultArtResFile2Name = "ART_RES.002";
        public const string DefaultArtResFile3Name = "ART_RES.003";
        public const string DefaultKeyMutFile2Name = "KEY_MUT.002";
        public const string DefaultKeyResFile2Name = "KEY_RES.002";

        public const string FuelNameDiesel = "3 - Diesel";

        public const string FuelNameLPG = "4 - L.P.G.";

        //Still needed as constants because the FillingTypes can not be given as
        //Test parameters as they are not constant.
        public const string FuelNameSuper = "1 - Super";

        public const string FuelNameSuperPlus = "2 - Super plus";

        //Should be 7 but currently not enough builds available with the FuelPosLoader
        public const int FuelPosVersionsToGoBack = 2;

        public const int MaxMinutesToWaitUntilFuelPosIsUpdated = 20;

        //The pre and suf-fix that enclose the Major and Minor version number of the snapshot
        public const string SnapshotDefaultPrefix = "DEFAULT_EURO_";

        //Hard coded country and company for now - to be made dynamic later
        public const string SnapshotSuffix = "_BELGIUM_OTHER";

        public const string SnapshotUpdatedPrefix = "UPDATED_EURO_";

        //Would like to always include version 49 for the Updatetests
        //But this is not possible for now due to the FuelPosLoader fix not being build on that version
        public static readonly string[] FuelPosVersionsToAlwaysInclude = { /*"49"*/ };

        //Excluding 58 and any versions that do not have the new FuelPosLoader
        public static readonly string[] FuelPosVersionsToSkip = { "58", "53", "52", "51", "50", "49", "48", "47" };

        #endregion Constants

        // Elements

        #region SCO

        public const string DutchAssistText = "Eén van onze medewerkers zal u zo spoedig mogelijk helpen!";
        public const string DutchPleasewait = "Een ogenblik geduld";
        public const string DutchSelectYourPreference = "Selecteer uw voorkeur";
        public const string EnglishAssistText = "One of our staff members will assist you shortly!";
        public const string EnglishPleaseWait = "Please wait";
        public const string EnglishSelectYourPreference = "Select your preference";
        public const string FrenchAssistText = "Un membre de notre personnel vous assistera prochainement!";
        public const string FrenchHelp = "Aide";
        public const string FrenchPleaseWait = "Veuillez patienter";
        public const string FrenchSelectYourPreference = "Sélectionnez votre préférence";
        public const string GermanAssistText = "Ein Mitarbeiter wird Ihnen sofort helfen!";
        public const string GermanHelp = "Hilfe";
        public const string GermanPleasewait = "Bitte warten";
        public const string GermanSelectYourPreference = "Bitte wählen Sie aus";
        public const string OneofOurStaffMsg = "One of our staff members will gladly assist you further";
        public const string SelectYourPump = "Select your pump";
        public const string TillClosed = "Till is closed";

        #endregion SCO
    }
}
