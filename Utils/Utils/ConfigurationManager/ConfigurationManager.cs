﻿using System;
using System.Diagnostics;
using System.IO;
using Config.Net;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Utils.ConfigurationManager
{
    public static class ConfigurationManager
    {
        static ConfigurationManager()
        {
            if (Debugger.IsAttached)
            {
                Config = new ConfigurationBuilder<ISettings>()
                    // The order in which sources are added is importants
                    // Config.Net will try to read the source in the configured order and return the value from the first store where it exists.

                    // Command line arguments
                    .UseCommandLineArgs()
                    // Environment variables
                    .UseEnvironmentVariables()
                    // Appsettings
                    .UseJsonConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json"))
                    // Appsettings - secrets
                    .UseJsonFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "secrets.json"))
                    // Override needed settings for debug
                    .UseJsonConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.Debug.json"))
                    .Build();
            }
            else
            {
                Config = new ConfigurationBuilder<ISettings>()
                    // The order in which sources are added is importants
                    // Config.Net will try to read the source in the configured order and return the value from the first store where it exists.

                    // Command line arguments
                    .UseCommandLineArgs()
                    // Environment variables
                    .UseEnvironmentVariables()
                    // Appsettings
                    .UseJsonConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json"))
                    // Appsettings - secrets
                    .UseJsonFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "secrets.json"))
                    .Build();
            }
        }

        public static ISettings Config { get; }
    }
}
