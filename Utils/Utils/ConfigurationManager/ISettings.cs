﻿namespace Utils.Utils.SettingsInterfaces
{
    public interface ISettings
    {
        IDebugOptions DebugOptions { get; }
        IDriverConnectionSettings DriverConnectionSettings { get; }
        IFilePaths FilePaths { get; }
        IRecoveryRebootSettings RecoveryRebootSettings { get; }
        IReports Reports { get; }
        ISecrets Secrets { get; }
        ISimulatorSettings SimulatorSettings { get; }
        IVirtualBoxSettings VirtualBoxSettings { get; }
        IRemoteEmisSettings RemoteEmisSettings { get; }
    }
}
