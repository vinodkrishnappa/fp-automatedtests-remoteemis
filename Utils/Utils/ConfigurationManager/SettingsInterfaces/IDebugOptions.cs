﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IDebugOptions
    {
        [Option(DefaultValue = true)]
        bool CreateRunningVboxesFiles { get; }

        [Option(DefaultValue = false)]
        bool LogVerbose { get; }

        [Option(DefaultValue = true)]
        bool RemoveTempDirectory { get; }
    }
}
