﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IDriverConnectionSettings
    {
        string AppiumNpmPackagePath { get; }

        [Option(DefaultValue = 5)]
        int MaxMinutesToWaitOnPing { get; }

        [Option(DefaultValue = 60)]
        int MinutesUntilDriverConnectionTimeOut { get; }
    }
}
