﻿namespace Utils.Utils.SettingsInterfaces
{
    public interface IFilePaths
    {
        string LocalFilesPath { get; }
        string TempDirectoryPath { get; }
    }
}
