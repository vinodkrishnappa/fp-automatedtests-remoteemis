﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IRecoveryRebootSettings
    {
        [Option(DefaultValue = 10)]
        int MaxMinutesToWaitForARecoveryReboot { get; }

        [Option(DefaultValue = 5)]
        int FrequencyToCheckForARecoveryRebootInSeconds { get; }
    }
}
