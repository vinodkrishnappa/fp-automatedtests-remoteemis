﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IRemoteEmisSettings
    {
        #region RemoteEmis

        [Option(Alias = "RemoteEmis.Path")]
        string RemoteEmisPath { get; }

        [Option(Alias = "RemoteEmis.WindowTitle")]
        string RemoteEmisWindowTitle { get; }

        #endregion RemoteEmis

    }
}
