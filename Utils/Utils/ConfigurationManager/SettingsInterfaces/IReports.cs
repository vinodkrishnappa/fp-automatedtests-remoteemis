﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IReports
    {
        string FromEmail { get; }

        [Option(DefaultValue = 150)]
        int MaxAmountOfReportsToKeep { get; }

        string ResultCCMail { get; }

        string ResultEmailId { get; }

        string TeamCityBuildLogUrl { get; }
    }
}
