﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface ISecrets
    {
        #region Connection

        [Option(Alias = "Connection.AppiumDriverPort")]
        int AppiumDriverPort { get; }

        [Option(Alias = "Connection.PosIP")]
        string PosIP { get; }

        [Option(Alias = "Connection.LocalIP")]
        string LocalIP { get; }

        [Option(Alias = "Connection.WinAppDriverPort")]
        int WinAppDriverPort { get; }

        #endregion Connection

        #region Emis login

        [Option(Alias = "Emis.Password")]
        string EmisPassword { get; }

        [Option(Alias = "Emis.Username")]
        string EmisUsername { get; }

        [Option(Alias = "Emis.Station")]
        string StationNumber { get; }
        #endregion Emis login

        #region FTP login

        [Option(Alias = "FTP.Password")]
        string FtpPassword { get; }

        [Option(Alias = "FTP.Username")]
        string FtpUsername { get; }

        [Option(Alias = "FTP.SecondaryPassword")]
        string FtpSecondaryPassword { get; }

        [Option(Alias = "FTP.SecondaryUsername")]
        string FtpSecondaryUsername { get; }
        #endregion FTP login
    }
}
