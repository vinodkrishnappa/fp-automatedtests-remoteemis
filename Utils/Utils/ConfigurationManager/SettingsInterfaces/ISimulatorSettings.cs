﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface ISimulatorSettings
    {
        #region Oase

        [Option(Alias = "Oase.Path")]
        string OaseSimulatorPath { get; }

        [Option(Alias = "Oase.WindowTitle")]
        string OaseSimulatorWindowTitle { get; }

        #endregion Oase

        #region Mux

        [Option(Alias = "Mux.ComPort")]
        int MuxSimulatorComPort { get; }

        [Option(Alias = "Mux.Path")]
        string MuxSimulatorPath { get; }

        [Option(Alias = "Mux.WindowTitle")]
        string MuxSimulatorWindowTitle { get; }

        #endregion Mux

        #region PinPad

        [Option(Alias = "PinPad.CardSimulationMode")]
        string PinPadSimulatorCardSimulationMode { get; }

        [Option(Alias = "PinPad.ComPort")]
        int PinPadSimulatorComPort { get; }

        [Option(Alias = "PinPad.Path")]
        string PinPadSimulatorPath { get; }

        [Option(Alias = "PinPad.Card")]
        string PinPadSimulatorPinPadCard { get; }

        [Option(Alias = "PinPad.WindowTitle")]
        string PinPadSimulatorWindowTitle { get; }

        #endregion PinPad
    }
}