﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IVboxManageSettings
    {
        [Option(DefaultValue = 1)]
        int DefaultVboxCommandTimeOutInMinutes { get; }

        [Option(DefaultValue = 5)]
        int MaxAmountOfSnapshotsToKeepPerFuelPosVersion { get; }
    }
}
