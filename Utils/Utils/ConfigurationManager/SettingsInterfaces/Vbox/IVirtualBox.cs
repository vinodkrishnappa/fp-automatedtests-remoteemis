﻿using Config.Net;

namespace Utils.Utils.SettingsInterfaces
{

    public interface IVirtualBox
    {
        string DefaultInitialSnapshot { get; }

        int DefaultNetworkCard { get; }

        string Name { get; }
    }
}
