﻿using System.Collections.Generic;

namespace Utils.Utils.SettingsInterfaces
{
    public interface IVirtualBoxSettings
    {
        IEnumerable<IVirtualBox> Vboxes { get; }
        IVboxManageSettings VboxManageSettings { get; }
    }
}
