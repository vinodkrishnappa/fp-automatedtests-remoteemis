﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Windows;
using Resources.Components.Screens;
using Resources.Constants.Images;
using Utils.Components.Screens.Efui;
using Utils.Components.Screens.Emis;
using Utils.Components.Windows;
using Utils.Components.Windows.Main;
using Utils.Components.Windows.Simulator;
using Utils.Components.Windows.Temporary;
using Utils.Utils.SettingsInterfaces;
using Utils.Utils.VboxUtils;
using static Utils.Utils.Utils;

namespace Utils.Utils.Drivers
{
    public class DriverConnection
    {
        #region configs

        private static IDriverConnectionSettings DriverConnectionConfig => ConfigurationManager.ConfigurationManager.Config.DriverConnectionSettings;
        private static ISecrets SecretsConfig => ConfigurationManager.ConfigurationManager.Config.Secrets;

        #endregion configs

        private static readonly List<BaseScreen> s_screens = new List<BaseScreen>();
        private static readonly List<Window> s_simulators = new List<Window>();
        private static Dictionary<Window, AppiumDriver<WindowsElement>> s_drivers = new Dictionary<Window, AppiumDriver<WindowsElement>>();

        private static string s_posIp;
        private static string s_posUriString;
        private static VBoxManager s_vmToUse;

        public DriverConnection(DriverOptions driverOptions = null)
        {
            driverOptions.MainWindows.ToList().ForEach(mainWindow => DriverConnection.AddMainWindow(mainWindow));
            driverOptions.SimulatorWindows.ToList().ForEach(simulator => DriverConnection.AddSimulator(simulator));

            s_vmToUse = driverOptions?.VboxOptions.VboxManager;
            if (s_vmToUse != null)
            {
                s_vmToUse.ToggleInUse(true);
                s_vmToUse.DoAfterStart = () => SetPosIpThroughVm(driverOptions.UseLocalIp);
                
                if (driverOptions.VboxOptions.RestoreDefaultSnapshotBeforeConnect == true)
                    s_vmToUse.RestoreDefaultInitialSnapshot();
            }
            else
            {
                s_posIp = driverOptions.Ip;
                Logger.LogInfo($"{nameof(DriverConnection)} - No vmToUse given, using IP '{s_posIp}' from DriverOptions");

                SetPosUriString();
            }

            DriverUtils.SetDriverConnection(this);

            if (driverOptions.ConnectAfterFirstInitialisation)
                Connect();
        }

        /// <summary>
        /// Verifies of the VM can be pinged
        /// </summary>
        public static bool FuelPosCanBePinged
        {
            get
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(s_posIp);
                return pingReply.Status == IPStatus.Success;
            }
        }

        internal static WindowsDriver<WindowsElement> _desktopDriver { get; private set; }

        /// <summary>
        /// Adds a MainWindow which will be initialised on every restart of fuelPos
        /// Adding it to the DriverOptions is recommended over calling this
        /// </summary>
        /// <param name="mainWindow"></param>
        /// <returns></returns>
        public static void AddMainWindow(Window mainWindow)
        {
            if (MainWindows.IsMainWindow(mainWindow))
                s_drivers[mainWindow] = null;
        }

        /// <summary>
        /// Adds a Simulator which will be initialised on every restart of fuelPos
        /// Adding it to the DriverOptions is recommended over calling this
        /// </summary>
        /// <param name="simulatorWindow"></param>
        /// <returns></returns>
        public static void AddSimulator(Window simulatorWindow)
        {
            if (SimulatorWindows.IsSimulatorWindow(simulatorWindow))
                s_simulators.Add(simulatorWindow);
        }

        /// <summary>
        /// Closes all drivers in the session, and the session itself
        /// </summary>
        public static void CloseConnection()
        {
            List<AppiumDriver<WindowsElement>> driversToClose = s_drivers.Values.ToList();
            driversToClose.Add(_desktopDriver);
            driversToClose.ForEach(driver => driver?.Quit());
        }

        /// <summary>
        /// Creates the AppiumDriver for a window. Each MainWindow will add itself to the DriverConnection automagically
        /// </summary>
        /// <param name="screen"></param>
        public static AppiumDriver<WindowsElement> CreateDriver(Window window, Action RunBeforeBindingDriver = null)
        {
            if (window.Exists)
            {
                Logger.LogInfo($"{window._title} is already running, binding to the window");
            }
            else if (window._executablePath != "")
            {
                Logger.LogInfo($"Launching {window._title}");

                AppiumDriver<WindowsElement> appiumDriver;
                try
                {
                    Logger.LogInfo($"{window._title} is already running.");
                    AppiumOptions capabilitiesSCO = new AppiumOptions();
                    capabilitiesSCO.AddAdditionalCapability(MobileCapabilityType.DeviceName, "WindowsPC");
                    capabilitiesSCO.AddAdditionalCapability("app", window._executablePath);
                    appiumDriver = CreateDriverWithCapabilities(capabilitiesSCO);
                }
                catch (Exception e) { throw new Exception($"Problem while trying to launch window {window._title} - '{window._executablePath}'", e); }
            }
            RunBeforeBindingDriver?.Invoke();
            return BindSessionToApplication(window);
        }

        /// <summary>
        /// Gets a driver from the desktop session
        /// </summary>
        /// <param name="window"></param>
        public static AppiumDriver<WindowsElement> GetDriver(Window window)
        {
            Window key = s_drivers.Keys.ToList().Find(item => item.Equals(window));
            return key == null ? null : s_drivers[key];
        }

        /// <summary>
        /// Returns a screen if it exists, make the screen and return it if the screen does not exist yet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetScreen<T>(params object[] constructorArgs) where T : BaseScreen
        {
            T screen = (T)s_screens.Find(item => item.GetType() == typeof(T));
            if (screen?.IsDisposed != false)
            {
                s_screens.Remove(screen);
                screen = constructorArgs.Length > 0 ? (T)Activator.CreateInstance(typeof(T), constructorArgs) : (T)Activator.CreateInstance(typeof(T));
                s_screens.Add(screen);
            }
            return screen;
        }

        public static VBoxManager GetVmInUse() => s_vmToUse;

        /// <summary>
        /// Start simulator, if it isn't initialised yet after the connection is made, without having to reconnect
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void InitialiseSimulator<T>() where T : Window
        {
            Window existingSimulator = s_simulators.Find(sim => sim.GetType() == typeof(T));
            if (existingSimulator != null)
                existingSimulator.IntializeAppiumDriver();
            else
                Logger.LogWarning($"Can't initialise simulator '{typeof(T)}' before it is added to the DriverConnection");
        }

        public static void Reconnect(bool reinitializeDrivers = false)
        {
            Stopwatch stopWatch = Stopwatch.StartNew();

            // Close connection
            if (reinitializeDrivers)
                CloseConnection();

            // Start vm if needed
            if (s_vmToUse?.IsRunning == false)
                s_vmToUse.Start();

            // Wait until connection is available again
            DriverConnection.WaitOnFuelPosPing(TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(1));

            // Wait until drivers are available again
            WaitUntilDriversAreReady();

            // Recreate the drivers, since they depend on the connection.
            InitializeDrivers(reinitializeDrivers);

            stopWatch.Stop();
            Logger.LogInfo($"Connecting took {stopWatch.Elapsed} seconds.");
        }

        public static void WaitOnFuelPosPing(TimeSpan frequencyToCheck, TimeSpan timeout, bool expectPingSuccess = true) => WaitUntil(
            () => expectPingSuccess == FuelPosCanBePinged,
            frequencyToCheck,
            timeout,
            expectPingSuccess ? "Couldn't ping ip within the timespan" : "Could still ping ip within the timespan"
        );

        public static void WaitOnFuelPosUpdateReboot(TimeSpan timeout)
        {
            Logger.LogInfo("Waiting on Fuel-POS update reboot");
            EmisMainScreen eMisScreen = DriverConnection.GetScreen<EmisMainScreen>();

            bool AnotherRebootIsNeeded()
            {
                Logger.LogInfo("Verifying if another reboot is needed...");
                bool InstallationIsInProgress() => ImageHandler.ImageIsFoundOnScreen(EmisImages.InstallationInProgress, eMisScreen);

                // Create drivers
                DriverConnection.Reconnect(true);

                // Open Emis
                DriverConnection.GetScreen<EfuiMainScreen>().OpenEmis();

                // Verify of installation is still going on
                WaitUntil(
                    () =>
                    {
                        if (InstallationIsInProgress())
                            Logger.LogInfo("Installation is still busy, waiting one minute before retrying...");
                        return !InstallationIsInProgress();
                    },
                    TimeSpan.FromMinutes(1),
                    TimeSpan.FromMinutes(15),
                    "Waiting until installation finished failed - DriverUtils, WaitOnFuelPosUpdateReboot"
                );
                Logger.LogInfo("Waiting on Fuel-POS update reboot succeeded");
                return false;
            }

            WaitUntil(
                () =>
                {
                    if (!FuelPosCanBePinged)
                    {
                        // Fuel POS went down
                        Logger.LogInfo("Fuel-POS went down, waiting until it goes up...");
                        WaitUntil(
                            () => FuelPosCanBePinged,
                            TimeSpan.FromSeconds(1),
                            timeout,
                            "Couldn't wait until Fuel-POS was up again"
                        );
                        return !AnotherRebootIsNeeded();
                    }

                    return TemporaryWindows.FuelPosLoader.Exists && !AnotherRebootIsNeeded();
                },
                // The function itself takes long, so the frequency can be low
                TimeSpan.FromSeconds(1),
                timeout,
                "Waiting on FuelPosUpdateReboot failed"
            );
        }

        /// <summary>
        /// Verifies of the driver ports are pingable
        /// </summary>
        public static void WaitUntilDriversAreReady()
        {
            WaitUntil(
                () => IsPortOpen(SecretsConfig.AppiumDriverPort) &&
                IsPortOpen(SecretsConfig.WinAppDriverPort),
                TimeSpan.FromMinutes(1),
                TimeSpan.FromMinutes(DriverConnectionConfig.MaxMinutesToWaitOnPing),
                "Couldn't reach drivers within the timespan"
            );
        }

        public static void WaitUntilFuelPosIsDown() => WaitUntilFuelPosIsDown(TimeSpan.FromMinutes(5));

        public static void WaitUntilFuelPosIsRebooted(TimeSpan timeout)
        {
            Logger.LogInfo("Waiting on Fuel-POS reboot");
            EmisMainScreen eMisScreen = DriverConnection.GetScreen<EmisMainScreen>();

            WaitUntil(
                () =>
                {
                    if (!FuelPosCanBePinged)
                    {
                        // Fuel POS went down
                        Logger.LogInfo("Fuel-POS went down, waiting until it goes up...");
                        WaitUntil(
                            () => FuelPosCanBePinged,
                            TimeSpan.FromSeconds(1),
                            timeout,
                            "Waiting on Fuel-POS to be up again took too long."
                        );
                        if (FuelPosCanBePinged)
                        {
                            Logger.LogInfo("Fuel-POS can be pinged again after going down.");
                            return true;
                        }
                        else
                        {
                            Logger.LogInfo("Fuel-POS could not be pinged after going down and waiting on the timeout.");
                            return false;
                        }
                    }

                    return false;
                },
                // The function itself takes long, so the frequency can be low
                TimeSpan.FromSeconds(1),
                timeout,
                "Fuel-POS never went down."
            );
        }

        public static void WaitUntilRecoveryRebootIsFinished()
        {
            Logger.LogInfo("Waiting for FuelPos to reboot.");
            //Wait until FuelPos reboots - it can no longer be pinged
            WaitOnFuelPosPing(TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(5), false);

            Logger.LogInfo("Reestablishing Connection & Reinitializing drivers.");
            //Reestablishing connection
            DriverConnection.Reconnect(true);
        }

        public void Connect(bool reinitializeDrivers = false) => Reconnect(reinitializeDrivers);

        internal static WindowsDriver<WindowsElement> CreateDriverWithCapabilities(AppiumOptions capabilities) => new WindowsDriver<WindowsElement>(new Uri(s_posUriString), capabilities);

        private static AppiumDriver<WindowsElement> BindSessionToApplication(Window windowToBind)
        {
            Logger.LogVerboseInfo($"Binding window '{windowToBind._title}' to session");
            windowToBind.WaitUntilExists(TimeSpan.FromMinutes(2));
            WindowsElement window = windowToBind.ReturnWindowAsWindowsElement;
            string mainWindowHandle = window.GetAttribute("NativeWindowHandle");
            mainWindowHandle = (int.Parse(mainWindowHandle)).ToString("x"); // Convert to Hex

            AppiumOptions capabilities = new AppiumOptions();
            capabilities.AddAdditionalCapability(MobileCapabilityType.DeviceName, "WindowsPC");
            capabilities.AddAdditionalCapability("appTopLevelWindow", mainWindowHandle);
            // Timeout of the driver
            capabilities.AddAdditionalCapability(MobileCapabilityType.NewCommandTimeout, Convert.ToInt32(TimeSpan.FromMinutes(DriverConnectionConfig.MinutesUntilDriverConnectionTimeOut).TotalSeconds));

            AppiumDriver<WindowsElement> driver = null;
            Retry(
                () => driver = CreateDriverWithCapabilities(capabilities) ?? throw new Exception($"Couldn't bind session to '{windowToBind._title}' succesfully"),
                TimeSpan.FromSeconds(1),
                3
            );
            Logger.LogVerboseInfo($"Binding window '{windowToBind._title}' to session succeeded");
            return driver;
        }

        private static void InitializeDrivers(bool reinitializeDrivers = false)
        {
            // First initialisation only stuff
            if (_desktopDriver == null)
            {
                // The order of the drivers matters, so sort them on first init
                Type[] screensOrder = { MainWindows.SelfCheckout.GetType(), MainWindows.Efui.GetType(), MainWindows.Emis.GetType() };

                s_drivers = s_drivers
                    .OrderBy(keyValuePair => screensOrder.ToList().IndexOf(keyValuePair.Key.GetType()))
                    .ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);

                // Update appium through offline npm package
                if (s_vmToUse != null && DriverConnectionConfig.AppiumNpmPackagePath != null && DriverConnectionConfig.AppiumNpmPackagePath.Length != 0)
                {
                    string path = s_vmToUse.AddTransientSharedFolder(DriverConnectionConfig.AppiumNpmPackagePath, "npm");
                    RunPowerShellCommandOnHost($"npm install -g '{path}'");
                }
            }

            if (reinitializeDrivers || _desktopDriver == null)
            {
                Logger.LogInfo("Creating desktop driver (session for appium drivers)");
                AppiumOptions capabilities = new AppiumOptions();
                capabilities.AddAdditionalCapability(MobileCapabilityType.App, "Root");
                capabilities.AddAdditionalCapability(MobileCapabilityType.DeviceName, "WindowsPC");

                // Timeout of the driver
                capabilities.AddAdditionalCapability(MobileCapabilityType.NewCommandTimeout, Convert.ToInt32(TimeSpan.FromMinutes(DriverConnectionConfig.MinutesUntilDriverConnectionTimeOut).TotalSeconds));
                _desktopDriver = CreateDriverWithCapabilities(capabilities);

                // The desktop driver is required for some methods
                Window.SetDesktopDriver(_desktopDriver);
                ImageHandler.SetDesktopDriver(_desktopDriver);
            }

            WaitOnFuelPosLoader();

            Logger.LogInfo($"Initializing MainScreen drivers [{string.Join(", ", s_drivers.Keys.Select(key => key._title))}]...");
            foreach (Window window in s_drivers.Keys.ToList())
            {
                // Better place for the ImageHandler resolution setting,
                // See ImageHandler.ResolutionIsSet method comments for more info
                // This method is currently called from the EfuiWindow, but it doesn't really belong there

                // Set the resolution if it isn't set
                // if (!ImageHandler.ResolutionIsSet)
                //    ImageHandler.SetResolution(window);

                // Create driver if not created yet
                if (reinitializeDrivers || window._appiumDriver == null)
                {
                    window.IntializeAppiumDriver();
                    s_drivers[window] = window._appiumDriver;
                }
            }

            // Intialize simulators
            if (reinitializeDrivers)
                s_simulators.ForEach(simulator => simulator?.IntializeAppiumDriver());
        }

        private static bool IsPortOpen(int portNumber)
        {
            try
            {
                using (TcpClient client = new TcpClient(s_posIp, portNumber))
                    return true;
            }
            catch (SocketException) { return false; }
        }

        private static void WaitOnFuelPosLoader()
        {
            string loaderElementId;

            // Check of there is a Fuel-POS Loader
            try
            {
                Retry(() => TemporaryWindows.FuelPosLoader.ReturnWindowAsWindowsElement, TimeSpan.FromSeconds(1), 10);
                Logger.LogInfo("Fuel-POS Loader detected");
                loaderElementId = TemporaryWindows.FuelPosLoader.ReturnWindowAsWindowsElement.Id;
            }
            catch (Exception)
            {
                Logger.LogInfo("No Fuel-POS Loader found -- not waiting on it");
                return;
            }

            bool loaderWasNotFoundInWait = false;
            Logger.LogInfo("Waiting until Fuel-POS Loader restarts...");
            WaitUntil(
                 () =>
                 {
                     try
                     {
                         WindowsElement fuelPosLoader = TemporaryWindows.FuelPosLoader.ReturnWindowAsWindowsElement;

                         // Loader not found, go to catch
                         if (fuelPosLoader == null)
                             throw new NoSuchElementException();

                         // Check of it is a new version of Fuel-POS Loader
                         if (fuelPosLoader?.Id != loaderElementId)
                         {
                             Logger.LogInfo("Restarted Fuel-POS Loader found...");
                             loaderWasNotFoundInWait = false;
                             return true;
                         }
                     }
                     catch (NoSuchElementException)
                     {
                         if (!loaderWasNotFoundInWait)
                         {
                             // Something odd happened, the loader disappeared, wait to see of it didn't just restart
                             // This isn't odd if the loader wasn't updated, for example when the Fuel-POS boots

                             // Sometimes it gives a false positive, but since there is a wait in the init drivers, it doesn't cause issues
                             Logger.LogInfo("Fuel-POS Loader disappeared waiting five seconds...");
                             Thread.Sleep(TimeSpan.FromSeconds(5));
                             loaderWasNotFoundInWait = true;
                             return false;
                         }
                         return true;
                     }
                     // The HttpTimeOut exception can happen during an update. It can mean that the connection broke.
                     catch (WebDriverException exception) when (WebDriverExceptionFinder.IsHttpTimeOutException(exception))
                     {
                         Logger.LogVerboseInfo($"WaitOnFuelPosLoader threw (safe to ignore) \n {Logger.GetExceptionInfo(exception)}");
                     }
                     // Catch all exceptions just in case, normally shouldn't be needed. In case something happens, log it.
                     catch (Exception exception) { Logger.LogWarning($"an unexpected exception appeared in WaitOnFuelPosLoader {Logger.GetExceptionInfo(exception)}"); }
                     return false;
                 },
                 TimeSpan.FromSeconds(1),
                 TimeSpan.FromMinutes(5),
                 "Couldn't wait until Fuel-POS Loader restarted"
            );

            // Something odd happened, and the loader didn't restart, but disappeared
            // This isn't odd if the loader wasn't updated, for example when the Fuel-POS boots
            if (loaderWasNotFoundInWait)
            {
                Logger.LogInfo("Fuel-POS Loader wasn't found after five seconds \n Exiting wait on Fuel-POS Loader...");
                return;
            }

            TemporaryWindows.FuelPosLoader.WaitUntilExists(TimeSpan.FromMinutes(5));
            Logger.LogInfo("Waiting until Fuel-POS Loader finishes...");
            TemporaryWindows.FuelPosLoader.WaitUntilDoesNotExists(TimeSpan.FromMinutes(5));
            Logger.LogInfo("Fuel-POS Loader finished...");
        }

        private static void WaitUntilFuelPosIsDown(TimeSpan timeout, bool checkOfVmIsDown = false)
        {
            WaitOnFuelPosPing(TimeSpan.FromSeconds(1), timeout, false);

            if (checkOfVmIsDown)
            {
                // Wait until Fuel Pos VM is down (is always passed if a VM isn't used)
                WaitUntil(
                    () => !DriverConnection.GetVmInUse().IsRunning,
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromMinutes(3),
                    "FuelPos VM is still running after timeout"
               );
            }
        }

        private void SetPosIpThroughVm(bool useLocalIp = false)
        {
            
            if (s_vmToUse != null && string.IsNullOrEmpty(s_posIp))
            {
                if (useLocalIp)
                    s_posIp = SecretsConfig.LocalIP;
                else
                    s_posIp = s_vmToUse.IP;
                Logger.LogInfo($"{nameof(DriverConnection)} - Use VM '{s_vmToUse.Name}' with IP {s_posIp}.");
                SetPosUriString();

                // Remove this action
                s_vmToUse.DoAfterStart = null;
            }
        }

        private void SetPosUriString()
        {
            // Set PosIp
            s_posUriString = $"http://{s_posIp}:4722/wd/hub";

            // Configure static classes
            FtpHelper.FtpHelper.InitializeFtpHelper(s_posIp);
        }
    }
}
