﻿using Utils.Components.Windows;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Utils.Drivers
{
    public class DriverOptions
    {

        public DriverOptions(Window[] mainWindows = null, Window[] simulatorWindows = null, bool connectAfterFirstInitialization = true, bool useLocalIp = false, string ip = "")
        {
            MainWindows = mainWindows ?? new Window[] { };
            SimulatorWindows = simulatorWindows ?? new Window[] { };
            Ip = ip.Length == 0 ? SecretsConfig.PosIP : ip;
            ConnectAfterFirstInitialisation = connectAfterFirstInitialization;
            UseLocalIp = useLocalIp;
        }

        public DriverOptions(Window[] mainWindows = null, Window[] simulatorWindows = null, bool connectAfterFirstInitialization = true, bool useLocalIp = false, VboxOptions vboxOptions = null)
        {
            MainWindows = mainWindows ?? new Window[] { };
            SimulatorWindows = simulatorWindows ?? new Window[] { };
            VboxOptions = vboxOptions ?? new VboxOptions();
            ConnectAfterFirstInitialisation = connectAfterFirstInitialization;
            UseLocalIp = useLocalIp;
        }
        public bool UseLocalIp { get; set; }
        public bool ConnectAfterFirstInitialisation { get; set; }
        public string Ip { get; set; }
        public Window[] MainWindows { get; set; }
        public Window[] SimulatorWindows { get; set; }
        public VboxOptions VboxOptions { get; set; }
        private static ISecrets SecretsConfig => ConfigurationManager.ConfigurationManager.Config.Secrets;
    }
}
