﻿using System;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Resources.Constants;
using Utils.Components.Windows.Temporary;

namespace Utils.Utils.Drivers
{
    /// <summary>
    /// Note: Adding code this class should be avoided.
    /// The name is vague, only stuff which requires s_driverConnection should go here.
    /// </summary>
    public static class DriverUtils
    {
        #region configs

        private static DriverConnection s_driverConnection;

        #endregion configs

        public static void CheckForASuccesfullRecovery()
        {
            // TODO: move to FtpHelper, it seems to be Ftp related, not driver related
            string firstLine = null;
            string[] restartLogFile = null;
            string[] finishedLogFile = null;

            FtpHelper.FtpHelper.DownloadFile(MutationFiles.FTPDownloadsDirectory, Constants.FtpDirectoryFPD, Constants.FtpFpdRestartAsc);
            FtpHelper.FtpHelper.DownloadFile(MutationFiles.FTPDownloadsDirectory, Constants.FtpDirectoryFPD, Constants.FtpFpdFinishedAsc);

            restartLogFile = File.ReadAllLines(MutationFiles.FTPDownloadsDirectory + Constants.FtpFpdRestartAsc);
            finishedLogFile = File.ReadAllLines(MutationFiles.FTPDownloadsDirectory + Constants.FtpFpdFinishedAsc);

            ResultFileParser.PrintStringArray(restartLogFile);
            Assert.That(restartLogFile.Length != 0, "A reboot is detected but RESTART.ASC is empty, the reason for a reboot should be logged in this file!");

            firstLine = finishedLogFile[0];
            Assert.That(firstLine.Equals("DONE"), $"FINISHED.ASC File did not contain the succes message 'DONE' - the first line read: {firstLine}");
            ResultFileParser.PrintStringArray(finishedLogFile);

            Logger.LogInfo("Succesful Recovery - Found succesmessage 'DONE' in the FINISHED.ASC file");
        }

        /// <summary>
        /// Checks for a defined period of time if the system enters a recovery reboot,
        /// this would happen if for example a process is killed.
        /// The reason for the reboot should be logged under C:/FPD/RESTART.ASC
        /// We download this log to our FTPDownloads folder and print it out in the testlog
        /// - further handling tbd
        /// </summary>
        /// <param name="frequencyToCheckInSeconds"></param>
        /// <param name="maxMinutesToCheck"></param>
        /// <returns></returns>
        public static bool HasTheSystemEnteredARecoveryReboot(int frequencyToCheckInSeconds, int maxMinutesToCheck)
        {
            bool RecoveryRebootScreenIsFound = false;
            int minutesElapsed = 0;
            DateTime start = DateTime.Now;

            try
            {
                Logger.LogInfo($"Starting '{maxMinutesToCheck}' minute check for the Recovery Reboot.");

                while (!RecoveryRebootScreenIsFound && DateTime.Now.Subtract(start).Minutes < maxMinutesToCheck)
                {
                    if (TemporaryWindows.RecoveryReboot.Exists)
                    {
                        RecoveryRebootScreenIsFound = true;
                        Logger.LogInfo("RecoveryRebootScreen detected, waiting for the recovery to complete.");
                    }
                    else
                    {
                        Thread.Sleep(1000 * frequencyToCheckInSeconds);
                        //We only print this log when a minute has passed to reduce spam
                        if (minutesElapsed < DateTime.Now.Subtract(start).Minutes)
                        {
                            minutesElapsed = DateTime.Now.Subtract(start).Minutes;
                            Logger.LogInfo(
                                $"Checking if no Recovery Reboot is initialized, waited for: {minutesElapsed}/{maxMinutesToCheck} minutes."
                            );
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(
                    $"Something went wrong whilst checking for the Recovery Reboot exception: {Logger.GetExceptionInfo(e)}"
                );
            }
            return RecoveryRebootScreenIsFound;
        }

        internal static void SetDriverConnection(DriverConnection driverConnection) => s_driverConnection = driverConnection;
    }
}
