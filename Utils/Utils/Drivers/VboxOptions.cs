﻿using Utils.Utils.VboxUtils;
using Utils.Utils.VboxUtils.Enums;

namespace Utils.Utils.Drivers
{
    public class VboxOptions
    {
        public VboxOptions(bool restoreDefaultSnapshotBeforeConnect = true, VBoxManager vboxManager = null)
        {
            RestoreDefaultSnapshotBeforeConnect = restoreDefaultSnapshotBeforeConnect;
            VboxManager = vboxManager ?? VBoxes.GetVmWhichIsNotInUse();
        }

        public bool RestoreDefaultSnapshotBeforeConnect { get; set; }

        public VBoxManager VboxManager { get; set; }
    }
}
