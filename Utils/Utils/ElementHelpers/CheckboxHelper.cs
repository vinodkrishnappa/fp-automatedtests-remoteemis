﻿using OpenQA.Selenium.Appium.Windows;

namespace Utils.Utils.ElementHelpers
{
    public static class CheckboxHelper
    {
        public static void SetCheckbox(WindowsElement checkbox, bool check)
        {
            if (checkbox.Selected ^ check)
                checkbox.Click();
        }
    }
}
