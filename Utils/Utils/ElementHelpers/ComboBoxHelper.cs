﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;

namespace Utils.Utils.ElementHelpers
{
    public static class ComboBoxHelper
    {

        //Cycles through the drop down list values until it matches the desired value
        public static void SetComboBox(WindowsElement checkBox, string valueToSelect)
        {
            string currentValue = "";
            //To detect when we have reached the end of the list.
            string previousValue = "";

            do
            {
                previousValue = currentValue;
                //Click to open the dropdown list
                checkBox.Click();
                //Move down one item
                checkBox.SendKeys(Keys.ArrowDown);
                //Click to set the item, otherwise the Value.Value attribute is not set.
                checkBox.Click();
                currentValue = checkBox.GetAttribute("Value.Value").Trim();
            } while (!currentValue.Equals(valueToSelect) && !currentValue.Equals(previousValue));

            previousValue = "Reset";
            while (!currentValue.Equals(valueToSelect) && !currentValue.Equals(previousValue))
            {
                previousValue = currentValue;
                //Click to open the dropdown list
                checkBox.Click();
                //Move down one item
                checkBox.SendKeys(Keys.ArrowUp);
                //Click to set the item, otherwise the Value.Value attribute is not set.
                checkBox.Click();
                currentValue = checkBox.GetAttribute("Value.Value").Trim();
            }

            Assert.That(currentValue.Equals(valueToSelect), $"Unable to set ComboBox to the desired value. Expected '{valueToSelect}' , actual value '{currentValue}'.");
        }
    }
}
