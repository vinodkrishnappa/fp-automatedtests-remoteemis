﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Utils
{
    public class EmailProccesor
    {
        #region configs

        private static IReports ReportsConfig => ConfigurationManager.ConfigurationManager.Config.Reports;

        #endregion configs

        /// <summary>
        /// Create Table format and style the body
        /// </summary>
        private string Style
        {
            get
            {
                StringBuilder sbStyle = new StringBuilder();
                if (string.IsNullOrEmpty(sbStyle.ToString()))
                {
                    sbStyle.AppendLine("<style = \"text/css\">\n");

                    sbStyle.AppendLine(".boldText, .boldtable TD, .boldtable TH\n");
                    sbStyle.AppendLine("{\n");
                    sbStyle.AppendLine("font-family: 'Arial Narrow', Arial, monospace;\n");
                    sbStyle.AppendLine("font-size:10pt;\n");
                    sbStyle.AppendLine("}\n");

                    sbStyle.AppendLine(".tableStyle\n");
                    sbStyle.AppendLine("{\n");
                    sbStyle.AppendLine("border-left: 1px solid black;\n");
                    sbStyle.AppendLine("border-right: 1px solid black;\n");
                    sbStyle.AppendLine("border-top-style:none;\n");
                    sbStyle.AppendLine("border-bottom-style:none;\n");

                    sbStyle.AppendLine("border-collapse: collapse;\n");
                    sbStyle.AppendLine("}\n");

                    sbStyle.AppendLine(".table,TD,TH\n");
                    sbStyle.AppendLine("{\n");
                    sbStyle.AppendLine("border-left-style:none;\n");
                    sbStyle.AppendLine("border-right: 1px solid black;\n");

                    sbStyle.AppendLine("border-bottom-style:none;\n");
                    sbStyle.AppendLine("border-top-style:1px solid\n");
                    sbStyle.AppendLine("border-collapse: collapse;\n");
                    sbStyle.AppendLine("}\n");

                    sbStyle.AppendLine(".noborder\n");
                    sbStyle.AppendLine("{\n");
                    sbStyle.AppendLine("border-left-style:none;\n");
                    sbStyle.AppendLine("border-right-style:none;\n");
                    sbStyle.AppendLine("border-top-style:none;;\n");
                    sbStyle.AppendLine("border-bottom-style:none;\n");
                    sbStyle.AppendLine("border-collapse: collapse;\n");
                    sbStyle.AppendLine("}\n");

                    sbStyle.AppendLine(".fullborder\n");
                    sbStyle.AppendLine("{\n");
                    sbStyle.AppendLine("border-left-style:none;;\n");
                    sbStyle.AppendLine("border-right: 1px solid black;\n");
                    sbStyle.AppendLine("border-bottom: 1px solid black;\n");
                    sbStyle.AppendLine("border-top: 1px solid black;\n");
                    sbStyle.AppendLine("border-collapse: collapse;\n");
                    sbStyle.AppendLine("}\n");

                    sbStyle.AppendLine("</style>\n");
                }
                return sbStyle.ToString();
            }
        }

        public void SendReportsEmail(string reportsDirectoryPath, Dictionary<string, string> testResults, TimeSpan totalTestRunTime) => SendEmail(
            $"Services Automation Result - {DateTime.Now.Date.ToString("dd/MM/yyyy")}",
            GetReportsMailBody(testResults, totalTestRunTime),
            ReportsConfig.FromEmail,
            ReportsConfig.ResultEmailId,
            ReportsConfig.ResultCCMail.Split(';'),
            // All html files in reportsConfig.Path directory
            Directory
                .EnumerateFiles(reportsDirectoryPath, "*.html")
                .Select(filePath => new Attachment(filePath, MediaTypeNames.Text.Html))
                .ToArray()
        );

        /// <summary>
        /// Genarate Mail Body
        /// </summary>
        /// <param name="testResults"></param>
        /// <param name="totalTestRunTime"></param>
        /// <returns>string</returns>
        private string GetReportsMailBody(Dictionary<string, string> testResults, TimeSpan totalTestRunTime)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<HTML>");
            sb.AppendLine("<HEAD>");
            sb.AppendLine(Style);
            sb.AppendLine("</HEAD>");
            sb.AppendLine("<BODY>");
            sb.AppendLine("<table bgcolor=\"#ffffff\" class=\"boldText\" frame=\"border\" border=\"2\" width=\"650\" align=\"LEFT\" >");
            sb.AppendLine("<TR height=\"3px\">");
            sb.AppendFormat(
                "<th  colspan=2 align=\"CENTER\" bgcolor=\"#009933\" ><H3 style=\"color:'#ffffff'\">{0}</H3></th>",
                $"Services Automation Test Result as on {DateTime.Now.Date.ToString("dd/MM/yyyy")}"
            ).AppendLine();
            sb.AppendLine("</TR>");

            IEnumerable<KeyValuePair<string, string>> filtered = from kvp in testResults
                                                                 where kvp.Value.Contains("Pass")
                                                                 select kvp;

            sb.AppendLine("<TR>");
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", " Environment").AppendLine();
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", "QA FuelPOS").AppendLine();
            sb.AppendLine("</TR>");
            sb.AppendLine("<TR>");
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", "FUnctioanl Test").AppendLine();
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", "Self CheckOut").AppendLine();
            sb.AppendLine("</TR>");
            sb.AppendLine("<TR>");
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", " Total").AppendLine();
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", testResults.Count).AppendLine();
            sb.AppendLine("</TR>");
            sb.AppendLine("<TR>");
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", " Passed").AppendLine();
            sb.AppendFormat("<TD ALIGN = \"LEFT\" style=\"color:#228B22;\"><b>{0}</b></TD>", filtered.Count()).AppendLine();
            sb.AppendLine("</TR>");
            sb.AppendLine("<TR>");
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", " Failed").AppendLine();
            sb.AppendFormat("<TD ALIGN = \"LEFT\" style=\"color:#FF0000;\"><b>{0}</b></TD>", testResults.Count - filtered.Count()).AppendLine();
            sb.AppendLine("</TR>");
            sb.AppendLine("<TR>");
            sb.AppendFormat("<TD ALIGN = \"LEFT\"><b>{0}</b></TD>", " Total Execution Time").AppendLine();
            sb.AppendFormat(
                "<TD ALIGN = \"LEFT\" style=\"color:#228B22;\"><b>{0}</b></TD>",
                $"{totalTestRunTime:%d} Days {totalTestRunTime:%h} Hours {totalTestRunTime:%m} Minutes {totalTestRunTime:%s} Seconds"
            ).AppendLine();
            sb.AppendLine("</TR>");

            sb.AppendLine("</table>");
            sb.AppendLine("</BODY>");
            sb.AppendLine("</HTML>");

            return sb.ToString();
        }

        /// <summary>
        /// Send Email with SMTP
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="emailBody"></param>
        /// <param name="fromEmail"></param>
        /// <param name="resultEmailId"></param>
        /// <param name="ccEmail"></param>
        private void SendEmail(string subject, string emailBody, string fromEmail, string resultEmailId, string[] ccEmail, Attachment[] attachments)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress(fromEmail);
            mail.To.Add(new MailAddress(resultEmailId));

            if (ccEmail != null)
                ccEmail.ToList().ForEach(email => mail.CC.Add(new MailAddress(email)));

            mail.Subject = subject;
            mail.Body = emailBody;
            mail.IsBodyHtml = true;

            Logger.LogInfo($"Add attachment: {attachments.Select(attachment => attachment.Name)}");
            if (attachments != null)
                attachments.ToList().ForEach(attachment => mail.Attachments.Add(attachment));

            //double fileLengthinMB = (double)fileInfo.Length / (1024 * 1024);
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("dover.fuelpos.automation@gmail.com", "DoverTest@01");
            smtpServer.EnableSsl = true;

            smtpServer.Send(mail);
        }
    }
}
