﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using Utils.Utils.Drivers;
using Utils.Utils.SettingsInterfaces;
using Utils.Utils.VboxUtils;

namespace Utils.Utils
{
    public class ExtentReportsHelper
    {
        // private static readonly EmailProccesor s_emailProccesor = new EmailProccesor();
        private static readonly ExtentReports s_extentReports = new ExtentReports();

        #region configs

        private static IFilePaths FilePaths => ConfigurationManager.ConfigurationManager.Config.FilePaths;
        private static IReports ReportsConfig => ConfigurationManager.ConfigurationManager.Config.Reports;

        #endregion configs

        private static readonly Stopwatch s_stopWatchTotalTests = new Stopwatch();
        private static readonly Dictionary<string, string> s_testResults = new Dictionary<string, string>();
        private static ExtentHtmlReporter s_extentHtmlReporter;
        private readonly string _path;

        public ExtentReportsHelper()
        {
            _path = $@"{FilePaths.LocalFilesPath}\Reports\{DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss")}";
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);

            // Has to point to a file, even if we do not create that file. It is needed to set the correct directory.
            s_extentHtmlReporter = new ExtentHtmlReporter($"{_path}/report.html");
            s_extentReports.AttachReporter(s_extentHtmlReporter);
            s_stopWatchTotalTests.Start();

            // Add the extentsReportsHelper to the Logger
            Logger.Initialize(this);
        }

        private ExtentTest TestReport { get; set; }

        public void AddReportForCurrentTest() => TestReport = s_extentReports
                .CreateTest(TestContext.CurrentContext.Test.Name)
                .Info(TestContext.CurrentContext.Test.FullName);

        public void CleanUpReports() => new DirectoryInfo($@"{FilePaths.LocalFilesPath}\Reports\")
            .EnumerateDirectories()
            .OrderBy(d => d.CreationTime)
            .Select(d => d)
            .Skip(ReportsConfig.MaxAmountOfReportsToKeep)
            .ToList()
            .ForEach(directory => directory.Delete(true));

        public void CloseReport()
        {
            Console.WriteLine("##teamcity[importData type='nunit' path='results.xml']");
            if (ReportsConfig.TeamCityBuildLogUrl != null)
                Log($@"TeamCity Build Log: <a href=""{ReportsConfig.TeamCityBuildLogUrl}"" target=""_blank"">{ReportsConfig.TeamCityBuildLogUrl}</a>");

            s_stopWatchTotalTests.Stop();
            s_extentReports.Flush();
            s_testResults.Add($"{TestContext.CurrentContext.Test.Name}{s_testResults.Count}", nameof(TestContext.CurrentContext.Result.Outcome.Status));

            Logger.LogInfo("Starting cleanup jobs (for snapshots + reports)");
            VBoxManager.CleanUpSnapshots();
            CleanUpReports();

            Logger.LogInfo("Finished execution");
            // Attach attachments to nunit runner
            Directory
                .EnumerateFiles(_path, "*.html")
                .ToList()
                .ForEach(filePath => TestContext.AddTestAttachment(filePath, $"Reports - {Path.GetFileName(filePath)}"));

            // s_emailProccesor.SendReportsEmail(path, s_testResults, s_stopWatchTotalTests.Elapsed);
        }

        public void Log(string message, Status status = Status.Info, bool addScreenshot = false)
        {
            if (addScreenshot)
            {
                message += $"Screenshot (VboxManage): {TestReport.AddScreenCaptureFromBase64String(DriverConnection.GetVmInUse()?.TakeScreenshotAsBase64())}" +
                $"Screenshot (appium): {TestReport.AddScreenCaptureFromBase64String(ImageHandler.GetScreenshot()?.AsBase64EncodedString)}";
            }

            TestReport.Log(status, message);
        }

        public void LogTestResult()
        {
            string testName = TestContext.CurrentContext.Test.Name;
            switch (TestContext.CurrentContext.Result.Outcome.Status)
            {
                case NUnit.Framework.Interfaces.TestStatus.Skipped:
                    TestReport.Log(Status.Skip, $"{testName} Test Skipped");
                    break;

                case NUnit.Framework.Interfaces.TestStatus.Passed:
                    TestReport.Log(Status.Pass, $"{testName} Test Passed");
                    break;

                case NUnit.Framework.Interfaces.TestStatus.Warning:
                    TestReport.Log(Status.Warning, $"{testName} Test Warning");
                    break;

                case NUnit.Framework.Interfaces.TestStatus.Failed:
                    TestReport.Log(Status.Fail, TestContext.CurrentContext.Result.StackTrace);
                    TestReport.Log(
                        Status.Fail,
                        $"Screenshot (appium):{TestReport.AddScreenCaptureFromBase64String(ImageHandler.GetScreenshot()?.AsBase64EncodedString)}" +
                        $"Screenshot (VboxManage):{TestReport.AddScreenCaptureFromBase64String(DriverConnection.GetVmInUse()?.TakeScreenshotAsBase64())}"
                     );
                    break;
            }
        }
    }
}
