﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using FluentFTP;
using FluentFTP.Rules;
using NUnit.Framework;
using Resources.Constants;
using Utils.Utils.SettingsInterfaces;
using static Utils.Utils.Utils;

namespace Utils.Utils.FtpHelper
{
    public static class FtpHelper
    {
        public static FtpUser s_primaryFtpUser;
        public static FtpUser s_secondaryFtpUser;

        // TODO: move to config
        private static readonly TimeSpan s_defaultFtpTimeout = TimeSpan.FromSeconds(15);

        #region configs

        private static IFilePaths FilePathsConfig => ConfigurationManager.ConfigurationManager.Config.FilePaths;
        private static ISecrets FtpConfig => ConfigurationManager.ConfigurationManager.Config.Secrets;

        #endregion configs

        private static string s_ftpIp;

        public static void CleanupFPDRestartAndFinishedFiles()
        {
            //Clear the existing local RESTART.ASC file
            //Then upload that clean file to overwrite the remote RESTART.ASC -For test stability
            File.WriteAllText(MutationFiles.FTPDownloadsDirectory + Constants.FtpFpdRestartAsc, string.Empty);
            DeleteFile(Constants.FtpFpdRestartAsc, Constants.FtpDirectoryFPD);
            UploadFile(MutationFiles.FTPDownloadsDirectory + Constants.FtpFpdRestartAsc, Constants.FtpDirectoryFPD, s_primaryFtpUser);

            //Clear the existing local RESTART.ASC file
            //Then upload that clean file to overwrite the remote RESTART.ASC -For test stability
            File.WriteAllText(MutationFiles.FTPDownloadsDirectory + Constants.FtpFpdFinishedAsc, string.Empty);
            DeleteFile(Constants.FtpFpdFinishedAsc, Constants.FtpDirectoryFPD);
            UploadFile(MutationFiles.FTPDownloadsDirectory + Constants.FtpFpdFinishedAsc, Constants.FtpDirectoryFPD, s_primaryFtpUser);
        }

        /// <summary>
        /// Deletes a remote file, if it exists, via FTP.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="ftpFilePath"></param>
        public static void DeleteFile(string fileName, string ftpFilePath)
        {
            try
            {
                using (FtpClient ftpClient = GetFtpClient())
                {
                    ftpClient.Connect();
                    ftpClient.SetWorkingDirectory(ftpFilePath);

                    // Deletes the file if it exists, does nothing if it does not exist
                    if (ftpClient.FileExists($"{ftpFilePath}/{fileName}"))
                    {
                        ftpClient.DeleteFile($"{ftpFilePath}/{fileName}");
                        TestContext.Progress.Write($"Succesfully deleted file: {fileName}");
                    }
                    else
                    {
                        TestContext.Progress.Write($"File '{fileName}' to delete, was not found.");
                    }
                }
            }
            catch (WebException e)
            {
                // Prints the FTP exception
                TestContext.Progress.Write("FTP Delete exception: " + e.InnerException);
                throw;
            }
        }

        public static bool DoesFileExist(string filePath)
        {
            using (FtpClient ftpClient = GetFtpClient())
            {
                ftpClient.Connect();
                return ftpClient.FileExists(filePath);
            }
        }

        /// <summary>
        /// Downloads a remote file via FTP to a local download folder
        /// </summary>
        /// <param name="localDownloadFolder"></param>
        /// <param name="ftpDirectory"></param>
        /// <param name="ftpFileName"></param>
        public static void DownloadFile(string localDownloadFolder, string ftpDirectory, string ftpFileName)
        {
            try
            {
                using (FtpClient ftpClient = GetFtpClient())
                {
                    ftpClient.Connect();
                    ftpClient.SetWorkingDirectory(ftpDirectory);

                    // Checks if the file to download exists, if not it will check again every second untill it has reached its the timeout time
                    WaitForFileToExistInRemoteDirectory(ftpDirectory, ftpFileName, 60);

                    // Downloads the remote file via FTP to the local download folder
                    ftpClient.DownloadFile(localDownloadFolder + ftpFileName, $"{ftpDirectory}/{ftpFileName}");
                    TestContext.Progress.Write("Succesfully downloaded the file '" + ftpFileName + "' to directory '" + localDownloadFolder + "'.");
                }
            }
            catch (Exception e)
            {
                // Prints the FTP exception
                TestContext.Progress.Write("FTP Download Exception: " + e.InnerException);
                throw;
            }
        }

        /// <summary>
        /// To check C:/FPD/RESTART.ASC via FTP we need to give our FTP user access to the FPD directory
        /// To do this, we add the line 'C:\FPD\*.* /s' to C:/Pro_bo/text/SYSTEM.DLC & SYSTEM.DEL & SYSTEM.ULC
        /// For this we have to use a different FTP user than the one we are granting the accesses
        /// Hence the need for the Secondary FTP user
        /// </summary>
        public static void GrantFTPAccessToFPDDirectory()
        {
            // Longer timeout is needed for slower machines
            TimeSpan timeout = TimeSpan.FromSeconds(s_defaultFtpTimeout.Seconds * 2);

            UploadFile(MutationFiles.FTPUploadsDirectory + Constants.FtpDeleteAccessSystemAccountFile, Constants.FtpDirectoryProBoText, s_secondaryFtpUser, timeout);
            UploadFile(MutationFiles.FTPUploadsDirectory + Constants.FtpDownloadAccessSystemAccountFile, Constants.FtpDirectoryProBoText, s_secondaryFtpUser, timeout);
            UploadFile(MutationFiles.FTPUploadsDirectory + Constants.FtpUploadAccessSystemAccountFile, Constants.FtpDirectoryProBoText, s_secondaryFtpUser, timeout);
        }

        // Has to be set before this class can be used
        public static void InitializeFtpHelper(string ip)
        {
            s_primaryFtpUser = new FtpUser(FtpConfig.FtpUsername, FtpConfig.FtpPassword);
            s_secondaryFtpUser = new FtpUser(FtpConfig.FtpSecondaryUsername, FtpConfig.FtpSecondaryPassword);
            s_ftpIp = ip;
        }

        public static void RenameFile(string ftpDirectory, string ftpFileNameOriginal, string ftpFileNameNew)
        {
            using (FtpClient ftpClient = GetFtpClient())
            {
                ftpClient.Connect();
                ftpClient.SetWorkingDirectory(ftpDirectory);
                ftpClient.Rename($"{ftpDirectory}/{ftpFileNameOriginal}", ftpFileNameNew);
            }
        }

        //Used to send in all the images in the images folder
        public static void UploadAllFilesInDirectory(string localDirectoryPath, string ftpFileDirectory, FtpUser user, TimeSpan timeout)
        {
            try
            {
                int counter = 0;
                string[] filePaths = Directory.GetFiles(localDirectoryPath, "*.*");
                int numberOfImages = filePaths.Length;
                using (FtpClient ftpClient = GetFtpClient(user.Username, user.Password, timeout))
                {
                    ftpClient.Connect();
                    ftpClient.SetWorkingDirectory(ftpFileDirectory);

                    if (filePaths != null)
                    {
                        Logger.LogInfo($"Uploading all files in directory: '{localDirectoryPath}' to remote directory '{ftpFileDirectory}'");
                        foreach (string file in filePaths)
                        {
                            //Print a status every 50 files
                            if (counter % 50 == 0)
                            {
                                Logger.LogInfo($"Uploaded {counter}/{numberOfImages} files.");
                            }
                            counter++;
                            //Upload the file, if it already exists we skip it
                            ftpClient.UploadFile(file, $"{ftpFileDirectory}/{Path.GetFileName(file)}", FtpRemoteExists.Skip, true, FtpVerify.Retry | FtpVerify.Delete | FtpVerify.Throw);
                        }
                    }
                    Logger.LogInfo($"Succesfuly uploaded all files in directory: '{localDirectoryPath}' to remote directory '{ftpFileDirectory}'", true);
                }
            }
            catch (WebException e)
            {
                Logger.LogWarning($"FTP Upload Exception: {Logger.GetExceptionInfo(e)}");
                throw;
            }
        }

        // Keep all FTP methods private, use wrapper methods
        // It keeps the amount of callable methods of FtpHelper limited. That limitation
        // keeps the class clean for the programmers. It is also safer. Only the requests
        // which are in the defined public methods can be made.
        public static void UploadFile(string localFilePath, string ftpFileDirectory, FtpUser user) => UploadFile(
            localFilePath,
            ftpFileDirectory,
            user,
            s_defaultFtpTimeout
        );

        public static void UploadFile(string localFilePath, string ftpFileDirectory, FtpUser user, TimeSpan timeout)
        {
            if (!File.Exists(localFilePath))
            {
                TestContext.Progress.Write("Upload via FTP failed: Local file to upload was not found at path: " + localFilePath);
                throw new FtpException("Upload via FTP failed: Local file to upload was not found at path: " + localFilePath);
            }

            string fileName = Path.GetFileName(localFilePath);

            try
            {
                using (FtpClient ftpClient = GetFtpClient(user.Username, user.Password, timeout))
                {
                    ftpClient.Connect();
                    ftpClient.SetWorkingDirectory(ftpFileDirectory);

                    // Upload file and ensure the FTP directory is created on the server
                    // verify the file after upload if not valid, retry, still unvalid remove file and throw error
                    ftpClient.UploadFile(localFilePath, $"{ftpFileDirectory}/{fileName}", FtpRemoteExists.Overwrite, true, FtpVerify.Retry | FtpVerify.Delete | FtpVerify.Throw);
                    TestContext.Progress.Write($"Succesfully uploaded file: {fileName}, to remote directory: {ftpFileDirectory}");
                }
            }
            catch (WebException e)
            {
                Logger.LogWarning($"FTP Upload Exception: {Logger.GetExceptionInfo(e)}");
                throw;
            }
        }

        /// <summary>
        /// Uploads a file to the remote directory C:/Pro_bo/EBOC/
        /// </summary>
        /// <param name="file"></param>
        public static void UploadFileToEBOC(string file, FtpUser ftpUser) => UploadFile(file, Constants.FtpDirectoryEBOC, ftpUser);

        // This is a good example of a wrapper method
        public static void UploadPackagesToAcivate(string fuelPosVersion, bool releaseBuild = false)
        {
            string packagesDirectory = $@"{GetFuelPosBuildsPath(releaseBuild)}\{fuelPosVersion}\installable\packages";

            // Create InstallablePackages.xml
            Process process = Process.Start(
                "CMD.exe",
                $@"/c {Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Executables.CreateInstallablePackagesExe)} -t b -p ""{packagesDirectory}"" -o ""{TempDirectoryUtil.s_temp_directory}"""
            );
            process.WaitForExit();
            // Log the creation of the installablepackages.xml
            if (!File.Exists(Path.Combine(TempDirectoryUtil.s_temp_directory, @"sw_activate\InstallablePackages.xml")))
                Logger.LogWarning("Creating InstallablePackages.xml failed");
            else
                Logger.LogInfo("Creating InstallablePackages.xml succeeded");

            // Log that the fiz file uploading started
            Logger.LogInfo("Uploading fiz files through FTP...");

            // Upload packages xml
            UploadFile(
                $@"{TempDirectoryUtil.s_temp_directory}\sw_activate\InstallablePackages.xml",
                Constants.FtpDirectoryActivate,
                s_primaryFtpUser
            );

            // Remove it from our temp directory (C# command does not work, likely because cmd launches as admin or something)
            Process.Start("CMD.exe", $"/c rmdir {Path.GetDirectoryName($@"{TempDirectoryUtil.s_temp_directory}\sw_activate\InstallablePackages.xml")}");

            // Upload packages
            Stopwatch stopWatch = Stopwatch.StartNew();
            using (FtpClient ftpClient = GetFtpClient())
            {
                ftpClient.Connect();

                ftpClient.UploadDirectory(
                    packagesDirectory,
                    Constants.FtpDirectoryActivate,
                    FtpFolderSyncMode.Update,
                    FtpRemoteExists.Overwrite,
                    // verify the file after upload if not valid, retry, still unvalid remove file and throw error
                    FtpVerify.Retry | FtpVerify.Delete | FtpVerify.Throw,
                    new List<FtpRule> {
                        // only allow fiz files
                        new FtpFileExtensionRule(true, new List<string>{ "fiz" })
                    }
                );
            }
            stopWatch.Stop();
            Logger.LogInfo($"Uploading packages took {stopWatch.Elapsed}");
        }

        /// <summary>
        /// Checks if the remote file exists, if not it will check again every second untill it has reached its the timeout time
        /// </summary>
        /// <param name="ftpDirectory"></param>
        /// <param name="ftpFileName"></param>
        /// <param name="maxWaitTime"></param>
        public static void WaitForFileToExistInRemoteDirectory(string ftpDirectory, string ftpFileName, int maxWaitTime)
        {
            try
            {
                using (FtpClient ftpClient = GetFtpClient())
                {
                    ftpClient.Connect();
                    ftpClient.SetWorkingDirectory(ftpDirectory);
                    // Checks if the file to download exists, if not it will try again every second untill it has reached its maxWaitTime
                    WaitUntil(
                        () => ftpClient.FileExists(ftpFileName),
                        TimeSpan.FromSeconds(1),
                        TimeSpan.FromSeconds(maxWaitTime),
                        "Wait for file to exist in remote directory FAILED, file was not found before the maxWaitTime"
                    );
                }
            }
            catch (WebException e)
            {
                // Prints the FTP exception
                TestContext.Progress.Write("FTP 'File Exists' exception: " + e.InnerException);
                throw;
            }
        }

        private static FtpClient GetFtpClient() => GetFtpClient(s_defaultFtpTimeout);

        private static FtpClient GetFtpClient(TimeSpan timeout) => GetFtpClient(FtpConfig.FtpUsername, FtpConfig.FtpPassword, timeout);

        private static FtpClient GetFtpClient(string username, string password, TimeSpan timeout) => new FtpClient(s_ftpIp, username, password) { ConnectTimeout = (int)timeout.TotalMilliseconds, RetryAttempts = 3 };

        private static string GetFuelPosBuildsPath(bool releaseBuilds = false)
        {
            string pathPiece = releaseBuilds ? "RELEASE" : "ReadyForTest";
            return $@"{FilePathsConfig.LocalFilesPath}\{pathPiece}\GroupFuelPos\FuelPos";
        }
    }
}
