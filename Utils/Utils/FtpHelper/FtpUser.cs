﻿namespace Utils.Utils.FtpHelper
{
    public class FtpUser
    {
        public FtpUser(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public string Password { get; }
        public string Username { get; }
    }
}
