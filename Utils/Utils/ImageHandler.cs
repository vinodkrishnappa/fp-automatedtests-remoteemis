﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.ImageComparison;
using OpenQA.Selenium.Appium.Interactions;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using Utils.Components;
using Utils.Components.Windows;
using static Utils.Utils.Utils;
using PointerInputDevice = OpenQA.Selenium.Appium.Interactions.PointerInputDevice;

namespace Utils.Utils
{
    public static class ImageHandler
    {
        private static WindowsDriver<WindowsElement> s_desktopDriver;
        private static int s_resolutionHeight;
        private static int s_resolutionWidth;

        public static bool ResolutionIsSet => s_resolutionHeight != 0;

        /// <summary>
        /// Clicks all images which have the same path as the parameter 'imagePath'
        /// It is wise to surround it with ImageIsFoundOnScreen in practice
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="screen"></param>
        public static void ClickAllImages(string imagePath, IHasParentWindow screen)
        {
            string base64Image = GetBase64ImageByImagePath(imagePath);
            Screenshot screenshot = screen.ParentWindow._appiumDriver.GetScreenshot();
            OccurenceMatchingResult occurencesResult = GetOccurencesResult(screenshot.AsBase64EncodedString, base64Image, screen);

            do
            {
                screenshot = screen.ParentWindow._appiumDriver.GetScreenshot();
                occurencesResult = GetOccurencesResult(screenshot.AsBase64EncodedString, base64Image, screen);
                ClickImageByOccurencesResult(occurencesResult, screen);
            } while (occurencesResult != null);
        }

        /// <summary>
        /// Clicks a specified image on a screen
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="screen"></param>
        public static void ClickByImage(string imagePath, IHasParentWindow screen)
        {
            Logger.LogVerboseInfo($"Attempting to click image '{imagePath}' - ClickByImage(), {nameof(ImageHandler)}");
            string base64Image = GetBase64ImageByImagePath(imagePath);

            // Try multiple times, to prevent false positives
            Retry(
                () =>
                {
                    Screenshot screenshot = GetScreenshot(screen);
                    OccurenceMatchingResult occurencesResult = GetOccurencesResult(screenshot.AsBase64EncodedString, base64Image, screen);

                    if (occurencesResult == null)
                        throw new Exception($"No image match while trying to click image '{imagePath}'");
                    ClickImageByOccurencesResult(occurencesResult, screen);
                },
                TimeSpan.FromSeconds(1),
                3
            );
            Logger.LogVerboseInfo($"Clicked image '{imagePath}' - ClickByImage(), {nameof(ImageHandler)}");
        }

        /// <summary>
        /// Takes a screenshot with the desktop driver, not recommended over the other screenshot method
        /// </summary>
        /// <returns></returns>
        public static Screenshot GetScreenshot()
        {
            try
            {
                return s_desktopDriver.GetScreenshot();
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"ImageHandler - Taking screenshot with appium failed, returning null \n {Logger.GetExceptionInfo(ex)}", true, false);
                return null;
            }
        }

        /// <summary>
        /// Returns true if an specified image is on a specified screen
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static bool ImageIsFoundOnScreen(string imagePath, IHasParentWindow screen)
        {
            string base64Image = GetBase64ImageByImagePath(imagePath);
            Screenshot screenshot = GetScreenshot(screen);
            return GetOccurencesResult(screenshot.AsBase64EncodedString, base64Image, screen) != null;
        }

        public static void SetDesktopDriver(WindowsDriver<WindowsElement> windowsDriver) => s_desktopDriver = windowsDriver;

        public static void SetResolution(Window window)
        {
            Logger.LogInfo("Setting the resolution for the ImageHandler");
            window.WaitUntilExists(TimeSpan.FromSeconds(10));

            // Supported in Appium 1.2.20, 
            // We use 1.2.19 at the moment....
            // https://github.com/appium/appium/issues/16316

            // WindowsElement windowAsElement = window.ReturnWindowAsWindowsElement;
            // s_resolutionHeight = windowAsElement.Size.Height;
            // s_resolutionWidth = windowAsElement.Size.Width;

            // Ugly work around
            // See comments in EfuiWindow & ImageHandler.SetResolution for more info
            using (MemoryStream memoryStream = new MemoryStream(window._appiumDriver.GetScreenshot().AsByteArray))
            {
                Image image = Image.FromStream(memoryStream);
                s_resolutionWidth = image.Width;
                s_resolutionHeight = image.Height;
            }
        }

        /// <summary>
        /// Waits until an image is found on a specified screen
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="timeout"></param>
        /// <param name="screen"></param>
        public static void WaitTillImageIsFound(string imagePath, TimeSpan timeout, IHasParentWindow screen) => WaitForImage(imagePath, timeout, screen);

        /// <summary>
        /// Waits until an image is found on a screen, but also tries to restore the connection if it falls away
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="timesToRetry"></param>
        /// <param name="retryDelay"></param>
        /// <param name="timeout"></param>
        /// <param name="screen"></param>
        public static void WaitTillImageIsFoundRetryWhenConnectionCloses(string imagePath, int timesToRetry, TimeSpan retryDelay, TimeSpan timeout, IHasParentWindow screen) => WaitForImageRetryWhenConnectionCloses(imagePath, timesToRetry, retryDelay, timeout, screen);

        /// <summary>
        /// Waits until an image is no longer present on a screen
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="timeout"></param>
        /// <param name="screen"></param>
        public static void WaitTillImageIsNotFound(string imagePath, TimeSpan timeout, IHasParentWindow screen) => WaitForImage(imagePath, timeout, screen, false);

        private static void ClickImageByOccurencesResult(OccurenceMatchingResult occurencesResult, IHasParentWindow screen)
        {
            PointerInputDevice touchContact = new PointerInputDevice(PointerKind.Touch);

            // The touchaction takes the upper left corner to click on.
            // Image recognition isn't 100% accurate we use below formula to aim for the middle
            ActionSequence touchSequence = new ActionSequence(touchContact, 0)
                .AddAction(
                    touchContact.CreatePointerMove(
                        screen.ParentWindow.ReturnWindowAsWindowsElement,
                        occurencesResult.Rect.X - (s_resolutionWidth / 2) + (occurencesResult.Rect.Width / 2),
                        occurencesResult.Rect.Y - (s_resolutionHeight / 2) + (occurencesResult.Rect.Height / 2),
                        TimeSpan.Zero
                    )
                )
                .AddAction(touchContact.CreatePointerDown(PointerButton.TouchContact))
                .AddAction(touchContact.CreatePointerUp(PointerButton.TouchContact));

            s_desktopDriver.PerformActions(new List<ActionSequence> { touchSequence });
        }

        private static string GetBase64ImageByImagePath(string imagePath)
        {
            string finalPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, imagePath);
            byte[] imageByteArray = File.ReadAllBytes(finalPath);
            return Convert.ToBase64String(imageByteArray);
        }

        private static OccurenceMatchingResult GetOccurencesResult(string screenshotBase64String, string imageBase64String, IHasParentWindow screen)
        {
            OccurenceMatchingResult occurencesResult = null;

            try
            {
                occurencesResult = screen.ParentWindow._appiumDriver.FindImageOccurence(screenshotBase64String, imageBase64String);
            }
            catch (WebDriverException exception) when (WebDriverExceptionFinder.IsImageOccurencesNotFoundInFullImageException(exception))
            {
                Logger.LogVerboseInfo($"GetOccurencesResult threw (safe to ignore) \n {Logger.GetExceptionInfo(exception)}");
            }
            catch (Exception exception)
            {
                // If there is no match is found an exception is thrown
                // Instead of throwing it, catch it, return null, and write the exception to the log
                Logger.LogWarning($"ImageHandler GetOccurencesResult threw exception. \n Set occurence result to 'null' \n {Logger.GetExceptionInfo(exception)}");
            }
            return occurencesResult;
        }

        private static Screenshot GetScreenshot(IHasParentWindow screen) => screen.ParentWindow._appiumDriver.GetScreenshot();

        private static bool IsImageOnScreen(string imagePath, IHasParentWindow screen)
        {
            string screenShotAsBase64 = GetScreenshot(screen).AsBase64EncodedString;
            string base64Image = GetBase64ImageByImagePath(imagePath);
            return GetOccurencesResult(screenShotAsBase64, base64Image, screen) != null;
        }

        private static void WaitForImage(string imagePath, TimeSpan timeout, IHasParentWindow screen, bool waitTillFound = true)
        {
            TimeSpan frequency = TimeSpan.FromSeconds(1);
            Func<bool> condition;
            string message;

            if (waitTillFound)
            {
                condition = () => IsImageOnScreen(imagePath, screen);
                message = "Waiting till image visible";
            }
            else
            {
                condition = () => !IsImageOnScreen(imagePath, screen);
                message = "Waiting till image not visible";
            }

            Stopwatch stopWatch = Stopwatch.StartNew();
            WaitUntil(
                () => condition(),
                frequency,
                timeout,
                $"{message} '{imagePath}' didn't complete within timespan"
            );
            stopWatch.Stop();
            Logger.LogInfo($"{message} '{imagePath}' took {stopWatch.Elapsed}");
        }

        private static void WaitForImageRetryWhenConnectionCloses(string imagePath, int timesToRetry, TimeSpan retryDelay, TimeSpan timeout, IHasParentWindow screen, bool waitTillFound = true)
        {
            TimeSpan frequency = TimeSpan.FromSeconds(1);
            Func<bool> condition;
            string message;

            if (waitTillFound)
            {
                condition = () => IsImageOnScreen(imagePath, screen);
                message = "Waiting till image visible";
            }
            else
            {
                condition = () => !IsImageOnScreen(imagePath, screen);
                message = "Waiting till image not visible";
            }

            Stopwatch stopWatch = Stopwatch.StartNew();
            RetryOnException<WebDriverException>(
                timesToRetry,
                retryDelay,
                () => WaitUntilAsync(
                    () => condition(),
                    frequency,
                    timeout,
                    $"{message} '{imagePath}' didn't complete within timespan"
                )
            );
            stopWatch.Stop();
            Logger.LogInfo($"{message} '{imagePath}' took {stopWatch.Elapsed} seconds.");
        }
    }
}
