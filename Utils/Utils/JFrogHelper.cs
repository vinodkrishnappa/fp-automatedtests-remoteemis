﻿using System;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;

using Resources.Constants;

using Utils.Utils.SettingsInterfaces;

namespace Utils.Utils
{
    public static class JFrogHelper
    {
        private static IFilePaths FilePathsConfig => ConfigurationManager.ConfigurationManager.Config.FilePaths;

        // A jfrog config file with the dfs connection is required for this helper!
        // (~/.jfrog/jfrog-cli.conf.v)

        public static void DownloadeRemoteEmisPackages(string fuelPosVersion, bool releaseBuild = false)
        {
            string DefaultpackagesDirectory = $@"{GetFuelPosBuildsPath(releaseBuild)}\{fuelPosVersion}\installable\eMisInstaller\Default";
            string IntermarchpackagesDirectory = $@"{GetFuelPosBuildsPath(releaseBuild)}\{fuelPosVersion}\installable\eMisInstaller\France-Intermarche";
            string jFrogFuelPosBuildsPath = GetJFrogFuelPosBuildsPath(releaseBuild);

            // Verify of packages directory exists
            string output = RunJFrog(
                $@"rt s --include-dirs --count ""{jFrogFuelPosBuildsPath}/{fuelPosVersion}""", "1"
            );

            if (output == "0")
                throw new DirectoryNotFoundException($"404 '{jFrogFuelPosBuildsPath}/{fuelPosVersion}' not found in jFrog. \n {output}");
            else if (output != "1")
                throw new Exception($"Error while checking of '{jFrogFuelPosBuildsPath}/{fuelPosVersion}' exists in jFrog. \n {output}");

            // Create local packages directory if needed
            if (!Directory.Exists(DefaultpackagesDirectory))
            {
                Directory.CreateDirectory(DefaultpackagesDirectory);
                Directory.CreateDirectory(IntermarchpackagesDirectory);
                // Download packages
                Stopwatch stopWatch = Stopwatch.StartNew();
                TestContext.Progress.WriteLine($"Downloading '{jFrogFuelPosBuildsPath}/{fuelPosVersion}' packages through jFrog to local directory: "
                    + DefaultpackagesDirectory);
                RunJFrog(
                    $@"rt dl --threads=20 --include-dirs=true --flat ""{jFrogFuelPosBuildsPath}/{fuelPosVersion}/installable/eMisInstaller/Default/*"" {DefaultpackagesDirectory}\"
                );
                RunJFrog(
                   $@"rt dl --threads=20 --include-dirs=true --flat ""{jFrogFuelPosBuildsPath}/{fuelPosVersion}/installable/eMisInstaller/France-Intermarche/*"" {IntermarchpackagesDirectory}\"
               );
                stopWatch.Stop();
                TestContext.Progress.WriteLine($"Downloading jFrog packages took {stopWatch.Elapsed}");
            }
            else
            {
                TestContext.Progress.Write("Package Directory already exists under: " + DefaultpackagesDirectory + " - No download from JFrog is required.");
            }
        }

        public static void DownloadFuelPosPackages(string fuelPosVersion, bool releaseBuild = false)
        {
            string packagesDirectory = $@"{GetFuelPosBuildsPath(releaseBuild)}\{fuelPosVersion}\installable\packages";
            string jFrogFuelPosBuildsPath = GetJFrogFuelPosBuildsPath(releaseBuild);

            // Verify of packages directory exists
            string output = RunJFrog(
                $@"rt s --include-dirs --count ""{jFrogFuelPosBuildsPath}/{fuelPosVersion}""", "1"
            );

            if (output == "0")
                throw new DirectoryNotFoundException($"404 '{jFrogFuelPosBuildsPath}/{fuelPosVersion}' not found in jFrog. \n {output}");
            else if (output != "1")
                throw new Exception($"Error while checking of '{jFrogFuelPosBuildsPath}/{fuelPosVersion}' exists in jFrog. \n {output}");

            // Create local packages directory if needed
            if (!Directory.Exists(packagesDirectory))
            {
                Directory.CreateDirectory(packagesDirectory);

                // Download packages
                Stopwatch stopWatch = Stopwatch.StartNew();
                Logger.LogInfo($"Downloading '{jFrogFuelPosBuildsPath}/{fuelPosVersion}' packages through jFrog to local directory: {packagesDirectory}");
                RunJFrog(
                    $@"rt dl --threads=20 --flat ""{jFrogFuelPosBuildsPath}/{fuelPosVersion}/installable/packages/*.fiz"" {packagesDirectory}\"
                );
                stopWatch.Stop();
                Logger.LogInfo($"Downloading jFrog packages took {stopWatch.Elapsed}");
            }
            else
            {
                TestContext.Progress.Write("Package Directory already exists under: " + packagesDirectory + " - No download from JFrog is required.");
            }
        }

        public static string GetLatestBuildNumberFromJFrog(int majorVersionToSearchFor, int minorVersionToSearchFor, bool releaseBuild)
        {
            string jFrogFuelPosBuildsPath = GetJFrogFuelPosBuildsPath(releaseBuild);
            string latestVersion = null;
            string minorVersionToSearchForString = minorVersionToSearchFor.ToString();
            if (minorVersionToSearchFor.ToString().Length == 1)
                minorVersionToSearchForString = $"0{minorVersionToSearchFor}";

            string jFrogOutput = RunJFrog(
                $@"rt s --include-dirs --limit=19 --recursive=false --count ""{jFrogFuelPosBuildsPath}/{majorVersionToSearchFor}.{minorVersionToSearchForString}*""", "1"
            );

            int.TryParse(jFrogOutput, out int totalDirectories);

            //Counts all build directories for this major and minor version and subtracts 1 to be used as the offset for the next command.
            int totalDirectoriesMinusOne = totalDirectories - 1;

            //If totalDirectoriesMinusOne equals -1 this means there were no directories found for this search command.
            Assert.That(totalDirectoriesMinusOne != -1, $"No build directory was found under path: {jFrogFuelPosBuildsPath}/{majorVersionToSearchFor}.{minorVersionToSearchForString}*");

            //Gets only the last build for this major and minor version using the offset.
            string latestBuildDirectory = RunJFrog(
                $@"rt s --include-dirs --limit=19 --recursive=false --offset={totalDirectoriesMinusOne} ""{jFrogFuelPosBuildsPath}/{majorVersionToSearchFor}.{minorVersionToSearchForString}*""", "1"
            );

            //The latestBuildDirectory contains 4 keys and values: path, type, created & modified, the version can be found in the path value
            //We extract this using the following
            latestVersion = latestBuildDirectory.Split(new[] { "\"," }, StringSplitOptions.None)[0];
            latestVersion = latestVersion.Split(new[] { jFrogFuelPosBuildsPath + "/" }, StringSplitOptions.None)[1];

            Logger.LogInfo($"jFrog found latest version '{latestVersion}' for FuelPOS with \n - Major Version: {majorVersionToSearchFor} \n - Minor Version: {minorVersionToSearchForString}");

            return latestVersion;
        }

        private static string GetBuildTypeDirectory(bool isReleaseBuild = false) => isReleaseBuild ? "RELEASE" : "ReadyForTest";

        private static string GetFuelPosBuildsPath(bool isReleaseBuild = false) => $@"{FilePathsConfig.LocalFilesPath}\{GetBuildTypeDirectory(isReleaseBuild)}\GroupFuelPos\FuelPos";

        private static string GetJFrogFuelPosBuildsPath(bool isReleaseBuild = false) => $"DFS_TUR_ARTIFACTS/{GetBuildTypeDirectory(isReleaseBuild)}/GroupFuelPos/FuelPos";

        private static string RunJFrog(string args, string attendedOutput = "")
        {
            ProcessStartInfo jFrogStartInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Executables.JFrogExe),
                Arguments = args,
                UseShellExecute = false,
                // We redirect output only if the user supply any attended output
                RedirectStandardOutput = attendedOutput != ""
            };

            jFrogStartInfo.EnvironmentVariables["JFROG_CLI_HOME_DIR"] = @"C:\jfrog\.jfrog";
            jFrogStartInfo.EnvironmentVariables["CI"] = true.ToString();

            try
            {
                Process jFrogProcess = Process.Start(jFrogStartInfo);
                jFrogProcess.WaitForExit();
                // If we are waiting for an output, we wait until the end of the output
                return attendedOutput != "" ? jFrogProcess.StandardOutput.ReadToEnd().Trim() : "-1";
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
