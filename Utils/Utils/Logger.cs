﻿using System;
using AventStack.ExtentReports;
using NUnit.Framework;

namespace Utils.Utils
{
    public static class Logger
    {
        private static ExtentReportsHelper s_reportsHelper;
        private static bool LogVerbose => ConfigurationManager.ConfigurationManager.Config.DebugOptions.LogVerbose;

        public static string GetExceptionInfo(Exception exception) => $"[{exception.GetType()}]: { exception.Message} \n { exception.StackTrace }";

        public static void Initialize(ExtentReportsHelper extentReportsHelper) => s_reportsHelper = extentReportsHelper;

        public static void LogInfo(string message, bool addToReport = false)
        {
            TestContext.Progress.WriteLine(message);
            if (addToReport && s_reportsHelper != null)
                s_reportsHelper.Log(message);
        }

        public static void LogInfoHeaderSection(string message, char headerChar = '-', bool addToReport = false)
        {
            string headerLine = new string(headerChar, message.Length);
            message = $"{headerLine}\n{message}\n{headerLine}";
            LogInfo(message, addToReport);
        }

        public static void LogVerboseInfo(string message, bool addToReport = false)
        {
            if (LogVerbose)
                LogInfo(message, addToReport);
        }

        public static void LogWarning(string message, bool addToReport = true, bool addScreenshotInReport = true)
        {
            Logger.LogInfo($"[WARNING]: {message}");
            if (addToReport && s_reportsHelper != null)
                s_reportsHelper.Log(message, Status.Warning, addScreenshotInReport);
        }
    }
}
