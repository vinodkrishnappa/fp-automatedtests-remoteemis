﻿using NUnit.Framework;
using System;
using System.IO;
using System.Linq;

namespace Utils
{
    public static class ResultFileParser
    {
        //Checks the RES file for errors, per block and in the final [INFO] block
        public static void ParseResultFileForErrors(string filePath)
        {
            bool errorsFound = false;
            StreamReader reader = File.OpenText(filePath);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Contains("="))
                {
                    //If a line contains either 'NO' or 'ERRT' it means something went wrong with the processing of that line.
                    //It prints these errors for debugging purposes and sets the bool to true to fail the testcase.
                    if (line.Split('=')[1] == "NO" || line.Split('=')[0].Contains("ERRT"))
                    {
                        TestContext.Progress.Write("Error: " + line);
                        errorsFound = true;
                    }
                    //If the [INFO] block does not say 'RESULT=Ok', there is a generic error (Incorrect blockname, network, etc.).
                    //It prints these errors for debugging purposes and sets the bool to true to fail the testcase.
                    else if (line.Contains("RESULT=") && !line.Contains("=Ok"))
                    {
                        TestContext.Progress.Write("A generic error was found in the 'INFO' block: " + line);
                        errorsFound = true;
                    }
                }
            }
            if (errorsFound)
            {
                TestContext.Progress.Write("Errors where found in the result file.");
                throw new Exception("Errors where found in result file :" + filePath + ".");
            }

            TestContext.Progress.Write("No errors where found in the result file.");
        }

        public static void PrintStringArray(string[] stringArrayToPrint) => stringArrayToPrint
            .Select((v, i) => new { Index = i, Value = v })
            .ToList()
            .ForEach(element => TestContext.WriteLine($"Line {element.Index}: {element.Value}"));
    }
}
