﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.UI;

namespace Utils.Utils
{
    /// <summary>
    /// These methods are implemented on everything that extends (or is) a BaseScreen or a BaseComponent
    /// </summary>
    public class ScreenHelpers
    {
        /// <summary>
        /// The AppiumDriver field should be overwritten by the class which implements this
        /// </summary>
        public virtual AppiumDriver<WindowsElement> AppiumDriver { get; }

        /// <summary>
        /// Used mostly to click the Save button in emis configuration
        /// This was giving alot of problems in the Clean Install Tests.
        /// </summary>
        /// <param name="elementToClick"></param>
        /// <param name="maxRetrys"></param>
        public void ClickUntillDisabledWithRetry(WindowsElement elementToClick, int maxRetrys)
        {
            int counter = 1;

            while (!elementToClick.Enabled && counter < maxRetrys)
            {
                Logger.LogInfo($"ClickWithRetry: Button was not yet enabled when we wanted to click it, retrying every second: {counter}/{maxRetrys}");
                Thread.Sleep(1000);
                counter++;
            }

            Assert.That(elementToClick.Enabled, $"Unable to click button at this point, retried {maxRetrys} times.");
            elementToClick.Click();
            Thread.Sleep(1000);

            counter = 1;
            while (elementToClick.Enabled && counter < maxRetrys)
            {
                Logger.LogInfo($"ClickWithRetry: Button was still enabled after clicking it once, retrying every second for: {counter}/{maxRetrys}");
                elementToClick.Click();
                Thread.Sleep(1000);
                counter++;
            }

            Assert.That(!elementToClick.Enabled, $"Button was not disabled after click, click was retried {maxRetrys} times.");
        }

        /// <summary>
        /// Used to click buttons in pop-ups, sometimes one click was not enough and the pop-up
        /// would block the screen.
        /// </summary>
        /// <param name="getElementToClick"></param>
        /// <param name="maxRetrys"></param>
        public void ClickUntillElementNoLongerExists(Func<AppiumWebElement> getElementToClick, int maxRetrys)
        {
            int counter = 1;

            while (!DoesElementExist(getElementToClick) && counter < maxRetrys)
            {
                Logger.LogInfo($"ClickUntillElementNoLongerExists: Button did not yet exist when we wanted to click it, retrying every second: {counter}/{maxRetrys}");
                Thread.Sleep(1000);
                counter++;
            }

            Assert.That(getElementToClick().Enabled, $"Unable to click button at this point, retried {maxRetrys} times.");
            getElementToClick().Click();
            Thread.Sleep(1000);

            counter = 1;
            while (DoesElementExist(getElementToClick) && counter < maxRetrys)
            {
                Logger.LogInfo($"ClickUntillElementNoLongerExists: Element was clicked but it still exists, waiting for it to dissapear, retrying every second: {counter}/{maxRetrys}");
                getElementToClick().Click();
                Thread.Sleep(1000);
                counter++;
            }

            Assert.That(!DoesElementExist(getElementToClick), $"Element did not dissapear after clicking it {maxRetrys} times.");
        }

        public bool DoesElementExist(Func<AppiumWebElement> getAppiumElement)
        {
            try
            { return getAppiumElement() != null; }
            catch (StaleElementReferenceException) { return false; }
            catch (NoSuchElementException) { return false; }
            catch (WebDriverException e)
            {
                TestContext.Error.WriteLine($"Warning DoesElementExist threw {e}");
                return false;
            }
        }

        public void VerifyIfElementContainsSpecifiedText(WindowsElement element, string text) => Assert.AreEqual(element.Text, text, $"{GetElementDescription(element)} does not contain expected text '{text}'");

        public void VerifyIfElementIsVisible(WindowsElement element) => Assert.That(element.Displayed, Is.True, $"{GetElementDescription(element)} is not visible");

        public void VerifyIfElementIsVisibleAndEnabled(WindowsElement element)
        {
            VerifyIfElementIsVisible(element);
            Assert.That(element.Enabled, Is.True, $"{GetElementDescription(element)} is not enabled");
        }

        public void WaitUntilElementEnabled(
           Func<WindowsElement> findElementFunction,
           string elementName,
           TimeSpan timeout
       )
        {
            Stopwatch stopWatch = Stopwatch.StartNew();
            WebDriverWait(
              (_) => findElementFunction().Enabled,
              AppiumDriver,
              timeout,
              $"{elementName} isn't enabled within timespan",
              new Type[] { typeof(NoSuchElementException), typeof(StaleElementReferenceException), typeof(WebDriverException) }
            );
            stopWatch.Stop();
            Logger.LogInfo($"Waiting until element '{elementName}' enabled took {stopWatch.Elapsed}");
        }

        public void WaitUntilElementExists(
            Func<WindowsElement> findElementFunction,
            string elementName,
            TimeSpan timeout
        )
        {
            Stopwatch stopWatch = Stopwatch.StartNew();
            WebDriverWait(
              (_) => findElementFunction().Displayed,
              AppiumDriver,
              timeout,
              $"{elementName} doesn't exists within timespan",
              new Type[] { typeof(NoSuchElementException), typeof(StaleElementReferenceException), typeof(WebDriverException) }
            );
            stopWatch.Stop();
            Logger.LogInfo($"Waiting until element '{elementName}' exists took {stopWatch.Elapsed}");
        }

        private string GetElementDescription(WindowsElement element) => $"{element.TagName}(text '{element.Text}')";

        private void WebDriverWait(
            Func<IWebDriver, bool> condition,
            AppiumDriver<WindowsElement> appiumDriver,
            TimeSpan timeout,
            string timeOutExpectionMessage = "",
            params Type[] exceptionTypes
        )
        {
            try
            {
                new WebDriverWait(appiumDriver, timeout).Until(_ =>
                {
                    try
                    { return condition(appiumDriver); }
                    catch (Exception e) when (exceptionTypes.Contains(e.GetType()))
                    {
                        // Ignored
                        // This is needed because the developers of Selenium do not want to properly implement this
                        // In default Selenium the expection gets thrown after wait because of the incorrect illogical implementation

                        // Read the comments of the following stack overflow answers for more info:
                        // https://stackoverflow.com/questions/43103129/ignoring-exceptions-when-using-c-sharp-selenium-webdriverwait-wait-untill-func
                        return false;
                    }
                });
            }
            catch (WebDriverTimeoutException e)
            {
                throw new TimeoutException(
                    timeOutExpectionMessage + $"\n Timeout period: {timeout.TotalSeconds} seconds",
                    e.InnerException
                );
            }
        }
    }
}
