﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class CarWashTypes
    {
        // Define your values here
        public static readonly CarWashTypes s_none = new CarWashTypes("None");
        public static readonly CarWashTypes s_generic = new CarWashTypes("Generic");
        public static readonly CarWashTypes s_codax = new CarWashTypes("Codax");
        public static readonly CarWashTypes s_without_interface = new CarWashTypes("Without interface");
        public static readonly CarWashTypes s_extended_generic_version = new CarWashTypes("Extended generic version");
        public static readonly CarWashTypes s_self_carwash = new CarWashTypes("Self carwash");

        // Enum logic
        private CarWashTypes(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(CarWashTypes carWashTypes) => carWashTypes.Value;

        internal static CarWashTypes Parse(string enumKey) => typeof(CarWashTypes).GetField($"s_{enumKey.ToLower()}").GetValue(null) as CarWashTypes;
    }
}
