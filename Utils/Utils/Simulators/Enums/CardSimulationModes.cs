﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class PinPadCardSimulationModes
    {
        // Define your values here
        public static readonly PinPadCardSimulationModes s_magstripe = new PinPadCardSimulationModes("Magstripe");

        public static readonly PinPadCardSimulationModes s_none = new PinPadCardSimulationModes("None");

        // Enum logic
        private PinPadCardSimulationModes(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(PinPadCardSimulationModes cardSimulationMode) => cardSimulationMode.Value;

        internal static PinPadCardSimulationModes Parse(string enumKey) => typeof(PinPadCardSimulationModes).GetField($"s_{enumKey.ToLower()}").GetValue(null) as PinPadCardSimulationModes ?? s_none;
    }
}
