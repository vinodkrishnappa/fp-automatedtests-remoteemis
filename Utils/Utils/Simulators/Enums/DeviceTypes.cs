﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class Devices
    {
        // Define your values here
        public static readonly Devices s_receipt_printer = new Devices("Receipt printer");
        public static readonly Devices s_back_office_computer = new Devices("Back Office Computer");
        public static readonly Devices s_dispensing_port_without_number = new Devices("Dispensing port ");

        // Enum logic
        private Devices(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(Devices devices) => devices.Value;

        internal static Devices Parse(string enumKey) => typeof(Devices).GetField($"s_{enumKey.ToLower()}").GetValue(null) as Devices;
    }
}
