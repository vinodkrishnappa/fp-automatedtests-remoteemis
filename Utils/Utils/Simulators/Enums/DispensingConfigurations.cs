﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class DispensingConfigurations
    {
        //PC:
        public static readonly DispensingConfigurations s_pc_pos_1 = new DispensingConfigurations("POS 1");

        //Type:
        public static readonly DispensingConfigurations s_type_lon = new DispensingConfigurations("LON");
        public static readonly DispensingConfigurations s_type_mux = new DispensingConfigurations("MUX");

        //Subtype:
        public static readonly DispensingConfigurations s_subtype_default = new DispensingConfigurations("Default");

        //Port:
        public static readonly DispensingConfigurations s_port_multiport_1 = new DispensingConfigurations("Multi-port 1");
        public static readonly DispensingConfigurations s_port_multiport_2 = new DispensingConfigurations("Multi-port 2");
        public static readonly DispensingConfigurations s_port_multiport_3 = new DispensingConfigurations("Multi-port 3");
        public static readonly DispensingConfigurations s_port_multiport_4 = new DispensingConfigurations("Multi-port 4");

        // Enum logic
        private DispensingConfigurations(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(DispensingConfigurations dispensingConfigurations) => dispensingConfigurations.Value;

        internal static DispensingConfigurations Parse(string enumKey) => typeof(DispensingConfigurations).GetField($"s_{enumKey.ToLower()}").GetValue(null) as DispensingConfigurations;
    }
}
