﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class FillingTypes
    {
        // Define your values here
        public static readonly FillingTypes s_super_with_index = new FillingTypes("1 - Super");
        public static readonly FillingTypes s_super_plus_with_index = new FillingTypes("2 - Super plus");
        public static readonly FillingTypes s_diesel_with_index = new FillingTypes("3 - Diesel");
        public static readonly FillingTypes s_lpg_with_index = new FillingTypes("4 - L.P.G.");
        public static readonly FillingTypes s_eurosuper_with_index = new FillingTypes("5 - Eurosuper");

        public static readonly FillingTypes s_super = new FillingTypes("Super");
        public static readonly FillingTypes s_super_plus = new FillingTypes("Super plus");
        public static readonly FillingTypes s_diesel = new FillingTypes("Diesel");
        public static readonly FillingTypes s_lpg = new FillingTypes("L.P.G.");
        public static readonly FillingTypes s_eurosuper = new FillingTypes("Eurosuper");

        // Enum logic
        private FillingTypes(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(FillingTypes fillingType) => fillingType.Value;

        internal static FillingTypes Parse(string enumKey) => typeof(FillingTypes).GetField($"s_{enumKey.ToLower()}").GetValue(null) as FillingTypes ?? FillingTypes.s_super;
    }
}
