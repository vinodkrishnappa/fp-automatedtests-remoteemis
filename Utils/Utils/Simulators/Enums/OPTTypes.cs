﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class OPTTypes
    {
        // Define your values here
        public static readonly OPTTypes s_crypto_vga_opt = new OPTTypes("Crypto VGA OPT");
        public static readonly OPTTypes s_crypto_vga_dit = new OPTTypes("Crypto VGA DIT");
        public static readonly OPTTypes s_iq_dit         = new OPTTypes("IQ DIT");
        public static readonly OPTTypes s_iq_opt         = new OPTTypes("IQ OPT");
        public static readonly OPTTypes s_crypto_iq_opt  = new OPTTypes("Crypto IQ OPT");
        public static readonly OPTTypes s_crypto_iq_dit  = new OPTTypes("Crypto IQ DIT");

        // Enum logic
        private OPTTypes(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(OPTTypes terminalType) => terminalType.Value;

        internal static OPTTypes Parse(string enumKey) => typeof(OPTTypes).GetField(enumKey).GetValue(null) as OPTTypes;
    }
}
