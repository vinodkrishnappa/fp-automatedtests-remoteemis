﻿namespace Utils.Utils.Enums
{
    // Acts like an Enum, but for strings
    public sealed class PinPadCards
    {
        // Define your values here
        public static readonly PinPadCards s_mastercard = new PinPadCards("Generic Mastercard (Magstripe, EMV, Ctls).XML");

        public static readonly PinPadCards s_none = new PinPadCards("None");

        // Enum logic
        private PinPadCards(string value) => Value = value;

        public string Value { get; }

        public static implicit operator string(PinPadCards pinPadCard) => pinPadCard.Value;

        internal static PinPadCards Parse(string enumKey) => typeof(PinPadCards).GetField($"s_{enumKey.ToLower()}").GetValue(null) as PinPadCards ?? s_none;
    }
}
