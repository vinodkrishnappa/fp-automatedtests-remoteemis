﻿using System.Collections.Generic;

namespace Utils.Utils
{
    public static class TeamCityArrayBuilder
    {
        private static readonly Dictionary<string, TeamCityDisposableArrayBuilder> s_teamCityArrayBuilders = new Dictionary<string, TeamCityDisposableArrayBuilder>();

        public static void AddValueToArray(string arrayVariableName, string value)
        {
            if (!s_teamCityArrayBuilders.ContainsKey(arrayVariableName) || s_teamCityArrayBuilders[arrayVariableName].IsDisposed)
                s_teamCityArrayBuilders.Add(arrayVariableName, new TeamCityDisposableArrayBuilder(arrayVariableName));

            s_teamCityArrayBuilders[arrayVariableName].AddToTeamCityArray(value);
        }

        public static void BuildArray(string arrayVariableName)
        {
            if (!s_teamCityArrayBuilders.ContainsKey(arrayVariableName))
            {
                Logger.LogWarning($"User tried to build with teamCityArrayBuilder that does not exists '{arrayVariableName}', canceled build");
                return;
            }

            TeamCityDisposableArrayBuilder builder = s_teamCityArrayBuilders[arrayVariableName];
            s_teamCityArrayBuilders.Remove(arrayVariableName);
            builder.BuildTeamCityArray();
            builder.Dispose();
        }
    }
}
