﻿using System;
using System.Linq;

namespace Utils.Utils
{
    public sealed class TeamCityCommand
    {
        private TeamCityCommand(string value) => Command = value;

        public string Command { get; }

        public static TeamCityCommand SetArrayEnvironmentVariableCommand(string variableName, string[] values) => new TeamCityCommand($"##teamcity[setParameter name='env.{variableName}' value='[{string.Join(", ", values.Select(value => $"|'{value}|'"))}|]']");

        public static TeamCityCommand SetEnvironmentVariableCommand(string variableName, string value) => new TeamCityCommand($"##teamcity[setParameter name='env.{variableName}' value='{value}']");

        public void Run()
        {
            Logger.LogVerboseInfo($"Run TeamCity Command '{Command}'");
            Console.WriteLine(Command);
        }
    }
}
