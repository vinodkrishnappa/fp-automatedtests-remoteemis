﻿using System;
using System.Collections.Generic;

namespace Utils.Utils
{
    internal class TeamCityDisposableArrayBuilder : IDisposable
    {
        private readonly List<string> _teamCityArrayValues = new List<string>();

        public TeamCityDisposableArrayBuilder(string arrayVariableName) => VariableName = arrayVariableName;

        public bool IsDisposed { get; private set; } = false;
        public string VariableName { get; }

        public void AddToTeamCityArray(string value) => _teamCityArrayValues.Add(value);

        public void BuildTeamCityArray() => TeamCityCommand.SetArrayEnvironmentVariableCommand(VariableName, _teamCityArrayValues.ToArray()).Run();

        public void Dispose()
        {
            IsDisposed = true;
            GC.SuppressFinalize(this);
        }
    }
}
