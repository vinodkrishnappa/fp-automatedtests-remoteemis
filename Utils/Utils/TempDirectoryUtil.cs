﻿using System.IO;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Utils
{
    public static class TempDirectoryUtil
    {
        // Location of the temp directory
        public static readonly string s_temp_directory;
        private static IDebugOptions DebugOptions => ConfigurationManager.ConfigurationManager.Config.DebugOptions;
        private static IFilePaths FilePathsConfig => ConfigurationManager.ConfigurationManager.Config.FilePaths;

        // Needed for a destructor, normal destructors do not exist for static classes, but this is a way around that
        #pragma warning disable RCS1213 // Remove unused member declaration.
        private static readonly Destructor s_finalise = new Destructor();
        #pragma warning restore RCS1213 // Remove unused member declaration.

        static TempDirectoryUtil()
        {
            // Create a temp directory
            string newTempDirectoryPath;

            do
            {
                string tempDirectoryPath = FilePathsConfig.TempDirectoryPath ?? Path.GetTempPath();
                newTempDirectoryPath = Path.Combine(tempDirectoryPath, Path.GetRandomFileName());
            } while (File.Exists(newTempDirectoryPath));

            Directory.CreateDirectory(newTempDirectoryPath);
            s_temp_directory = newTempDirectoryPath;
        }

        // Destructor
        private sealed class Destructor
        {
            // Remove the temp files
            ~Destructor()
            {
                if (DebugOptions.RemoveTempDirectory)
                    Directory.Delete(s_temp_directory, true);
            }
        }
    }
}
