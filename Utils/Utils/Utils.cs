﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.UI;
using Utils.Utils.Drivers;

namespace Utils.Utils
{
    public static class Utils
    {
        #region Retry function

        public static T Retry<T>(Func<T> action, TimeSpan retryInterval, int maxAttemptCount = 3, Action doOnFail = null)
        {
            List<Exception> exceptions = new List<Exception>();

            for (int attempted = 0; attempted < maxAttemptCount; attempted++)
            {
                try
                {
                    if (attempted > 0)
                        Thread.Sleep(retryInterval);
                    return action();
                }
                catch (Exception ex) { exceptions.Add(ex); }
            }
            if (doOnFail != null)
            {
                doOnFail();
                return default;
            }

            // Remove exceptions which have the same message
            exceptions = exceptions.GroupBy(exception => exception.Message).Select(x => x.First()).ToList();
            // Throw single exception if all messages were the same
            if (exceptions.Count == 1)
                throw exceptions[0];
            // Throw all exceptions if there are different messages
            throw new AggregateException($"{exceptions} \n {exceptions.Last().Message} \n {exceptions.Last().StackTrace}");
        }

        public static void Retry(Action action, TimeSpan retryInterval, int maxAttemptCount = 3, Action doOnFail = null) => Retry<object>(() => { action(); return null; }, retryInterval, maxAttemptCount, doOnFail);

        #endregion Retry function

        public static string GetDateLocalTime(string dateTimeFormat) => DateTime.Now.ToString(dateTimeFormat);

        /// <summary>
        /// Kills a specific process - Used to test if FuelPos recovers correctly from this
        /// </summary>
        /// <param name="processToKillName"></param>
        public static void KillProcess(string processToKillName)
        {
            RunPowerShellCommand($"Stop-Process -name {processToKillName} -Force", TimeSpan.FromSeconds(10));
            Logger.LogInfo($"Killed Process: '{processToKillName}' on purpose.");
        }

        public static void RetryOnException<TException>(int times, TimeSpan delay, Func<Task> operation, bool retryOnTimeout = true) where TException : Exception => 
            RetryOnExceptionAsync<TException>(times, delay, operation, retryOnTimeout)
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();

        public static void RunPowerShellCommand(string command, TimeSpan timeOut)
        {
            AppiumOptions capabilitiesSCO = new AppiumOptions();
            capabilitiesSCO.AddAdditionalCapability("app", "powershell.exe");
            // Do not hide the powershell windows with ($"-windowstyle hidden {command}");
            // It has to show up so that we can wait until the command ends
            capabilitiesSCO.AddAdditionalCapability("appArguments", $"-NoProfile -ExecutionPolicy unrestricted -Command {command}");
            capabilitiesSCO.AddAdditionalCapability(MobileCapabilityType.DeviceName, "WindowsPC");

            using (WindowsDriver<WindowsElement> driver = DriverConnection.CreateDriverWithCapabilities(capabilitiesSCO))
            {
                // Will always fail, but it launches powershell.exe
                try
                {
                    new WebDriverWait(driver, timeOut).Until(webDriver => webDriver == null);
                }
                // ignored
                catch (Exception) { Logger.LogInfo("Launched powershell command"); }
            }
        }

        public static string RunPowerShellCommandOnHost(string command, string attendedOutput = "")
        {
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = "powershell.exe",
                Arguments = $"-NoProfile -ExecutionPolicy unrestricted -Command {command}",
                UseShellExecute = false,
                // We redirect output only if the user wants output
                RedirectStandardOutput = attendedOutput != ""
            };
            Process process = Process.Start(startInfo);
            process.WaitForExit();

            // If we are waiting for an output, we wait until the end of the output
            string result = attendedOutput != "" ? process.StandardOutput.ReadToEnd().Trim() : "-1";
            Logger.LogVerboseInfo($"RunPowershellCommandOnHost command '{command}' \n Returned: '{result}'");
            return result;
        }

        /// <summary>
        /// Runs a function multiple times 'Times(10, () => Console.WriteLine("10 lines"));'
        /// </summary>
        /// <param name="count"></param>
        /// <param name="action"></param>
        public static void Times(this int count, Action action)
        {
            for (int i = 0; i < count; i++)
                action();
        }

        public static void WaitUntil(Func<bool> condition, TimeSpan frequency, TimeSpan timeout, string timeoutMessage = "") => 
            WaitUntilAsync(condition, frequency, timeout, timeoutMessage)
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();

        /// <summary>
        /// Blocks until condition is true or timeout occurs.
        /// </summary>
        /// <param name="condition">The break condition.</param>
        /// <param name="frequency">The frequency at which the condition will be checked.</param>
        /// <param name="timeout">The timeout in milliseconds.</param>
        /// <param name="timeoutMessage"></param>
        /// <returns>Task</returns>
        public static async Task WaitUntilAsync(Func<bool> condition, TimeSpan frequency, TimeSpan timeout, string timeoutMessage = "")
        {
            Task waitTask = Task.Run(async () => { while (!condition()) { await Task.Delay(frequency).ConfigureAwait(false); } });

            if (waitTask != await Task.WhenAny(waitTask, Task.Delay(timeout)).ConfigureAwait(false))
            {
                if (!string.IsNullOrEmpty(timeoutMessage))
                    throw new TimeoutException(timeoutMessage);
                else
                    throw new TimeoutException();
            }
        }

        /// <summary>
        /// Retries a task a couple of times for a specific exception type
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="times">The amount of retries.</param>
        /// <param name="delay">The retry delay after the function when an exception is throw.</param>
        /// <param name="operation">The operation/task to run.</param>
        private static async Task RetryOnExceptionAsync<TException>(int times, TimeSpan delay, Func<Task> operation, bool retryOnTimeout = true) where TException : Exception
        {
            if (times <= 0)
                throw new ArgumentOutOfRangeException(nameof(times));

            int attempts = 0;
            while (true)
            {
                try
                {
                    attempts++;
                    operation().ConfigureAwait(false).GetAwaiter().GetResult();
                    break;
                }
                catch (TimeoutException) when (retryOnTimeout) { await Task.Delay(delay).ConfigureAwait(false); }
                catch (TException) when (attempts != times) { await Task.Delay(delay).ConfigureAwait(false); }
            }
        }
    }
}
