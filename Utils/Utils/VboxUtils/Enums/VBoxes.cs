﻿using System;
using System.Linq;
using Utils.Utils.SettingsInterfaces;

namespace Utils.Utils.VboxUtils.Enums
{
    public static class VBoxes
    {
        private static VBoxManager[] s_vboxes;

        static VBoxes() => s_vboxes = VboxConfig
            .Vboxes
            .Select(vbox => new VBoxManager(vbox.Name, vbox.DefaultNetworkCard, vbox.DefaultInitialSnapshot))
            .ToArray();

        private static IVirtualBoxSettings VboxConfig => ConfigurationManager.ConfigurationManager.Config.VirtualBoxSettings;

        public static VBoxManager GetVmWhichIsNotInUse()
        {
            try
            {
                return s_vboxes.First(vbox => !vbox.IsInUse) ?? throw new Exception("All VMs are currently in use");
            }
            catch (InvalidOperationException e) when (e.Message.Contains("Sequence contains no matching element"))
            {
                throw new Exception("All VMs are currently in use");
            }
        }
    }
}
