﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using NUnit.Framework;
using Utils.Utils.Drivers;
using Utils.Utils.SettingsInterfaces;
using static Utils.Utils.Utils;

namespace Utils.Utils.VboxUtils
{
    public class VBoxManager
    {
        #region configs

        private static IDebugOptions DebugOptions => ConfigurationManager.ConfigurationManager.Config.DebugOptions;
        private static IFilePaths FilePaths => ConfigurationManager.ConfigurationManager.Config.FilePaths;
        private static IVboxManageSettings VboxManageSettings => ConfigurationManager.ConfigurationManager.Config.VirtualBoxSettings.VboxManageSettings;

        #endregion configs

        public Action DoAfterStart;
        private readonly string _defaultInitialSnapshot;
        private readonly int _defaultNetworkCardToPing;
        private readonly string _vboxDirectory = Constants.VboxDirectory;
        private readonly string _vmInUseFilePath;

        public VBoxManager(string vmName, int defaultNetworkCardToPing, string defaultInitialSnapshot)
        {
            Name = vmName;
            _defaultNetworkCardToPing = defaultNetworkCardToPing;
            _defaultInitialSnapshot = defaultInitialSnapshot;
            _vmInUseFilePath = Path.Combine(FilePaths.LocalFilesPath, $@"RunningVMs\{Name}");
        }

        public string IP
        {
            get
            {
                string ipVm() => LaunchVboxManager($@"guestproperty get ""{Name}"" ""/VirtualBox/GuestInfo/Net/{_defaultNetworkCardToPing}/V4/IP""", "Value");
                return ipVm() != "-1" && ipVm().Length >= 7 ? ipVm().Substring(7) : "-1";
            }
        }

        public bool IsInUse => File.Exists(_vmInUseFilePath);

        public bool IsRunning => "True" == RunPowerShellCommandOnHost($@"$vBoxManage = '{ _vboxDirectory }\VBoxManage.exe';$runningVmList = & $vBoxManage list runningvms; $runningVmList -ne $null -and $runningVmList.Contains('{Name}')", "True");

        public string Name { get; }
        private List<string> VmInfo => LaunchVboxManager($"showvminfo {Name} --machinereadable", "SnapshotName").Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();

        public static void CleanUpSnapshots()
        {
            List<string> updatedSnapshots = DriverConnection
                .GetVmInUse()
                .GetAllSnapshotNames()
                .Where(snapshot => snapshot.Contains("UPDATED_EURO_"))
                .ToList();

            Logger.LogVerboseInfo($"List of updatetest snapshots: {string.Join("\n - ", updatedSnapshots.ToArray())}");

            List<string> majorMinorVersionsUnique = updatedSnapshots
                .Select(snapshot => snapshot.Replace("UPDATED_EURO_", "").Substring(0, 5))
                .Distinct()
                .ToList();

            Logger.LogVerboseInfo($"List of unique versions of updatetest snapshots: {string.Join("\n - ", majorMinorVersionsUnique.ToArray())}");

            foreach (var majorMinorVersion in majorMinorVersionsUnique)
            {
                var allSnapshotsOfMajorMinor = updatedSnapshots.Where(snapshot => snapshot.Contains(majorMinorVersion)).ToList();
                if (allSnapshotsOfMajorMinor.Count > VboxManageSettings.MaxAmountOfSnapshotsToKeepPerFuelPosVersion)
                {
                    allSnapshotsOfMajorMinor.Sort();
                    allSnapshotsOfMajorMinor
                        .Skip(VboxManageSettings.MaxAmountOfSnapshotsToKeepPerFuelPosVersion)
                        .ToList()
                        .ForEach(snapshot => DriverConnection.GetVmInUse().DeleteSnapshot(snapshot));
                }
            }
        }

        public static List<string> GetListOfSnapshotsToUpdateFrom(List<string> listOfMajorVersionsToTest)
        {
            List<string> snapshotNamesToUpdateFrom = new List<string>();
            List<string> allSnapshots = new List<string>();
            VBoxManager vbox = DriverConnection.GetVmInUse();

            // Returns a list of all snapshot names
            allSnapshots = vbox.GetAllSnapshotNames();

            // Loop through each version to test and for each version check if a snapshot exists containing that version
            foreach (string currentMajorVersion in listOfMajorVersionsToTest)
            {
                // We use a temp list for all found matches,
                // used to filter out any major versions that have multiple minor version snapshots
                List<string> tempFoundMatches = new List<string>();
                allSnapshots.ForEach(vmName => { if (vmName.Contains(Constants.SnapshotDefaultPrefix + currentMajorVersion)) tempFoundMatches.Add(vmName); });

                // Throw this error if no snapshot was found for the version
                Assert.That(tempFoundMatches.Count != 0, $"No Snapshot name found that contains the major version: {currentMajorVersion} - Exclude this version or create a snapshot for it to start the UpdateTests.");

                // First sorts the list ascending then reverses it,
                // so that the version with the highest minor version is the first index
                tempFoundMatches.Sort();
                tempFoundMatches.Reverse();

                snapshotNamesToUpdateFrom.Add(tempFoundMatches[0]);
            }
            Logger.LogInfo($"Snapshot(s) to update from: \n - { string.Join("\n - ", snapshotNamesToUpdateFrom.ToArray())}");
            return snapshotNamesToUpdateFrom;
        }

        /// <summary>
        /// Adds a shared folder while the VM is running, transient shared folders automatically get removed when the VM stops
        /// </summary>
        /// <param name="path"></param>
        /// <param name="folderName"></param>
        public string AddTransientSharedFolder(string path, string folderName = "temp")
        {
            if (!IsRunning)
            {
                Logger.LogWarning("Can't add transient shared folder if VM is not running, starting VM");
                Start();
            }

            // Remove if already exist
            List<string> vmInfo = VmInfo;
            if (vmInfo.Contains("SharedFolderNameTransientMapping"))
            {
                IEnumerable<string> transientSharedFolders = vmInfo.Where(line => line.Contains("SharedFolderNameTransientMapping"));
                if (transientSharedFolders.Contains(folderName))
                    LaunchVboxManager($@"sharedfolder remove ""{Name}"" --name ""{folderName}""");
            }

            // Add folder
            LaunchVboxManager($@"sharedfolder add ""{Name}"" --name ""{folderName}"" --hostpath ""{path}"" -transient -readonly -automount");

            // Return path for guest
            return Path.Combine("\\\\VBoxSvr", folderName);
        }

        public void MakeSnapshot(string snapshotName, string description) => LaunchVboxManager($@"snapshot ""{Name}"" take ""{snapshotName}"" --description ""{description}""");

        public void RestoreDefaultInitialSnapshot() => RestoreSnapshot(_defaultInitialSnapshot);

        public void RestoreSnapshot(string snapshotName)
        {
            StopVM();
            Logger.LogVerboseInfo($"Restoring snapshot '{snapshotName}' on VM '{Name}'");
            LaunchVboxManager($@"snapshot ""{Name}"" restore ""{snapshotName}""");
            Logger.LogInfo($"Restored snapshot '{snapshotName}' on VM '{Name}'");
        }

        public void Start(bool headless = true)
        {
            Logger.LogInfo($"Starting VM '{Name}'");
            string command = $@"startvm ""{Name}""";
            if (headless)
                command += " --type headless";

            // Start vm
            LaunchVboxManager(command);

            // Wait until vbox claims it is running, and start the vm every three tries (sometimes vboxManage doesn't start it, so retry is needed)
            int tries = 0;
            WaitUntil(
                () =>
                {
                    tries++;
                    if (tries == 3)
                    {
                        LaunchVboxManager(command);
                        tries = 0;
                    }
                    return IsRunning;
                },
                TimeSpan.FromSeconds(5),
                TimeSpan.FromMinutes(5),
                "Couldn't start VM within time limit"
            );

            // Wait until booted
            WaitUntil(
                () => IPAddress.TryParse(IP, out IPAddress address),
                TimeSpan.FromMinutes(1),
                TimeSpan.FromMinutes(15),
                $"Couldn't manage to get the IP of VM '{Name}' - networkcard {_defaultNetworkCardToPing} within timespan."
            );

            // Do after start action if set
            DoAfterStart?.Invoke();
        }

        public string TakeScreenshotAsBase64()
        {
            LaunchVboxManager($@"controlvm ""{Name}"" screenshotpng ""{TempDirectoryUtil.s_temp_directory}/tempScreenshot.png""");
            return Convert.ToBase64String(File.ReadAllBytes($"{TempDirectoryUtil.s_temp_directory}/tempScreenshot.png"));
        }

        public void ToggleInUse(bool inUse)
        {
            if (DebugOptions.CreateRunningVboxesFiles && inUse)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_vmInUseFilePath));
                File.Create(_vmInUseFilePath);
            }
            else
            {
                File.Delete(_vmInUseFilePath);
            }
        }

        private void DeleteSnapshot(string snapshotName) => LaunchVboxManager($@"snapshot ""{Name}"" delete ""{snapshotName}""");

        private List<string> GetAllSnapshotNames()
        {
            string resultString = LaunchVboxManager($@"snapshot ""{Name}"" list --machinereadable", "SnapshotName");
            List<string> listOfAllSnapshots = resultString
                .Split(new string[] { "\r\n" }, StringSplitOptions.None)
                .Where(line => line.Contains("SnapshotName"))
                .Select(line =>
                {
                    int indexFrom = line.IndexOf('"') + 1;
                    int indexTo = line.LastIndexOf('"');
                    return line.Substring(indexFrom, indexTo - indexFrom);
                })
                .ToList();

            Logger.LogInfo($"Succesfully retrieved all snapshots for VM '{Name}'");
            return listOfAllSnapshots;
        }

        private string LaunchVboxManager(string args, string attendedOutput) => LaunchVBoxManager(args, attendedOutput, null);

        private string LaunchVboxManager(string args) => LaunchVboxManager(args, "");

        private string LaunchVBoxManager(string args, string attendedOutput, TimeSpan? timeOut)
        {
            ProcessStartInfo vboxManageStartInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                FileName = $@"{_vboxDirectory}\VBoxManage.exe",
                Arguments = args,
                UseShellExecute = false,
                // We redirect output only if the user wants output
                RedirectStandardOutput = attendedOutput != ""
            };

            Process vboxManage = Process.Start(vboxManageStartInfo);
            if (timeOut == null)
                timeOut = TimeSpan.FromMinutes(VboxManageSettings.DefaultVboxCommandTimeOutInMinutes);

            vboxManage.WaitForExit((int)((TimeSpan)timeOut).TotalMilliseconds);

            // If we are waiting for an output, we wait until the end of the output
            string result = attendedOutput != "" ? vboxManage.StandardOutput.ReadToEnd().Trim() : "-1";
            Logger.LogVerboseInfo($"VboxManage command '{args}' \n With TimeOut {timeOut.Value.TotalMinutes} minutes \n Returned: '{result}'");
            return result;
        }

        /// <summary>
        /// Stops the vm - Shouldn't be called be the coder, since the coder should shut the vm down in a clean way
        /// </summary>
        private void StopVM()
        {
            Logger.LogInfo($"Stopping VM '{Name}'");
            LaunchVboxManager($@"controlvm ""{Name}"" poweroff soft");
            WaitUntil(() => !IsRunning, TimeSpan.FromSeconds(5), TimeSpan.FromMinutes(5), "Couldn't stop VM within time limit");
        }
    }
}
