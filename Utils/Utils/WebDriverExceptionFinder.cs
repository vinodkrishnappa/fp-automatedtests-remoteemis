﻿using OpenQA.Selenium;

namespace Utils.Utils
{
    public static class WebDriverExceptionFinder
    {
        public static bool IsHttpTimeOutException(WebDriverException exception) => exception.Message.Contains("HTTP request to the remote WebDriver") && exception.Message.Contains("timed out after");

        public static bool IsImageOccurencesNotFoundInFullImageException(WebDriverException exception) => exception.Message.Contains("Cannot find any occurrences of the partial image in the full image.");

        public static bool IsSessionTerminatedException(WebDriverException exception) => exception.Message.Contains("A session is either terminated or not started");
    }
}
